package com.joseph.TBD.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.main.DetailsListActivity;

/**
 * Created by ToSuccess on 11/22/2016.
 */

public class MusclelistAdapter extends BaseAdapter{

    Context _context;

    public MusclelistAdapter (Context context){

        this._context = context ;
    }
    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MusclHolder musclHolder ;

        if (convertView == null) {

            musclHolder = new MusclHolder();

            LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_mucle_list, parent, false);

            musclHolder.ui_txvContent = (TextView) convertView.findViewById(R.id.txv_content);
            musclHolder.ui_imvBack = (ImageView) convertView.findViewById(R.id.imv_back);

            convertView.setTag(musclHolder);
        } else {

            musclHolder =(MusclHolder) convertView.getTag();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return convertView;
    }

    public class MusclHolder {

        TextView ui_txvContent ;
        ImageView ui_imvBack;
        TextView ui_txvIndex ;

    }
}

package com.joseph.TBD.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.main.OrientationActivity;

import java.io.InputStream;

public class EulaFragment extends Fragment {

    OrientationActivity _activity;
    View view;

    String result;
    TextView txv_eula;

    public EulaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_eula, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        txv_eula = (TextView)view.findViewById(R.id.txv_eula);

        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.tt);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);
        } catch (Exception e) {
            result = "Error : can't show file."  ;
        }

        txv_eula.setText(result.toString());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (OrientationActivity) context;
    }

}

package com.joseph.TBD.main;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.fragment.EulaFragment;
import com.joseph.TBD.fragment.HomeFragment;
import com.joseph.TBD.fragment.TutorialFragment;

public class OrientationActivity extends CommonActivity implements View.OnClickListener {

    LinearLayout lyt_eula,lyt_tutorial;
    ImageView imv_back;

    TextView txv_eula, txv_tutorial;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orientation);

        loadLayout();

    }

    private void loadLayout() {

        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(this);

        lyt_eula = (LinearLayout)findViewById(R.id.lyt_eula);
        lyt_eula.setOnClickListener(this);

        lyt_tutorial = (LinearLayout)findViewById(R.id.lyt_tutorial);
        lyt_tutorial.setOnClickListener(this);

        txv_eula = (TextView)findViewById(R.id.txv_eula);
        txv_tutorial = (TextView)findViewById(R.id.txv_tutorial);

        gotoEulaFragment();
        colorSelect(R.color.colorAccent, R.color.white);
    }

    public void gotoEulaFragment(){

        EulaFragment fragment = new EulaFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fly_containter, fragment).commit();
    }

    public void gotoTutorialFragment(){

        TutorialFragment fragment = new TutorialFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fly_containter, fragment).commit();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;

            case R.id.lyt_eula:
                gotoEulaFragment();
                colorSelect(R.color.colorAccent, R.color.white);
                break;

            case R.id.lyt_tutorial:
                gotoTutorialFragment();
                colorSelect(R.color.white, R.color.colorAccent);
                break;
        }
    }

    public void colorSelect(int a, int b){

        txv_eula.setTextColor(getResources().getColor(a));
        txv_tutorial.setTextColor(getResources().getColor(b));

    }
}

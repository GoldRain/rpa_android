package com.joseph.TBD.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.joseph.TBD.R;

import java.util.ArrayList;


public class CommonlistAdapter extends BaseAdapter {

    private  static final int TYPE_INDEX = 0;
    private static final int TYPE_USER = 1;

    private Context _context ;

    public ArrayList<String> _contents = new ArrayList<>();
    private ArrayList<String> _allcontents = new ArrayList<>();

    public CommonlistAdapter(Context context, ArrayList<String> contents){

        super();
        this._context = context ;
    }

    @Override
    public int getCount() {
        return _contents.size();
    }

    @Override
    public Object getItem(int position) {
        return _contents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        if (getItem(position).toString().length() > 1)
            return TYPE_USER ;

        return TYPE_INDEX ;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        switch (type) {

            case TYPE_INDEX :{

                IndexHolder holder ;

                if (convertView == null){

                    holder = new IndexHolder();

                    LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.directory_indexlistitem, parent,false);

                    holder.txvIndex = (TextView)convertView.findViewById(R.id.txv_index);

                    convertView.setTag(holder);

                }else {
                    holder = (IndexHolder) convertView.getTag();
                }

                String index = _contents.get(position);
                holder.txvIndex.setText(index);
            }

            break;

            case TYPE_USER: {

                CustomHolder holder;

                if (convertView == null) {

                    holder = new CustomHolder();

                    LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.directory_userlistitem, parent, false);

                    holder.txvName = (TextView) convertView.findViewById(R.id.txv_content);

                    convertView.setTag(holder);

                } else {

                    holder = (CustomHolder) convertView.getTag();
                }

                holder.txvName.setText(_contents.get(position));
            }

                break;
        }
        return convertView;
    }

    public void addItem(ArrayList<String> entity){

        _allcontents.addAll(entity);
    }

    public void initProducts() {

        _contents.clear();
        _contents.addAll(_allcontents);
    }

    public void filter(String charText) {

        charText = charText.toLowerCase();

        _contents.clear();

        if (charText.length() == 0) {
            _contents.addAll(_allcontents);

        } else {

            for (String product : _allcontents) {

                if (product.toLowerCase().contains(charText))
                {
                    _contents.add(product);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class CustomHolder {

        public TextView txvName;

    }

    public class IndexHolder {

        public TextView txvIndex;
    }
}

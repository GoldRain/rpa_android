package com.joseph.TBD.commons;

import android.os.Handler;

import com.joseph.TBD.base.CommonActivity;

/**
 * Created by ToSuccess on 11/13/2016.
 */

public class Commons {

    public static boolean g_isAppRunning = false ;
    public static boolean g_isAppPaused = false ;

    public static Handler g_handler = null ;
    public static String g_appVersion = "1.0";

    public static int status = 0 ;


    public static CommonActivity g_currentActivity = null;
}

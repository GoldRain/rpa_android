package com.joseph.TBD.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.joseph.TBD.R;
import com.joseph.TBD.base.BaseFragment;
import com.joseph.TBD.main.MainActivity;
import com.joseph.TBD.main.SearchByTissureActivity.JointActivity;
import com.joseph.TBD.main.SearchByTissureActivity.LigamentActivity;
import com.joseph.TBD.main.SearchByTissureActivity.MuscleActivity;
import com.joseph.TBD.main.SearchByTissureActivity.PeriostealActivity;

import java.util.ArrayList;


public class SearchFragment extends BaseFragment implements View.OnClickListener{


    MainActivity _activity ;
    View view ;

    ImageView imv_mus, imv_lig, imv_joint, imv_perios;

    public SearchFragment(MainActivity activity) {
        this._activity = activity ;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_search, container, false);

        loadLayout();

        return view ;
    }

    private void loadLayout() {

        imv_mus = (ImageView)view.findViewById(R.id.imv_muscle);
        imv_mus.setOnClickListener(this);

        imv_lig = (ImageView)view.findViewById(R.id.imv_lig);
        imv_lig.setOnClickListener(this);

        imv_joint = (ImageView)view.findViewById(R.id.imv_joint);
        imv_joint.setOnClickListener(this);

        imv_perios = (ImageView)view.findViewById(R.id.imv_periosteal);
        imv_perios.setOnClickListener(this);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.imv_muscle:

                Intent intent = new Intent(_activity, MuscleActivity.class);
                _activity.startActivity(intent);
                _activity.overridePendingTransition(0,0);
                break;

            case R.id.imv_lig:

                Intent intent1 = new Intent(_activity, LigamentActivity.class);
                _activity.startActivity(intent1);
                _activity.overridePendingTransition(0,0);

                break;

            case R.id.imv_joint:

                _activity.startActivity(new Intent(_activity, JointActivity.class));
                _activity.overridePendingTransition(0,0);
                break;

            case R.id.imv_periosteal:
                _activity.startActivity(new Intent(_activity, PeriostealActivity.class));
                _activity.overridePendingTransition(0,0);
                break;

        }
    }
}

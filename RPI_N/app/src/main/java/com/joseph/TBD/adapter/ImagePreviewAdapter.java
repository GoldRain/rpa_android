package com.joseph.TBD.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.joseph.TBD.R;
import com.joseph.TBD.utils.TouchImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/14/2017.
 */

public class ImagePreviewAdapter extends PagerAdapter {

    Context _context;
    LayoutInflater _layoutInflater;
    ArrayList<Integer> _imageUrls = new ArrayList<>();

    public ImagePreviewAdapter(Context context){

        _context = context ;
        _layoutInflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public void setDatas(ArrayList<Integer> datas){

        _imageUrls = datas;

        Log.d("===Count==>", String.valueOf(_imageUrls.size()));
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return _imageUrls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = _layoutInflater.inflate(R.layout.item_image_preview, container, false);

        final TouchImageView imageView = (TouchImageView) itemView.findViewById(R.id.imv_preview);
        imageView.setImageResource(R.drawable.slide1);

        Glide.with(_context).load(_imageUrls.get(position)).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                imageView.setImageBitmap(resource);
            }
        });

        container.addView(itemView);
        return itemView ;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}

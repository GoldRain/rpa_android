package com.joseph.TBD.main.PainSelcection;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.adapter.StoreAdapter;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.DetailsActivity.AnconiusDetailsActivity;
import com.joseph.TBD.model.ItemModel;

import java.util.ArrayList;

import static com.joseph.TBD.commons.Constants.anconiusstatus;
import static com.joseph.TBD.commons.Constants.array_ligamentList;
import static com.joseph.TBD.commons.Constants.backbody1;
import static com.joseph.TBD.commons.Constants.backbody2;
import static com.joseph.TBD.commons.Constants.backbody3;
import static com.joseph.TBD.commons.Constants.backbody4;
import static com.joseph.TBD.commons.Constants.backfoot1;
import static com.joseph.TBD.commons.Constants.backfoot10;
import static com.joseph.TBD.commons.Constants.backfoot11;
import static com.joseph.TBD.commons.Constants.backfoot12;
import static com.joseph.TBD.commons.Constants.backfoot13;
import static com.joseph.TBD.commons.Constants.backfoot14;
import static com.joseph.TBD.commons.Constants.backfoot15;
import static com.joseph.TBD.commons.Constants.backfoot16;
import static com.joseph.TBD.commons.Constants.backfoot17;
import static com.joseph.TBD.commons.Constants.backfoot18;
import static com.joseph.TBD.commons.Constants.backfoot19;
import static com.joseph.TBD.commons.Constants.backfoot2;
import static com.joseph.TBD.commons.Constants.backfoot20;
import static com.joseph.TBD.commons.Constants.backfoot21;
import static com.joseph.TBD.commons.Constants.backfoot22;
import static com.joseph.TBD.commons.Constants.backfoot3;
import static com.joseph.TBD.commons.Constants.backfoot4;
import static com.joseph.TBD.commons.Constants.backfoot5;
import static com.joseph.TBD.commons.Constants.backfoot6;
import static com.joseph.TBD.commons.Constants.backfoot7;
import static com.joseph.TBD.commons.Constants.backfoot8;
import static com.joseph.TBD.commons.Constants.backfoot9;
import static com.joseph.TBD.commons.Constants.backhead1;
import static com.joseph.TBD.commons.Constants.backhead2;
import static com.joseph.TBD.commons.Constants.backhead3;
import static com.joseph.TBD.commons.Constants.backhead4;
import static com.joseph.TBD.commons.Constants.backhead5;
import static com.joseph.TBD.commons.Constants.backhead6;
import static com.joseph.TBD.commons.Constants.backleg1;
import static com.joseph.TBD.commons.Constants.backleg10;
import static com.joseph.TBD.commons.Constants.backleg11;
import static com.joseph.TBD.commons.Constants.backleg12;
import static com.joseph.TBD.commons.Constants.backleg13;
import static com.joseph.TBD.commons.Constants.backleg2;
import static com.joseph.TBD.commons.Constants.backleg3;
import static com.joseph.TBD.commons.Constants.backleg4;
import static com.joseph.TBD.commons.Constants.backleg5;
import static com.joseph.TBD.commons.Constants.backleg6;
import static com.joseph.TBD.commons.Constants.backleg7;
import static com.joseph.TBD.commons.Constants.backleg8;
import static com.joseph.TBD.commons.Constants.backleg9;
import static com.joseph.TBD.commons.Constants.body_part;
import static com.joseph.TBD.commons.Constants.frontbody1;
import static com.joseph.TBD.commons.Constants.frontbody2;
import static com.joseph.TBD.commons.Constants.frontbody3;
import static com.joseph.TBD.commons.Constants.frontfoot1;
import static com.joseph.TBD.commons.Constants.frontfoot10;
import static com.joseph.TBD.commons.Constants.frontfoot11;
import static com.joseph.TBD.commons.Constants.frontfoot2;
import static com.joseph.TBD.commons.Constants.frontfoot3;
import static com.joseph.TBD.commons.Constants.frontfoot4;
import static com.joseph.TBD.commons.Constants.frontfoot5;
import static com.joseph.TBD.commons.Constants.frontfoot6;
import static com.joseph.TBD.commons.Constants.frontfoot7;
import static com.joseph.TBD.commons.Constants.frontfoot8;
import static com.joseph.TBD.commons.Constants.frontfoot9;
import static com.joseph.TBD.commons.Constants.fronthead1;
import static com.joseph.TBD.commons.Constants.fronthead2;
import static com.joseph.TBD.commons.Constants.fronthead3;
import static com.joseph.TBD.commons.Constants.fronthead4;
import static com.joseph.TBD.commons.Constants.fronthead5;
import static com.joseph.TBD.commons.Constants.fronthead6;
import static com.joseph.TBD.commons.Constants.fronthead7;
import static com.joseph.TBD.commons.Constants.fronthead8;
import static com.joseph.TBD.commons.Constants.fronthead9;
import static com.joseph.TBD.commons.Constants.frontleg1;
import static com.joseph.TBD.commons.Constants.frontleg10;
import static com.joseph.TBD.commons.Constants.frontleg11;
import static com.joseph.TBD.commons.Constants.frontleg2;
import static com.joseph.TBD.commons.Constants.frontleg3;
import static com.joseph.TBD.commons.Constants.frontleg4;
import static com.joseph.TBD.commons.Constants.frontleg5;
import static com.joseph.TBD.commons.Constants.frontleg6;
import static com.joseph.TBD.commons.Constants.frontleg7;
import static com.joseph.TBD.commons.Constants.frontleg8;
import static com.joseph.TBD.commons.Constants.frontleg9;
import static com.joseph.TBD.commons.Constants.jointList;
import static com.joseph.TBD.commons.Constants.muscleList;
import static com.joseph.TBD.commons.Constants.periostealList;
import static com.joseph.TBD.commons.Constants.post;
import static com.joseph.TBD.commons.Constants.screenheight;
import static com.joseph.TBD.commons.Constants.screenwidth;

public class PainSelectDetaileActivity extends CommonActivity implements View.OnClickListener {

    TextView ui_txvCaption;
    ImageView ui_imvBack;
    GridView gridView;
    StoreAdapter storeAdapter;

    ArrayList<ItemModel> _images = new ArrayList<>();

    ArrayList<String> nameList = new ArrayList<>();

    LinearLayout lyt_title_pain_details;
    int titlbar = 0;
    int width = 0;
    int height = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_painselectdetaile);

        loadLayout();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void getImageData(ArrayList<String> nameList){

        _images.clear();

        for(int i = 0; i < nameList.size(); i++){

            int k = 0;
            for(int j = 0; j < muscleList.size(); j++){

                if(nameList.get(i).equals(muscleList.get(j))){

                    int temp = 0;
                    for(int q = 0; q < _images.size(); q++){
                        if(nameList.get(q).equals(muscleList.get(j))){
                           temp = 1;
                        }
                    }

                    if(temp == 0) {
                        ItemModel _name = new ItemModel();
                        _name.setPosition(j);
                        _name.setSelect_status(1);
                        _images.add(_name);
                    }
                    j = muscleList.size();
                    k = 1;
                }
            }

            if(k != 1) {

                for (int j = 0; j < array_ligamentList.size(); j++) {

                    if (nameList.get(i).equals(array_ligamentList.get(j))) {

                        int temp = 0;
                        for(int q = 0; q < _images.size(); q++){
                            if(nameList.get(q).equals(array_ligamentList.get(j))){
                                temp = 1;
                            }
                        }
                        if(temp == 0) {
                            ItemModel _name = new ItemModel();
                            _name.setPosition(j);
                            _name.setSelect_status(2);
                            _images.add(_name);
                        }
                        j = array_ligamentList.size();
                        k = 1;
                    }
                }

                if(k != 1) {
                    for (int j = 0; j < jointList.size(); j++) {

                        if (nameList.get(i).equals(jointList.get(j))) {

                            int temp = 0;
                            for(int q = 0; q < _images.size(); q++){
                                if(nameList.get(q).equals(jointList.get(j))){
                                    temp = 1;
                                }
                            }
                            if (temp == 0) {
                                ItemModel _name = new ItemModel();
                                _name.setPosition(j);
                                _name.setSelect_status(3);
                                _images.add(_name);
                            }
                            j = jointList.size();
                            k = 1;
                        }
                    }

                    if(k != 1) {
                        for (int j = 0; j < periostealList.size(); j++) {

                            if (nameList.get(i).equals(periostealList.get(j))) {
                                int temp = 0;
                                for(int q = 0; q < _images.size(); q++){
                                    if(nameList.get(q).equals(periostealList.get(j))){
                                        temp = 1;
                                    }
                                }
                                if (temp == 0) {
                                    ItemModel _name = new ItemModel();
                                    _name.setPosition(j);
                                    _name.setSelect_status(4);
                                    _images.add(_name);
                                }
                                j = periostealList.size();
                            }
                        }
                    }
                }
            }
        }
    }

    public void loadLayout(){

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        Display display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        final int statusbarheight = getStatusBarHeight();

        Log.d("statusbarHeight", String.valueOf(statusbarheight));

        gridView = (GridView)findViewById(R.id.gridView);
        storeAdapter = new StoreAdapter(this);

        lyt_title_pain_details = (LinearLayout)findViewById(R.id.lyt_title_pain_details);

        lyt_title_pain_details.post(new Runnable()
        {

            @Override
            public void run()
            {
                Log.i("TEST", "Layout width : "+ lyt_title_pain_details.getHeight());


                titlbar = lyt_title_pain_details.getHeight();
                Log.d("TITLE==>", String.valueOf(titlbar));

                screenwidth = width - 5 * 3;
                screenheight = height - statusbarheight - titlbar - 15*3/* - 3 * 72*/;
                Log.d("width==========>", String.valueOf(width));
                Log.d("Height++++++++==>", String.valueOf(height));
                gridView.setAdapter(storeAdapter);

            }
        });



        ui_txvCaption = (TextView)findViewById(R.id.txv_title);

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);



        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.d("Possition===", String.valueOf(position));
                ItemModel selected = _images.get(position);

                Constants.select_Status = selected.getSelect_status();
                Constants.position = selected.getPosition();

                Log.d("position==============>", String.valueOf(Constants.position));

                Intent intent = new Intent(PainSelectDetaileActivity.this, AnconiusDetailsActivity.class);
                startActivity(intent);
            }
        });

        if(post == 1 && body_part == 8){//front body

            nameList.clear();

            if(frontbody1 == 1){

                nameList.add("External Oblique");
                nameList.add("Intercostal");
                nameList.add("Lateral Oblique");
                nameList.add("Xiphoid");
                nameList.add("Dura (Thoracic)");
                nameList.add("Sclerotome T8");
                nameList.add("Thermatome (T4-L2)");

            }

            if(frontbody2 == 1){

                nameList.add("External Oblique");
                nameList.add("Lateral Oblique");
                nameList.add("Latissimus Dorsi 2");
                nameList.add("Dura (Thoracic)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sclerotome L1");
                nameList.add("Sclerotome T12");
                nameList.add("Sclerotome T7");
                nameList.add("Thermatome (T4-L2)");

            }

            if(frontbody3 == 1){

                nameList.add("Lateral Oblique");
                nameList.add("Pectineus");
                nameList.add("Quadratus Lumborum 3");
                nameList.add("Vastus Lateralis 4");
                nameList.add("Lateral Pubic Symphysis");
                nameList.add("Superior Pubic Symphysis");
                nameList.add("Inguinal");
                nameList.add("Lumbar Facet Capsule");
                nameList.add("Pubic Symphysis");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Supraspinatus (S1)");
                nameList.add("Dura (Thoracic)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sacroiliac Joint");
                nameList.add("Sclerotome L1");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome T10");
                nameList.add("Sclerotome T9");
                nameList.add("Thermatome (T11-L2)");
                nameList.add("Thermatome (T4-L2)");
            }

            getImageData(nameList);

        } else if(post == 2 && body_part == 4){//back head

            nameList.clear();

            if(backhead1 == 1){

                nameList.add("Occipitalis");
                nameList.add("Splenius Cervicis 1");
                nameList.add("Sternocleidomastoid 1");
                nameList.add("Cervical Facet Capsule (Upper)");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Thermatome (T1-T2)");

            }

            if(backhead2 == 1){

                nameList.add("Semispinalis Capitis 2");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Facet C0-C1-C2");
                nameList.add("Facet C2-C3");
                nameList.add("Sclerotome C1");
                nameList.add("Sclerotome C2");

            }

            if(backhead3 == 1){

                nameList.add("Multifidus (Cervical)");
                nameList.add("Nuchal Line");
                nameList.add("Posterior Foramen Magnum");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Facet C0-C1-C2");
                nameList.add("Facet C2-C3");
                nameList.add("Facet C3-C4");
                nameList.add("Facet C4-C5");
                nameList.add("Sclerotome C1");
                nameList.add("Sclerotome C2");
                nameList.add("Sclerotome C5");
                nameList.add("Sclerotome C6");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");

            }

            if(backhead4 == 1){

                nameList.add("C2 Spinous Process");
                nameList.add("Spinous Process");
                nameList.add("Dura (Cervicothoracic)");

            }

            if(backhead5 == 1){

                nameList.add("Levator Scapulae");
                nameList.add("Multifidus (Cervical)");
                nameList.add("Paraspinal (Mid Thoracic)");
                nameList.add("Scalene");
                nameList.add("Splenius Cervicis 2");
                nameList.add("Supraspinatus 1");
                nameList.add("Trapezius 2");
                nameList.add("Trapezius 3");
                nameList.add("Trapezius 6");
                nameList.add("Triceps Brachii 1");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Facet C3-C4");
                nameList.add("Facet C4-C5");
                nameList.add("Facet C5-C6");
                nameList.add("Facet C6-C7");
                nameList.add("Facet C7-T1");
                nameList.add("Facet T1-T2");
                nameList.add("Sclerotome C1");
                nameList.add("Sclerotome C3");
                nameList.add("Sclerotome C4");
                nameList.add("Sclerotome C5");
                nameList.add("Sclerotome C6");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Sclerotome T1");

            }

            if(backhead6 == 1){

                nameList.add("Masseter 4");
                nameList.add("Medial Pterygoid");
                nameList.add("Splenius Cervicis 2");
                nameList.add("Sternocleidomastoid 2");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Sclerotome C1");
                nameList.add("Thermatome (T1-T2)");

            }

            getImageData(nameList);

        } else if(post == 2 && body_part == 8){//back body

            nameList.clear();

            if(backbody1 == 1){

                nameList.add("Paraspinal (Mid Thoracic)");
                nameList.add("Paraspinal (Thoracolumbar)");
                nameList.add("Quadratus Lumborum 2");
                nameList.add("Quadratus Lumborum 4");
                nameList.add("Rectus Abdominis");
                nameList.add("Dura (Thoracic)");
                nameList.add("Facet T3-T4");
                nameList.add("Facet T5-T6");
                nameList.add("Facet T7-T11");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome T10");
                nameList.add("Sclerotome T12");
                nameList.add("Sclerotome T7");
                nameList.add("Sclerotome T8");
                nameList.add("Thermatome (T4-L2)");

            }

            if(backbody2 == 1){

                nameList.add("Iliopsoas");
                nameList.add("Paraspinal (Mid Thoracic)");
                nameList.add("Paraspinal (Thoracolumbar)");
                nameList.add("Quadratus Lumborum 1");
                nameList.add("Quadratus Lumborum 2");
                nameList.add("Serratus Posterior Inferior");
                nameList.add("Lumbar Facet Capsule");
                nameList.add("Dura (Thoracic)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Facet Lumbar");
                nameList.add("Facet T5-T6");
                nameList.add("Facet T6-T7");
                nameList.add("Facet T7-T11");
                nameList.add("Sclerotome L1");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome L5");
                nameList.add("Sclerotome T10");
                nameList.add("Sclerotome T11");
                nameList.add("Sclerotome T12");
                nameList.add("Sclerotome T8");
                nameList.add("Sclerotome T9");
                nameList.add("Thermatome (T11-L2)");
                nameList.add("Thermatome (T4-L2)");

            }

            if(backbody3 == 1){

                nameList.add("Iliopsoas");
                nameList.add("Paraspinal (Thoracolumbar)");
                nameList.add("Quadratus Lumborum 1");
                nameList.add("Quadratus Lumborum 2");
                nameList.add("Rectus Abdominis");
                nameList.add("Iliac Crest");
                nameList.add("Lumbar Facet Capsule");
                nameList.add("Thoracolumbar Facet Capsule");
                nameList.add("Dura (Thoracic)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Facet Lumbar");
                nameList.add("Facet T11-T12");
                nameList.add("Facet T7-T11");
                nameList.add("Sclerotome L1");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L3");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome L5");
                nameList.add("Sclerotome S1");
                nameList.add("Sclerotome T10");
                nameList.add("Sclerotome T12");
                nameList.add("Thermatome (T11-L2)");
                nameList.add("Thermatome (T4-L2)");

            }

            if(backbody4 == 1){

                nameList.add("Multifidus (Thoracic and Lumbar)");
                nameList.add("Rectus Abdominis");
                nameList.add("Spinous Process");
                nameList.add("Lumbar Facet Capsule");
                nameList.add("Dura (Thoracic)");
                nameList.add("Dura (Thoracolumbar)");
            }

            getImageData(nameList);

        } else if(post == 2 && (body_part == 5 || body_part == 6 || body_part == 7)){//back leg

            nameList.clear();

            if(backleg1 == 1){

                nameList.add("Multifidus (Thoracic and Lumbar)");
                nameList.add("Spinous Process");
                nameList.add("Dura (Cervicothoracic)");

            }

            if(backleg2 == 1){

                nameList.add("Levator Scapulae");
                nameList.add("Multifidus (Cervical)");
                nameList.add("Paraspinal (Mid Thoracic)");
                nameList.add("Scalene");
                nameList.add("Splenius Cervicis 2");
                nameList.add("Supraspinatus 1");
                nameList.add("Trapezius 2");
                nameList.add("Trapezius 3");
                nameList.add("Trapezius 6");
                nameList.add("Triceps Brachii 1");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Facet C3-C4");
                nameList.add("Facet C4-C5");
                nameList.add("Facet C5-C6");
                nameList.add("Facet C6-C7");
                nameList.add("Facet C7-T1");
                nameList.add("Facet T1-T2");
                nameList.add("Sclerotome C1");
                nameList.add("Sclerotome C3");
                nameList.add("Sclerotome C4");
                nameList.add("Sclerotome C5");
                nameList.add("Sclerotome C6");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Sclerotome T1");

            }

            if(backleg3 == 1){

                nameList.add("Infraspinatus");
                nameList.add("Latissimus Dorsi 1");
                nameList.add("Levator Scapulae");
                nameList.add("Multifidus (Cervical)");
                nameList.add("Paraspinal (Mid Thoracic)");
                nameList.add("Rhomboid");
                nameList.add("Scalene");
                nameList.add("Serratus Anterior");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Subscapularis");
                nameList.add("Trapezius 3");
                nameList.add("Trapezius 4");
                nameList.add("Trapezius 5");
                nameList.add("Costosternal");
                nameList.add("Costotransverse");
                nameList.add("Supraspinatus (T1)");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Facet C6-C7");
                nameList.add("Facet C7-T1");
                nameList.add("Facet T1-T2");
                nameList.add("Facet T3-T4");
                nameList.add("FacetT2-T3");
                nameList.add("FacetT4-T5");
                nameList.add("Sclerotome C5");
                nameList.add("Sclerotome C6");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Sclerotome T1");
                nameList.add("Sclerotome T2");
                nameList.add("Sclerotome T3");
                nameList.add("Sclerotome T4");
                nameList.add("Sclerotome T5");
                nameList.add("Thermatome (T1-T4)");
                nameList.add("Thermatome (T2-T7)");

            }

            if(backleg4 == 1){

                nameList.add("Coracobrachialis");
                nameList.add("Infraspinatus");
                nameList.add("Latissimus Dorsi 1");
                nameList.add("Levator Scapulae");
                nameList.add("Middle Deltoid");
                nameList.add("Posterior Deltoid");
                nameList.add("Scalene");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Subscapularis");
                nameList.add("Supraspinatus 1");
                nameList.add("Supraspinatus 2");
                nameList.add("Teres Major");
                nameList.add("Teres Minor");
                nameList.add("Triceps Brachii 1");
                nameList.add("Deltoid Insertion");
                nameList.add("Cervical Facet Capsule (Lower)");
                nameList.add("Cervical Facet Capsule (Mid)");
                nameList.add("Cervical Facet Capsule (Upper)");
                nameList.add("Glenohumeral Joint Capsule");
                nameList.add("Supraspinatus (C5)");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Facet C7-T1");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Sclerotome T2");
                nameList.add("Thermatome (T2-T7)");

            }

            if(backleg5 == 1){

                nameList.add("Paraspinal (Mid Thoracic)");
                nameList.add("Serratus Anterior");
                nameList.add("Suboccipital");
                nameList.add("Angle of the Ribs");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Facet T3-T4");
                nameList.add("FacetT2-T3");
                nameList.add("FacetT4-T5");
                nameList.add("Sclerotome T3");
                nameList.add("Sclerotome T5");
                nameList.add("Sclerotome T6");
                nameList.add("Sclerotome T7");
                nameList.add("Thermatome (T1-T2)");
                nameList.add("Thermatome (T1-T4)");
                nameList.add("Thermatome (T2-T7)");

            }

            if(backleg6 == 1){

                nameList.add("Coracobrachialis");
                nameList.add("Infraspinatus");
                nameList.add("Latissimus Dorsi 1");
                nameList.add("Posterior Deltoid");
                nameList.add("Scalene");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Subscapularis");
                nameList.add("Supraspinatus 1");
                nameList.add("Teres Minor");
                nameList.add("Trapezius 7");
                nameList.add("Triceps Brachii 1");
                nameList.add("Triceps Brachii 3");
                nameList.add("Cervical Facet Capsule (Lower)");
                nameList.add("Cervical Facet Capsule (Mid)");
                nameList.add("Cervical Facet Capsule (Upper)");
                nameList.add("Supraspinatus (C6)");
                nameList.add("Supraspinatus (C7)");
                nameList.add("Supraspinatus (T1)");
                nameList.add("Thermatome (T1-T4)");
                nameList.add("Thermatome (T2-T7)");

            }

            if(backleg7 == 1){

                nameList.add("Anconeus");
                nameList.add("Brachioradialis");
                nameList.add("Coracobrachialis");
                nameList.add("Extensor Carpi Radialis Longus");
                nameList.add("Extensor Digitorum 1");
                nameList.add("Extensor Digitorum 2");
                nameList.add("Infraspinatus");
                nameList.add("Scalene");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Subclavius");
                nameList.add("Supinator");
                nameList.add("Supraspinatus 1");
                nameList.add("Trapezius 7");
                nameList.add("Triceps Brachii 1");
                nameList.add("Triceps Brachii 2");
                nameList.add("Triceps Brachii 4");
                nameList.add("Lateral Epicondyle");
                nameList.add("Medial Epicondyle");
                nameList.add("Cervical Facet Capsule (Lower)");
                nameList.add("Cervical Facet Capsule (Mid)");
                nameList.add("Cervical Facet Capsule (Upper)");
                nameList.add("Radial Collateral");
                nameList.add("Supraspinatus (C6)");
                nameList.add("Supraspinatus (C7)");
                nameList.add("Supraspinatus (T1)");
                nameList.add("Sclerotome C6");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Thermatome (T1-T4)");
                nameList.add("Thermatome (T2-T7)");

            }

            if(backleg8 == 1){

                nameList.add("Brachioradialis");
                nameList.add("Coracobrachialis");
                nameList.add("Extensor Carpi Radialis Brevis");
                nameList.add("Extensor Carpi Radialis Longus");
                nameList.add("Extensor Carpi Ulnaris");
                nameList.add("Extensor Digitorum 1");
                nameList.add("Extensor Digitorum 2");
                nameList.add("Extensor Indicis");
                nameList.add("Infraspinatus");
                nameList.add("Scalene");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Subclavius");
                nameList.add("Subscapularis");
                nameList.add("Supinator");
                nameList.add("Supraspinatus 1");
                nameList.add("Triceps Brachii 1");
                nameList.add("Triceps Brachii 2");
                nameList.add("Triceps Brachii 3");
                nameList.add("Radial Styloid");
                nameList.add("Cervical Facet Capsule (Lower)");
                nameList.add("Cervical Facet Capsule (Mid)");
                nameList.add("Cervical Facet Capsule (Upper)");
                nameList.add("Interosseous Membrane (Forearm)");
                nameList.add("Radial Collateral");
                nameList.add("Supraspinatus (C6)");
                nameList.add("Supraspinatus (C7)");
                nameList.add("Supraspinatus (T1)");
                nameList.add("Ulnar Collateral");
                nameList.add("Sclerotome C6");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Thermatome (T2-T7)");

            }

            if(backleg9 == 1){

                nameList.add("Brachialis");
                nameList.add("Brachioradialis");
                nameList.add("Coracobrachialis");
                nameList.add("Extensor Carpi Radialis Brevis");
                nameList.add("Extensor Carpi Radialis Longus");
                nameList.add("Extensor Digitorum 2");
                nameList.add("Extensor Indicis");
                nameList.add("First Dorsal Interosseous of the Hand");
                nameList.add("Infraspinatus");
                nameList.add("Scalene");
                nameList.add("Scalenus Minimus");
                nameList.add("Subclavius");
                nameList.add("Supinator");
                nameList.add("Cervical Facet Capsule (Upper)");
                nameList.add("Dorsal Carpal");
                nameList.add("Thoracic Facet Capsule (Upper)");
                nameList.add("Thermatome (T2-T7)");
                nameList.add("");

            }

            if(backleg10 == 1){

                nameList.add("Extensor Indicis");
                nameList.add("First Dorsal Interosseous of the Hand");
                nameList.add("Scalene");
                nameList.add("Scalenus Minimus");
                nameList.add("Subclavius");
                nameList.add("Cervical Facet Capsule (Mid)");
                nameList.add("Dorsal Carpal");
                nameList.add("Thoracic Facet Capsule (Upper)");
                nameList.add("Thermatome (T2-T7)");

            }

            if(backleg11 == 1){

                nameList.add("Extensor Digitorum 2");
                nameList.add("Scalene");
                nameList.add("Scalenus Minimus");
                nameList.add("Second Dorsal Interosseous of the Hand");
                nameList.add("Subclavius");
                nameList.add("Cervical Facet Capsule (Mid)");
                nameList.add("Dorsal Carpal");
                nameList.add("Thoracic Facet Capsule (Upper)");
                nameList.add("Thermatome (T2-T7)");

            }

            if(backleg12 == 1){

                nameList.add("Extensor Digitorum 1");
                nameList.add("Scalenus Minimus");
                nameList.add("Triceps Brachii 3");
                nameList.add("Cervical Facet Capsule (Mid)");
                nameList.add("Dorsal Carpal");
                nameList.add("Supraspinatus (T1)");
                nameList.add("Thoracic Facet Capsule (Upper)");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Thermatome (T2-T7)");

            }

            if(backleg13 == 1){

                nameList.add("Abductor Digiti Minimi (hand)");
                nameList.add("Extensor Carpi Radialis Brevis");
                nameList.add("Extensor Carpi Ulnaris");
                nameList.add("Extensor Digitorum 1");
                nameList.add("First Dorsal Interosseous of the Hand");
                nameList.add("Infraspinatus");
                nameList.add("Scalenus Minimus");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Triceps Brachii 3");
                nameList.add("Cervical Facet Capsule (Lower)");
                nameList.add("Dorsal Carpal");
                nameList.add("Supraspinatus (T1)");
                nameList.add("Thoracic Facet Capsule (Upper)");
                nameList.add("Ulnar Collateral");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Thermatome (T2-T7)");
            }

            getImageData(nameList);

        } else if(post == 1 && body_part == 3){//front head

            nameList.clear();

            if(fronthead1 == 1){

                nameList.add("Occipitalis");
                nameList.add("Splenius Cervicis 1");
                nameList.add("Sternocleidomastoid 1");
                nameList.add("Cervical Facet Capsule (Upper)");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Thermatome (T1-T2)");

            }

            if(fronthead2 == 1){

                nameList.add("Masseter 3");
                nameList.add("Occipitalis");
                nameList.add("Occipitofrontalis");
                nameList.add("Orbicularis Oculi");
                nameList.add("Semispinalis Capitis 1");
                nameList.add("Sternocleidomastoid 1");
                nameList.add("Sternocleidomastoid 2");
                nameList.add("Temporalis 1");
                nameList.add("Trapezius 1");
                nameList.add("Zygomaticus Major");
                nameList.add("Cervical Facet Capsule (Upper)");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Sclerotome C1");
                nameList.add("Thermatome (T1-T2)");

            }

            if(fronthead3 == 1){

                nameList.add("Masseter 3");
                nameList.add("Occipitalis");
                nameList.add("Semispinalis Capitis 1");
                nameList.add("Sternocleidomastoid 1");
                nameList.add("Temporalis 1");
                nameList.add("Temporalis 2");
                nameList.add("Temporalis 3");
                nameList.add("Temporalis 4");
                nameList.add("Trapezius 1");
                nameList.add("Cervical Facet Capsule (Mid)");
                nameList.add("Cervical Facet Capsule (Upper)");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Sclerotome C1");
                nameList.add("Thermatome (T1-T2)");

            }

            if(fronthead4 == 1){

                nameList.add("Lateral Pterygoid");
                nameList.add("Orbicularis Oculi");
                nameList.add("Splenius Cervicis 2");
                nameList.add("Sternocleidomastoid 1");
                nameList.add("Temporalis 1");
                nameList.add("Temporalis 2");
                nameList.add("Zygomaticus Major");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Thermatome (T1-T2)");

            }

            if(fronthead5 == 1){

                nameList.add("Lateral Pterygoid");
                nameList.add("Masseter 3");
                nameList.add("Masseter 4");
                nameList.add("Medial Pterygoid");
                nameList.add("Splenius Cervicis 2");
                nameList.add("Sternocleidomastoid 1");
                nameList.add("Sternocleidomastoid 2");
                nameList.add("Suboccipital");
                nameList.add("Temporalis 2");
                nameList.add("Trapezius 1");
                nameList.add("Cervical Facet Capsule (Mid)");
                nameList.add("Cervical Facet Capsule (Upper)");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Sclerotome C1");
                nameList.add("Thermatome (T1-T2)");

            }

            if(fronthead6 == 1){

                nameList.add("Buccinator");
                nameList.add("Diagastric 2");
                nameList.add("Masseter 1");
                nameList.add("Masseter 2");
                nameList.add("Masseter 3");
                nameList.add("Masseter 4");
                nameList.add("Medial Pterygoid");
                nameList.add("Platysma");
                nameList.add("Soleus 4");
                nameList.add("Sternocleidomastoid 1");
                nameList.add("Temporalis 3");
                nameList.add("Trapezius 1");
                nameList.add("Mandibular Condyle");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Thermatome (T1-T2)");

            }

            if(fronthead7 == 1){

                nameList.add("Masseter 4");
                nameList.add("Medial Pterygoid");
                nameList.add("Splenius Cervicis 2");
                nameList.add("Sternocleidomastoid 2");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Sclerotome C1");
                nameList.add("Thermatome (T1-T2)");

            }

            if(fronthead8 == 1){

                nameList.add("Diagastric 1");
                nameList.add("Scalene");
                nameList.add("Sternocleidomastoid 1");
                nameList.add("Erb's Point");
                nameList.add("Sternalcostal (1st Rib)");
                nameList.add("Sternoclavicular");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Thermatome (T1-T2)");

            }

            if(fronthead9 == 1){

                nameList.add("Diagastric 1");
                nameList.add("Medial Pterygoid");
                nameList.add("Trapezius 1");
                nameList.add("Transverse Process of Atlas");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Thermatome (T1-T2)");
            }

            getImageData(nameList);

        } else if(post == 1 && (body_part == 6 || body_part == 5 || body_part == 7)){ //front leg

            nameList.clear();

            if(frontleg1 == 1){

                nameList.add("Intercostal");
                nameList.add("Pectoralis Major 1");
                nameList.add("Pectoralis Major 2");
                nameList.add("Pectoralis Major 3");
                nameList.add("Pectoralis Major 4");
                nameList.add("Pectoralis Minor");
                nameList.add("Scalene");
                nameList.add("Serratus Anterior");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Sternalis");
                nameList.add("Subclavius");
                nameList.add("Clavicle (Sternal End)");
                nameList.add("Ribs (Anterior)");
                nameList.add("Costosternal");
                nameList.add("Costotransverse");
                nameList.add("Sternoclavicular");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome T2");
                nameList.add("Sclerotome T6");
                nameList.add("Thermatome (T1-T4)");
                nameList.add("Thermatome (T2-T7)");

            }

            if(frontleg2 == 1){

                nameList.add("Diaphragm");
                nameList.add("Intercostal");
                nameList.add("Serratus Anterior");
                nameList.add("Ribs (Axilla)");
                nameList.add("Dura (Thoracic)");
                nameList.add("Sclerotome T1");
                nameList.add("Sclerotome T3");
                nameList.add("Sclerotome T4");
                nameList.add("Sclerotome T5");
                nameList.add("Sclerotome T6");
                nameList.add("Thermatome (T4-L2)");

            }

            if(frontleg3 == 1){

                nameList.add("Anterior Deltoid");
                nameList.add("Biceps Brachii");
                nameList.add("Brachialis");
                nameList.add("Coracobrachialis");
                nameList.add("Infraspinatus");
                nameList.add("Latissimus Dorsi 2");
                nameList.add("Middle Deltoid");
                nameList.add("Pectoralis Major 2");
                nameList.add("Pectoralis Major 3");
                nameList.add("Pectoralis Minor");
                nameList.add("Scalene");
                nameList.add("Sternalis");
                nameList.add("Subclavius");
                nameList.add("Subscapularis");
                nameList.add("Supraspinatus 1");
                nameList.add("Supraspinatus 2");
                nameList.add("Deltoid Insertion");
                nameList.add("Acromioclavicular");
                nameList.add("Supraspinatus (C5)");
                nameList.add("Dura (Cervicothoracic)");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Thermatome (T2-T7)");

            }

            if(frontleg4 == 1){

                nameList.add("Biceps Brachii");
                nameList.add("Brachialis");
                nameList.add("Infraspinatus");
                nameList.add("Latissimus Dorsi 1");
                nameList.add("Pectoralis Major 2");
                nameList.add("Pectoralis Major 4");
                nameList.add("Pectoralis Minor");
                nameList.add("Scalene");
                nameList.add("Scalenus Minimus");
                nameList.add("Serratus Anterior");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Sternalis");
                nameList.add("Subclavius");
                nameList.add("Subscapularis");
                nameList.add("Supraspinatus 1");
                nameList.add("Supraspinatus (C6)");
                nameList.add("Sclerotome C8");
                nameList.add("Sclerotome T1");
                nameList.add("Thermatome (T1-T4)");
                nameList.add("Thermatome (T2-T7)");

            }

            if(frontleg5 == 1){

                nameList.add("Biceps Brachii");
                nameList.add("Brachialis");
                nameList.add("Infraspinatus");
                nameList.add("Pectoralis Major 2");
                nameList.add("Pectoralis Minor");
                nameList.add("Pronator Teres");
                nameList.add("Scalene");
                nameList.add("Scalenus Minimus");
                nameList.add("Serratus Anterior");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Sternalis");
                nameList.add("Supraspinatus 1");
                nameList.add("Triceps Brachii 5");
                nameList.add("Lateral Epicondyle");
                nameList.add("Medial Epicondyle");
                nameList.add("Radial Collateral");
                nameList.add("Supraspinatus (C6)");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Thermatome (T1-T4)");
                nameList.add("Thermatome (T2-T7)");

            }

            if(frontleg6 == 1){

                nameList.add("Flexor Carpi Radialis");
                nameList.add("Flexor Carpi Ulnaris");
                nameList.add("Infraspinatus");
                nameList.add("Latissimus Dorsi 1");
                nameList.add("Opponens Pollicis");
                nameList.add("Palmaris Longus");
                nameList.add("Pectoralis Major 2");
                nameList.add("Pectoralis Minor");
                nameList.add("Pronator Teres");
                nameList.add("Scalene");
                nameList.add("Serratus Anterior");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Subclavius");
                nameList.add("Subscapularis");
                nameList.add("Triceps Brachii 5");
                nameList.add("Radial Styloid");
                nameList.add("Interosseous Membrane (Forearm)");
                nameList.add("Radial Collateral");
                nameList.add("Supraspinatus (C6)");
                nameList.add("Supraspinatus (C7)");
                nameList.add("Ulnar Collateral");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Thermatome (T2-T7)");

            }

            if(frontleg7 == 1){

                nameList.add("Adductor Pollicis");
                nameList.add("Flexor Carpi Radialis");
                nameList.add("Brachialis");
                nameList.add("First Dorsal Interosseous of the Hand");
                nameList.add("Flexor Digitorum Superficialis");
                nameList.add("Flexor Pollicis Longus");
                nameList.add("Infraspinatus");
                nameList.add("Opponens Pollicis");
                nameList.add("Palmaris Longus");
                nameList.add("Pronator Teres");
                nameList.add("Scalene");
                nameList.add("Serratus Anterior");
                nameList.add("Subclavius");
                nameList.add("Palmer Carpal");
                nameList.add("Thermatome (T2-T7)");

            }

            if(frontleg8 == 1){

                nameList.add("First Dorsal Interosseous of the Hand");
                nameList.add("Subclavius");
                nameList.add("Palmer Carpal");
                nameList.add("Thermatome (T2-T7)");

            }

            if(frontleg9 == 1){

                nameList.add("Flexor Digitorum Superficialis");
                nameList.add("Pectoralis Major 2");
                nameList.add("Subclavius");
                nameList.add("Palmer Carpal");
                nameList.add("Thermatome (T2-T7)");

            }

            if(frontleg10 == 1){

                nameList.add("Flexor Digitorum Profundus");
                nameList.add("Latissimus Dorsi 1");
                nameList.add("Pectoralis Major 2");
                nameList.add("Pectoralis Minor");
                nameList.add("Serratus Anterior");
                nameList.add("Triceps Brachii 5");
                nameList.add("Palmer Carpal");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Thermatome (T2-T7)");

            }

            if(frontleg11 == 1){

                nameList.add("Flexor Carpi Ulnaris");
                nameList.add("Flexor Digitorum Profundus");
                nameList.add("Latissimus Dorsi 1");
                nameList.add("Palmaris Longus");
                nameList.add("Pectoralis Major 2");
                nameList.add("Pectoralis Minor");
                nameList.add("Serratus Anterior");
                nameList.add("Serratus Posterior Superior");
                nameList.add("Triceps Brachii 5");
                nameList.add("Palmer Carpal");
                nameList.add("Ulnar Collateral");
                nameList.add("Sclerotome C7");
                nameList.add("Sclerotome C8");
                nameList.add("Thermatome (T2-T7)");
            }

            getImageData(nameList);

        }else if(post == 2 && body_part == 9){//back foot

            nameList.clear();

            if(backfoot1 == 1){

                nameList.add("Gluteus Medius 3");
                nameList.add("Gluteus Minimus 2");
                nameList.add("Paraspinal (Thoracolumbar)");
                nameList.add("Rectus Abdominis");
                nameList.add("Soleus 3");
                nameList.add("Iliolumbar");
                nameList.add("Lumbar Facet Capsule");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Supraspinatus (S1)");
                nameList.add("Thoracolumbar Facet Capsule");
                nameList.add("Dura (Thoracic)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Facet Lumbar");
                nameList.add("Facet T11-T12");
                nameList.add("Sclerotome L1");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L3");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome L5");
                nameList.add("Sclerotome S1");
                nameList.add("Sclerotome T12");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot2 == 1){

                nameList.add("Gluteus Maximus 2");
                nameList.add("Gluteus Medius 2");
                nameList.add("Gluteus Minimus 1");
                nameList.add("Gluteus Minimus 2");
                nameList.add("Paraspinal (Thoracolumbar)");
                nameList.add("Piriformis 2");
                nameList.add("Quadratus Lumborum 3");
                nameList.add("Quadratus Lumborum 4");
                nameList.add("Vastus Lateralis 4");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Iliolumbar");
                nameList.add("Lumbar Facet Capsule");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Sacrotuberous");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Facet Lumbar");
                nameList.add("Sacroiliac Joint");
                nameList.add("Sclerotome L1");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L3");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome L5");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot3 == 1){

                nameList.add("Gluteus Maximus 1");
                nameList.add("Gluteus Maximus 2");
                nameList.add("Gluteus Medius 1");
                nameList.add("Gluteus Medius 2");
                nameList.add("Gluteus Minimus 1");
                nameList.add("Paraspinal (Thoracolumbar)");
                nameList.add("Piriformis 1");
                nameList.add("Piriformis 2");
                nameList.add("Quadratus Lumborum 1");
                nameList.add("Quadratus Lumborum 3");
                nameList.add("Quadratus Lumborum 4");
                nameList.add("Semimembranosus and Semitendinosus");
                nameList.add("Ischial Tuberosity");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Sacrotuberous");
                nameList.add("Supraspinatus (L3)");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Supraspinatus (S1)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Facet Lumbar");
                nameList.add("Sacroiliac Joint");
                nameList.add("Sclerotome L1");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome L5");
                nameList.add("Sclerotome T12");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot4 == 1){

                nameList.add("Gluteus Maximus 1");
                nameList.add("Gluteus Maximus 2");
                nameList.add("Gluteus Maximus 3");
                nameList.add("Gluteus Medius 1");
                nameList.add("Gluteus Medius 3");
                nameList.add("Gluteus Minimus 2");
                nameList.add("Obturator Internus");
                nameList.add("Pelvic Floor");
                nameList.add("Quadratus Lumborum 3");
                nameList.add("Quadratus Lumborum 4");
                nameList.add("Coccyx");
                nameList.add("Sacrospinous");
                nameList.add("Sacrotuberous");
                nameList.add("Supraspinatus (L3)");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sacroiliac Joint");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L4");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot5 == 1){

                nameList.add("Gluteus Minimus 1");
                nameList.add("Gluteus Minimus 2");
                nameList.add("Vastus Lateralis 1");
                nameList.add("Vastus Lateralis 2");
                nameList.add("Vastus Lateralis 3");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Iliolumbar");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Sacrotuberous");
                nameList.add("Supraspinatus (L3)");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Supraspinatus (S1)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Facet Lumbar");
                nameList.add("Sacroiliac Joint");
                nameList.add("Sclerotome L1");
                nameList.add("Sclerotome L3");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot6 == 1){

                nameList.add("Biceps Femoris");
                nameList.add("Gluteus Medius 2");
                nameList.add("Gluteus Minimus 2");
                nameList.add("Obturator Internus");
                nameList.add("Piriformis 1");
                nameList.add("Piriformis 2");
                nameList.add("Semimembranosus and Semitendinosus");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Sacrotuberous");
                nameList.add("Supraspinatus (L3)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Supraspinatus (S1)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Facet Lumbar");
                nameList.add("Sacroiliac Joint");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L5");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot7 == 1){

                nameList.add("Sacrospinous");
                nameList.add("Sacrotuberous");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (S1)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L4");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot8 == 1){

                nameList.add("Biceps Femoris");
                nameList.add("Gastrocnemius 3");
                nameList.add("Gastrocnemius 4");
                nameList.add("Gluteus Minimus 1");
                nameList.add("Gluteus Minimus 2");
                nameList.add("Plantaris");
                nameList.add("Popliteus");
                nameList.add("Semimembranosus and Semitendinosus");
                nameList.add("Vastus Lateralis 1");
                nameList.add("Vastus Lateralis 2");
                nameList.add("Anterior and Posterior Cruciate");
                nameList.add("Iliolumbar");
                nameList.add("Sacrospinous");
                nameList.add("Supraspinatus (L3)");
                nameList.add("Supraspinatus (S1)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sclerotome L3");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot9 == 1){

                nameList.add("Gastrocnemius 2");
                nameList.add("Gastrocnemius 4");
                nameList.add("Gluteus Minimus 1");
                nameList.add("Vastus Lateralis 2");
                nameList.add("Vastus Lateralis 3");
                nameList.add("Fibular Head");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Iliolumbar");
                nameList.add("Interosseous Membrane (Shin)");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sacroiliac Joint");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot10 == 1){

                nameList.add("Flexor Digitorum Longus");
                nameList.add("Gastrocnemius 1");
                nameList.add("Gastrocnemius 3");
                nameList.add("Gluteus Minimus 2");
                nameList.add("Plantaris");
                nameList.add("Semimembranosus and Semitendinosus");
                nameList.add("Soleus 2");
                nameList.add("Tibialis Posterior");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Interosseous Membrane (Shin)");
                nameList.add("Sacrospinous");
                nameList.add("Sacrotuberous");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sclerotome L3");

            }

            if(backfoot11 == 1){

                nameList.add("Flexor Digitorum Longus");
                nameList.add("Gastrocnemius 1");
                nameList.add("Gluteus Minimus 1");
                nameList.add("Soleus 1");
                nameList.add("Tibialis Posterior");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot12 == 1){

                nameList.add("Extensor Digitorum Brevis");
                nameList.add("Extensor Digitorum Longus");
                nameList.add("Extensor Hallucis Brevis");
                nameList.add("Extensor Hallucis Longus");
                nameList.add("Peroneus Brevis");
                nameList.add("Peroneus Tertius");
                nameList.add("Tibialis Anterior");
                nameList.add("Deltoid");
                nameList.add("Extensor Retinaculum (Ankle)");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Lateral Collateral (Knee)");
                nameList.add("Medial Collateral (Knee)");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Talofibular");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot13 == 1){

                nameList.add("Peroneus Tertius");
                nameList.add("Soleus 1");
                nameList.add("Tibialis Posterior");
                nameList.add("Sacrotuberous");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot14 == 1){

                nameList.add("Soleus 1");
                nameList.add("Tibialis Posterior");

            }

            if(backfoot15 == 1){

                nameList.add("Abductor Hallucis");
                nameList.add("Quadratus Plantae");
                nameList.add("Soleus 1");
                nameList.add("Tibialis Posterior");
                nameList.add("Calcaneal Spur");
                nameList.add("Sacrotuberous");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot16 == 1){

                nameList.add("Abductor Digiti Minimi (foot)");
                nameList.add("Flexor Digitorum Longus");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot17 == 1){

                nameList.add("First Dorsal Interosseous of the Foot");
                nameList.add("Flexor Digitorum Longus");
                nameList.add("Gastrocnemius 1");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot18 == 1){

                nameList.add("Abductor Hallucis");
                nameList.add("Adductor Hallicis");
                nameList.add("Gastrocnemius 1");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot19 == 1){

                nameList.add("Abductor Digiti Minimi (foot)");
                nameList.add("Adductor Hallicis");
                nameList.add("First Dorsal Interosseous of the Foot");
                nameList.add("Flexor Digitorum Brevis");
                nameList.add("Flexor Digitorum Longus");
                nameList.add("Flexor Hallucis Brevis");
                nameList.add("Flexor Hallucis Longus");
                nameList.add("Tibialis Posterior");
                nameList.add("Metatarsal Head");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot20 == 1){

                nameList.add("Flexor Digitorum Longus");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot21 == 1){

                nameList.add("Flexor Digitorum Longus");
                nameList.add("Flexor Hallucis Brevis");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot22 == 1){

                nameList.add("Flexor Hallucis Brevis");
                nameList.add("Flexor Hallucis Longus");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");
            }

            getImageData(nameList);

        }else if(post == 1 && body_part == 9){//front foot

            nameList.clear();

            if(frontfoot1 == 1){

                nameList.add("Lateral Oblique");
                nameList.add("Pectineus");
                nameList.add("Quadratus Lumborum 3");
                nameList.add("Vastus Lateralis 4");
                nameList.add("Lateral Pubic Symphysis");
                nameList.add("Superior Pubic Symphysis");
                nameList.add("Inguinal");
                nameList.add("Lumbar Facet Capsule");
                nameList.add("Pubic Symphysis");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Supraspinatus (S1)");
                nameList.add("Dura (Thoracic)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sacroiliac Joint");
                nameList.add("Sclerotome L1");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome T10");
                nameList.add("Sclerotome T9");
                nameList.add("Thermatome (T11-L2)");
                nameList.add("Thermatome (T4-L2)");

            }

            if(frontfoot2 == 1){

                nameList.add("Quadratus Lumborum 3");
                nameList.add("Sartorius");
                nameList.add("Tensor Fasciae Latae");
                nameList.add("Vastus Intermedius");
                nameList.add("Vastus Lateralis 1");
                nameList.add("Vastus Lateralis 2");
                nameList.add("Vastus Lateralis 3");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Iliolumbar");
                nameList.add("Inguinal");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Supraspinatus (L3)");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Supraspinatus (S1)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L3");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome L5");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(frontfoot3 == 1){

                nameList.add("Adductor Longus and Brevis");
                nameList.add("Iliopsoas");
                nameList.add("Pectineus");
                nameList.add("Rectus Femoris");
                nameList.add("Sartorius");
                nameList.add("Vastus Intermedius");
                nameList.add("Vastus Lateralis 5");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Iliolumbar");
                nameList.add("Inguinal");
                nameList.add("Supraspinatus (L3)");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Supraspinatus (S1)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L3");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome L5");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(frontfoot4 == 1){

                nameList.add("Adductor Longus and Brevis");
                nameList.add("Adductor Magnus");
                nameList.add("Gracilis");
                nameList.add("Sartorius");
                nameList.add("Vastus Intermedius");
                nameList.add("Vastus Medialis 1");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Iliolumbar");
                nameList.add("Inguinal");
                nameList.add("Pubic Symphysis");
                nameList.add("Sacrospinous");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(frontfoot5 == 1){

                nameList.add("Adductor Longus and Brevis");
                nameList.add("Rectus Femoris");
                nameList.add("Sartorius");
                nameList.add("Vastus Lateralis 2");
                nameList.add("Vastus Lateralis 3");
                nameList.add("Vastus Lateralis 5");
                nameList.add("Vastus Medialis 1");
                nameList.add("Vastus Medialis 2");
                nameList.add("Knee Joint Line");
                nameList.add("Pes Anserine");
                nameList.add("Superior Patella");
                nameList.add("Anterior and Posterior Cruciate");
                nameList.add("Iliolumbar");
                nameList.add("Medial Collateral (Knee)");
                nameList.add("Sacroiliac");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sclerotome L2");
                nameList.add("Sclerotome L3");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome L5");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(frontfoot6 == 1){

                nameList.add("Extensor Digitorum Longus");
                nameList.add("Extensor Hallucis Longus");
                nameList.add("Peroneus Brevis");
                nameList.add("Peroneus Longus");
                nameList.add("Vastus Lateralis 2");
                nameList.add("Vastus Lateralis 3");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Interosseous Membrane (Shin)");
                nameList.add("Lateral Collateral (Knee)");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Sacroiliac Joint");
                nameList.add("Sclerotome L3");
                nameList.add("Sclerotome L4");
                nameList.add("Sclerotome L5");
                nameList.add("Sclerotome S1");
                nameList.add("Thermatome (T11-L2)");

            }

            if(frontfoot7 == 1){

                nameList.add("Adductor Longus and Brevis");
                nameList.add("Tibialis Anterior");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Interosseous Membrane (Shin)");
                nameList.add("Medial Collateral (Knee)");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Supraspinatus (L4)");
                nameList.add("Supraspinatus (L5)");
                nameList.add("Dura (Thoracolumbar)");
                nameList.add("Thermatome (T11-L2)");

            }

            if(frontfoot8 == 1){

                nameList.add("Extensor Digitorum Brevis");
                nameList.add("Extensor Digitorum Longus");
                nameList.add("Extensor Hallucis Brevis");
                nameList.add("Extensor Hallucis Longus");
                nameList.add("Peroneus Brevis");
                nameList.add("Peroneus Tertius");
                nameList.add("Tibialis Anterior");
                nameList.add("Deltoid");
                nameList.add("Extensor Retinaculum (Ankle)");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Lateral Collateral (Knee)");
                nameList.add("Medial Collateral (Knee)");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Talofibular");
                nameList.add("Thermatome (T11-L2)");

            }

            if(frontfoot9 == 1){

                nameList.add("Lateral Collateral (Knee)");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Talofibular");
                nameList.add("Thermatome (T11-L2)");

            }

            if(frontfoot10 == 1){

                nameList.add("Extensor Digitorum Longus");
                nameList.add("First Dorsal Interosseous of the Foot");
                nameList.add("Deltoid");
                nameList.add("Lateral Collateral (Knee)");
                nameList.add("Sacroiliac");
                nameList.add("Sacrospinous");
                nameList.add("Talofibular");
                nameList.add("Thermatome (T11-L2)");

            }

            if(frontfoot11 == 1){

                nameList.add("Extensor Hallucis Longus");
                nameList.add("Flexor Hallucis Brevis");
                nameList.add("Tibialis Anterior");
                nameList.add("Deltoid");
                nameList.add("Femoralacetabular Joint Capsule");
                nameList.add("Medial Collateral (Knee)");
                nameList.add("Sacrospinous");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot15 == 1){

                nameList.add("Abductor Hallucis");
                nameList.add("Quadratus Plantae");
                nameList.add("Soleus 1");
                nameList.add("Tibialis Posterior");
                nameList.add("Calcaneal Spur");
                nameList.add("Sacrotuberous");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot16 == 1){

                nameList.add("Abductor Digiti Minimi (foot)");
                nameList.add("Flexor Digitorum Longus");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot17 == 1){

                nameList.add("First Dorsal Interosseous of the Foot");
                nameList.add("Flexor Digitorum Longus");
                nameList.add("Gastrocnemius 1");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot18 == 1){

                nameList.add("Abductor Hallucis");
                nameList.add("Adductor Hallicis");
                nameList.add("Gastrocnemius 1");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot19 == 1){

                nameList.add("Abductor Digiti Minimi (foot)");
                nameList.add("Adductor Hallicis");
                nameList.add("First Dorsal Interosseous of the Foot");
                nameList.add("Flexor Digitorum Brevis");
                nameList.add("Flexor Digitorum Longus");
                nameList.add("Flexor Hallucis Brevis");
                nameList.add("Flexor Hallucis Longus");
                nameList.add("Tibialis Posterior");
                nameList.add("Metatarsal Head");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot20 == 1){

                nameList.add("Flexor Digitorum Longus");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot21 == 1){

                nameList.add("Flexor Digitorum Longus");
                nameList.add("Flexor Hallucis Brevis");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");

            }

            if(backfoot22 == 1){

                nameList.add("Flexor Hallucis Brevis");
                nameList.add("Flexor Hallucis Longus");
                nameList.add("Tibialis Posterior");
                nameList.add("Thermatome (T11-L2)");
            }

            getImageData(nameList);
        }

        storeAdapter.setUserDatas(_images);
        storeAdapter.notifyDataSetChanged();


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:

                    fronthead1 = 0; fronthead2 =0; fronthead3 = 0; fronthead4 = 0; fronthead5 = 0; fronthead6 = 0;
                    frontbody1 = 0; frontbody2 = 0; frontbody3 = 0;
                    frontleg1 = 0; frontleg2 = 0; frontleg3 = 0; frontleg4 = 0; frontleg5 = 0; frontleg6 = 0; frontleg7 = 0; frontleg8 = 0; frontleg9 = 0; frontleg10 = 0; frontleg11 = 0;
                    frontfoot1 = 0; frontfoot2 = 0; frontfoot3 = 0; frontfoot4 = 0; frontfoot5 = 0; frontfoot6 = 0; frontfoot7 = 0; frontfoot8 = 0; frontfoot9 = 0; frontfoot10 = 0; frontfoot11 = 0;
                    backhead1 = 0; backhead2 = 0; backhead3 = 0; backhead4 = 0; backhead5 = 0; backhead6 = 0;
                    backbody1 = 0; backbody2 = 0; backbody3 = 0; backbody4 = 0;
                    backleg1 = 0; backleg2 = 0; backleg3 = 0; backleg4 = 0; backleg5 = 0; backleg6 = 0; backleg7 = 0; backleg8 = 0; backleg9 = 0; backleg10 = 0; backleg11 = 0; backleg12 = 0; backleg13 = 0;
                    backfoot1 = 0; backfoot2 = 0; backfoot3 = 0; backfoot4 = 0; backfoot5 = 0; backfoot6 = 0; backfoot7 = 0; backfoot8 = 0; backfoot9 = 0; backfoot10 = 0; backfoot11 = 0; backfoot12 = 0; backfoot13 = 0; backfoot14 = 0; backfoot15 = 0; backfoot16 = 0; backfoot17 = 0; backfoot18 = 0; backfoot19 = 0; backfoot20 = 0; backfoot21 = 0; backfoot22 = 0;

                    finish();
                    break;
        }

    }
}

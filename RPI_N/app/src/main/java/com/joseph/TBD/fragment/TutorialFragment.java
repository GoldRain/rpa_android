package com.joseph.TBD.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.adapter.ImagePreviewAdapter;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.OrientationActivity;

import java.util.ArrayList;

public class TutorialFragment extends Fragment {

    OrientationActivity _activity;

    ViewPager ui_viewPager;
    ImagePreviewAdapter _adapter;
    TextView ui_txvImageNo;

    ArrayList<Integer> _imagePaths = new ArrayList<>();
    int _position = 0;

    View view;

    public TutorialFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tutorial, container, false);

 /*       _imagePaths = getIntent().getStringArrayListExtra(Constants.KEY_IMAGEPATH);
        _position = getIntent().getIntExtra(Constants.KEY_POSITION,0);*/


        loadImages();
        loadlayout();
        return view;
    }

    private void loadImages(){

        _imagePaths.add(R.drawable.slide1); _imagePaths.add(R.drawable.slide2); _imagePaths.add(R.drawable.slide3); _imagePaths.add(R.drawable.slide4);_imagePaths.add(R.drawable.slide5);
        _imagePaths.add(R.drawable.slide6);  _imagePaths.add(R.drawable.slide7); _imagePaths.add(R.drawable.slide8);_imagePaths.add(R.drawable.slide9); _imagePaths.add(R.drawable.slide10);
        _imagePaths.add(R.drawable.slide11);  _imagePaths.add(R.drawable.slide12); _imagePaths.add(R.drawable.slide13);_imagePaths.add(R.drawable.slide14);
    }

    private void loadlayout() {

        ui_txvImageNo = (TextView)view.findViewById(R.id.txv_timeline_no);

        ui_viewPager = (ViewPager)view.findViewById(R.id.viewpager);
        _adapter = new ImagePreviewAdapter(_activity);
        ui_viewPager.setAdapter(_adapter);
        _adapter.setDatas(_imagePaths);
        ui_viewPager.setCurrentItem(_position);

        ui_txvImageNo.setText((_position + 1) + " / " + _imagePaths.size());

        ui_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ui_txvImageNo.setText((position + 1) + " / " + _imagePaths.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (OrientationActivity)context;
    }
}

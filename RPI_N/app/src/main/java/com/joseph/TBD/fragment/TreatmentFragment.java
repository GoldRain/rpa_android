package com.joseph.TBD.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.base.BaseFragment;
import com.joseph.TBD.main.MainActivity;

import java.io.InputStream;


public class TreatmentFragment extends BaseFragment {

    MainActivity _activity ;
    View view ;

    TextView ui_txvGlossary;
    String result;

    public TreatmentFragment(MainActivity activity) {
        // Required empty public constructor

        this._activity = activity ;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_treatment, container, false);

        loadLayout();
        return view ;
    }

    private void loadLayout() {

        ui_txvGlossary = (TextView)view.findViewById(R.id.txv_glossary);
        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.glossary);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);
        } catch (Exception e) {
            result = "Error : can't show file."  ;
        }

        ui_txvGlossary.setText(result.toString());

    }

//    public static class BlankFragment extends BaseFragment {
//
//        MainActivity _activity ;
//        View view ;
//
//        public BlankFragment(MainActivity activity) {
//            // Required empty public constructor
//            this._activity = activity ;
//        }
//
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            // Inflate the layout for this fragment
//            view = inflater.inflate(R.layout.fragment_blank, container, false);
//
//            loadLayout();
//
//            return view ;
//        }
//
//        private void loadLayout() {
//
//        }
//
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }
}

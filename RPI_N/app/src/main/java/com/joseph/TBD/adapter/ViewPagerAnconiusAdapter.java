package com.joseph.TBD.adapter;

import android.content.Context;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.joseph.TBD.R;
import com.joseph.TBD.main.DetailsActivity.AnconiusDetailsActivity;
import com.joseph.TBD.main.PainSelcection.PainSelectDetaileActivity;
import com.joseph.TBD.model.ItemModel;

import java.util.ArrayList;

import static com.joseph.TBD.commons.Constants.array_image1;
import static com.joseph.TBD.commons.Constants.array_imageLigament;
import static com.joseph.TBD.commons.Constants.array_imagePeriosteal1;
import static com.joseph.TBD.commons.Constants.joint_image;


public class ViewPagerAnconiusAdapter extends PagerAdapter {

    private ArrayList<ItemModel> _userDatas = new ArrayList<>();
    private ArrayList<ItemModel> _allUserDatas = new ArrayList<>();

    Context mContext;
    LayoutInflater mLayoutInflater;

    public ViewPagerAnconiusAdapter(PainSelectDetaileActivity context) {


        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setUserDatas(ArrayList<ItemModel> users) {

        _allUserDatas = users;
        _userDatas.clear();
        _userDatas.addAll(_allUserDatas);
    }

    @Override
    public int getCount() {

        return _userDatas.size();
    }



    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {

        final View itemView = mLayoutInflater.inflate(R.layout.fragment_model, container, false);

        final ItemModel user =  _userDatas.get(position);


        final ImageView ui_imvImage = (ImageView) itemView.findViewById(R.id.imv_image);


        final int position1 = user.getPosition();
        final int select_status = user.getSelect_status();

        if(select_status == 1) {
            ui_imvImage.setImageResource(array_image1.get(position1));
        } else if(select_status == 4){
            ui_imvImage.setImageResource(array_imagePeriosteal1.get(position1));
        } else if(select_status == 2){
            ui_imvImage.setImageResource(array_imageLigament.get(position1));
        } else if(select_status == 3){
            ui_imvImage.setImageResource(joint_image.get(position1));
        }


        ui_imvImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, AnconiusDetailsActivity.class);
                mContext.startActivity(intent);
            }

        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}

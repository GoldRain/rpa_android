package com.joseph.TBD.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joseph.TBD.R;
import com.joseph.TBD.base.BaseFragment;
import com.joseph.TBD.main.MainActivity;

public class PainFragment extends BaseFragment {

    MainActivity _activity ;

    View view ;

    public PainFragment(MainActivity activity) {
        // Required empty public constructor

        this._activity = activity ;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_pain, container, false);

        loadLayout();

        return view ;
    }

    private void loadLayout() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}

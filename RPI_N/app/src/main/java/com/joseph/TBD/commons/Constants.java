package com.joseph.TBD.commons;



import java.util.ArrayList;


public class Constants {


    public static final int SPLASH_TIME = 1500 ;
    public static final int PROFILE_IMAGE_SIZE = 256 ;

    public static final String POSITION = "position";

    public static int screenheight = 0;
    public static int screenwidth = 0;



    public static int activitystatus=0;
    public static int anconiusstatus = 0;

    public static int select_Status = 0;
    public static int position = 0;

    public static  int post = 1 ;
    public static  int body_part = 1;

    public static  int frontbody1 = 0;
    public static  int frontbody2 = 0;
    public static  int frontbody3 = 0;

    public static int backhead1 = 0;
    public static int backhead2 = 0;
    public static int backhead3 = 0;
    public static int backhead4 = 0;
    public static int backhead5 = 0;
    public static int backhead6 = 0;

    public static int backbody1 = 0;
    public static int backbody2 = 0;
    public static int backbody3 = 0;
    public static int backbody4 = 0;

    public static int backleg1 = 0;
    public static int backleg2 = 0;
    public static int backleg3 = 0;
    public static int backleg4 = 0;
    public static int backleg5 = 0;
    public static int backleg6 = 0;
    public static int backleg7 = 0;
    public static int backleg8 = 0;
    public static int backleg9 = 0;
    public static int backleg10 = 0;
    public static int backleg11 = 0;
    public static int backleg12 = 0;
    public static int backleg13 = 0;

    public static int fronthead1 = 0;
    public static int fronthead2 = 0;
    public static int fronthead3 = 0;
    public static int fronthead4 = 0;
    public static int fronthead5 = 0;
    public static int fronthead6 = 0;
    public static int fronthead7 = 0;
    public static int fronthead8 = 0;
    public static int fronthead9 = 0;

    public static int frontleg1 = 0;
    public static int frontleg2 = 0;
    public static int frontleg3 = 0;
    public static int frontleg4 = 0;
    public static int frontleg5 = 0;
    public static int frontleg6 = 0;
    public static int frontleg7 = 0;
    public static int frontleg8 = 0;
    public static int frontleg9 = 0;
    public static int frontleg10 = 0;
    public static int frontleg11 = 0;

    public static int backfoot1 = 0;
    public static int backfoot2 = 0;
    public static int backfoot3 = 0;
    public static int backfoot4 = 0;
    public static int backfoot5 = 0;
    public static int backfoot6 = 0;
    public static int backfoot7 = 0;
    public static int backfoot8 = 0;
    public static int backfoot9 = 0;
    public static int backfoot10 = 0;
    public static int backfoot11 = 0;
    public static int backfoot12 = 0;
    public static int backfoot13 = 0;
    public static int backfoot14 = 0;
    public static int backfoot15 = 0;
    public static int backfoot16 = 0;
    public static int backfoot17 = 0;
    public static int backfoot18 = 0;
    public static int backfoot19 = 0;
    public static int backfoot20 = 0;
    public static int backfoot21 = 0;
    public static int backfoot22 = 0;

    public static int frontfoot1 = 0;
    public static int frontfoot2 = 0;
    public static int frontfoot3 = 0;
    public static int frontfoot4 = 0;
    public static int frontfoot5 = 0;
    public static int frontfoot6 = 0;
    public static int frontfoot7 = 0;
    public static int frontfoot8 = 0;
    public static int frontfoot9 = 0;
    public static int frontfoot10 = 0;
    public static int frontfoot11 = 0;

    public static ArrayList<String> muscleList = new ArrayList<>();
    public static ArrayList<Integer> array_image1 = new ArrayList<>();
    public static ArrayList<String> muscleDysfunction = new ArrayList<>();
    public static ArrayList<String> muscleAssessment = new ArrayList<>();
    public static ArrayList<String> muscleDifferential = new ArrayList<>();
    public static ArrayList<String> muscleAssociated = new ArrayList<>();

    public static ArrayList<String> periostealList = new ArrayList<>();
    public static ArrayList<Integer> array_imagePeriosteal1 = new ArrayList<>();
    public static ArrayList<String> periostealCauseTenision = new ArrayList<>();
    public static ArrayList<String> periostealAssessment = new ArrayList<>();
    public static ArrayList<String> periostealDifferential = new ArrayList<>();
    public static ArrayList<String> periostealAssociated = new ArrayList<>();


    public static ArrayList<String> array_ligamentList = new ArrayList<>();
    public static ArrayList<Integer> array_imageLigament = new ArrayList<>();
    public static ArrayList<String> ligamentDysfunction = new ArrayList<>();
    public static ArrayList<String> ligamentAssessment = new ArrayList<>();
    public static ArrayList<String> ligamentDifferential = new ArrayList<>();
    public static ArrayList<String> ligamentAssociated = new ArrayList<>();

    public static ArrayList<String> jointList = new ArrayList<>();
    public static ArrayList<Integer> joint_image = new ArrayList<>();
    public static ArrayList<String> jointDysfunction = new ArrayList<>();
    public static ArrayList<String> jointAssossment = new ArrayList<>();
    public static ArrayList<String> jointDifferential = new ArrayList<>();
    public static ArrayList<String> jointAssociated = new ArrayList<>();

}

package com.joseph.TBD.main.DetailsActivity;

import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.joseph.TBD.R;

import com.joseph.TBD.base.CommonActivity;

import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.PainSelcection.PainSelectActivity;
import com.joseph.TBD.main.PainSelcection.PainSelectDetaileActivity;

import static com.joseph.TBD.commons.Constants.anconiusstatus;
import static com.joseph.TBD.commons.Constants.array_image1;
import static com.joseph.TBD.commons.Constants.array_imageLigament;
import static com.joseph.TBD.commons.Constants.array_imagePeriosteal1;
import static com.joseph.TBD.commons.Constants.array_ligamentList;
import static com.joseph.TBD.commons.Constants.jointAssociated;
import static com.joseph.TBD.commons.Constants.jointDifferential;
import static com.joseph.TBD.commons.Constants.jointList;
import static com.joseph.TBD.commons.Constants.joint_image;
import static com.joseph.TBD.commons.Constants.ligamentDifferential;
import static com.joseph.TBD.commons.Constants.muscleAssociated;
import static com.joseph.TBD.commons.Constants.muscleDifferential;
import static com.joseph.TBD.commons.Constants.muscleDysfunction;
import static com.joseph.TBD.commons.Constants.muscleList;
import static com.joseph.TBD.commons.Constants.periostealAssessment;
import static com.joseph.TBD.commons.Constants.periostealCauseTenision;
import static com.joseph.TBD.commons.Constants.periostealDifferential;
import static com.joseph.TBD.commons.Constants.periostealList;
import static com.joseph.TBD.commons.Constants.position;

public class AnconiusDetailsActivity extends CommonActivity implements View.OnClickListener {



    ImageView ui_imvBack, ui_imagedetail;
    TextView ui_txvTitle;

    TextView txv_trigger_content, ui_txvDysfunction, ui_txvAssessment, ui_txvDifferential, ui_txvAssociated;
    TextView txv_title_trigger_point, txv_title_dysfunction, txv_title_assessment, txv_title_differential, txv_title_associated ;
    TextView txv_title_tension, txv_tension_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anconius_details);

        loadLayout();
    }

    private void loadLayout() {

        if(muscleDifferential.size() < 1){

            muscleDifferential.add("");                                ///A
            muscleDifferential.add("Other Lower Extremity Trigger Points, Plantar Fasciitis, Structural Problems in the foot, Compartment Syndrome");
            muscleDifferential.add("Hand Strain, Ulnar Nerve Injury");
            muscleDifferential.add("Other Lower Extremity Trigger Points, Plantar Fasciitis, Structural Problems in the foot, Compartment Syndrome");
            muscleDifferential.add("Other Lower Extremity Trigger Points");
            muscleDifferential.add("Hernia, Neuralgia, Symphysitis, Pelvic Organ Disease");
            muscleDifferential.add("Hernia, Neuralgia, Symphysitis, Pelvic Organ Disease");
            muscleDifferential.add("De Quervain's Syndrome, Carpal Tunnel Syndrome");
            muscleDifferential.add("Epicondylitis");
            muscleDifferential.add("Adhesive Capsulitis, Biceps Brachii Strain");
            muscleDifferential.add("");                                ///B
            muscleDifferential.add("Bicipital Tendonitis");
            muscleDifferential.add("Sciatica, Disc Dysfunction, Knee Joint Disease");
            muscleDifferential.add("Thoracic Outlet Syndrome, Cervical Syndrome");
            muscleDifferential.add("Thoracic Outlet Syndrome, Cervical Syndrome");
            muscleDifferential.add("Jaw Dysfunction, Dental Issues, Sinus Problems");
            muscleDifferential.add("");                                ///C
            muscleDifferential.add("Cervical Syndrome");
            muscleDifferential.add("");                               ////D
            muscleDifferential.add("Jaw Dysfunction, Dental Issues");
            muscleDifferential.add("Jaw Dysfunction, Dental Issues");
            muscleDifferential.add("Rib Dysfunction");
            muscleDifferential.add("");                               ///E
            muscleDifferential.add("Epicondylitis");
            muscleDifferential.add("Epicondylitis");
            muscleDifferential.add("Epicondylitis");
            muscleDifferential.add("Thoracic Outlet Syndrome, Cervical Syndrome");
            muscleDifferential.add("Thoracic Outlet Syndrome, Cervical Syndrome");
            muscleDifferential.add("Other Lower Extremity Trigger Points, Injury to Synovial Joints of the Tarsals");
            muscleDifferential.add("Other Lower Extremity Trigger Points, Injury to Synovial Joints of the Tarsals");
            muscleDifferential.add("Other Lower Extremity Trigger Points, Injury to Synovial Joints of the Tarsals");
            muscleDifferential.add("Other Lower Extremity Trigger Points, Injury to Synovial Joints of the Tarsals");
            muscleDifferential.add("Wrist Strain");
            muscleDifferential.add("Gastrointestinal Issues");
            muscleDifferential.add("");                                 ///F
            muscleDifferential.add("Other Lower Extremity Trigger Points, Hammer Toe, Morton's Neuroma");
            muscleDifferential.add("Carpal Tunnel Syndrome, Cervical Radiculopathy, Dorsal Hand Injury");
            muscleDifferential.add("Carpal Tunnel Syndrome");
            muscleDifferential.add("Ulnar Nerve Injury");
            muscleDifferential.add("Medial Ankle Injury, Tarsal Tunnel Syndrome, Other Lower Extremity Trigger Points");
            muscleDifferential.add("Medial Ankle Injury, Tarsal Tunnel Syndrome, Other Lower Extremity Trigger Points");
            muscleDifferential.add("Ulnar Nerve Injury");
            muscleDifferential.add("Carpal Tunnel Syndrome");
            muscleDifferential.add("Medial Ankle Injury, Tarsal Tunnel Syndrome, Other Lower Extremity Trigger Points");
            muscleDifferential.add("Medial Ankle Injury, Tarsal Tunnel Syndrome, Other Lower Extremity Trigger Points");
            muscleDifferential.add("Cervical Syndrome, Carpal Tunnel Syndrome");
            muscleDifferential.add("");                                 ///G
            muscleDifferential.add("Lumbar Radiculopathy, Compartment Syndrome, Achilles Tendonitis, Retrocalcaneal Bursitis");
            muscleDifferential.add("Lumbar Radiculopathy, Compartment Syndrome, Achilles Tendonitis, Retrocalcaneal Bursitis");
            muscleDifferential.add("Lumbar Radiculopathy, Compartment Syndrome, Achilles Tendonitis, Retrocalcaneal Bursitis");
            muscleDifferential.add("Lumbar Radiculopathy, Compartment Syndrome, Achilles Tendonitis, Retrocalcaneal Bursitis");
            muscleDifferential.add("Sciatica, Trochanteric Injury, Sacroiliac Joint Dysfunction");
            muscleDifferential.add("Sciatica, Trochanteric Injury, Sacroiliac Joint Dysfunction");
            muscleDifferential.add("Sciatica, Trochanteric Injury, Sacroiliac Joint Dysfunction");
            muscleDifferential.add("Sacroiliac Joint Dysfunction, Lumbar Facet Dysfunction, Intermittent Claudication");
            muscleDifferential.add("Sacroiliac Joint Dysfunction, Lumbar Facet Dysfunction, Intermittent Claudication");
            muscleDifferential.add("Sacroiliac Joint Dysfunction, Lumbar Facet Dysfunction, Intermittent Claudication");
            muscleDifferential.add("Sacroiliac Joint Dysfunction, Lumbar Facet Dysfunction, Intermittent Claudication");
            muscleDifferential.add("Sacroiliac Joint Dysfunction, Lumbar Facet Dysfunction, Intermittent Claudication");
            muscleDifferential.add("Hernia, Neuralgia, Symphysitis, Pelvic Organ Disease");
            muscleDifferential.add("");                                 ///I
            muscleDifferential.add("Other Hip Muscle Dysfunction, Lumbar Disc Syndrome");
            muscleDifferential.add("Shoulder Osteoarthritis, Bicipital Tendonitis");
            muscleDifferential.add("Postherpetic Neuralgia, Tietze's Syndrome");
            muscleDifferential.add("");                                 ///L
            muscleDifferential.add("Visceral Disease, Hernia");
            muscleDifferential.add("TMJ Dysfunction, Trigeminal Neuralgia");
            muscleDifferential.add("Thoracic Outlet Syndrome");
            muscleDifferential.add("Thoracic Outlet Syndrome");
            muscleDifferential.add("Scapulocostal Syndrome");
            muscleDifferential.add("");                                 ///M
            muscleDifferential.add("Atypical Facial Neuralgia, Earache, TMJ Dysfunction, Tension Headache");
            muscleDifferential.add("Atypical Facial Neuralgia, Earache, TMJ Dysfunction, Tension Headache");
            muscleDifferential.add("Atypical Facial Neuralgia, Earache, TMJ Dysfunction, Tension Headache");
            muscleDifferential.add("Atypical Facial Neuralgia, Earache, TMJ Dysfunction, Tension Headache");
            muscleDifferential.add("Jaw Dysfunction, Eustachian Tube Dysfunction");
            muscleDifferential.add("Subacromial Bursitis");
            muscleDifferential.add("Tension Headache");
            muscleDifferential.add("Segmental Spinal Dysfunction");
            muscleDifferential.add("");                                 ///O
            muscleDifferential.add("Coccyx Dysfunction, Sacral Trauma, Sacral Fracture");
            muscleDifferential.add("Tension Headache, Cervicogenic Headache");
            muscleDifferential.add("Tension Headache, Cervicogenic Headache");
            muscleDifferential.add("Cervical Syndrome, Carpal Tunnel Syndrome");
            muscleDifferential.add("Tension Headache, Sinus Problems");
            muscleDifferential.add("");                                 ///P
            muscleDifferential.add("Thoracic Outlet Sydrome, Cervical Syndrome");
            muscleDifferential.add("Back Pain");
            muscleDifferential.add("Back Pain");
            muscleDifferential.add("Obtruator Nerve Entrapment, Hip joint Disease, Osteitis Pubis");
            muscleDifferential.add("Atypical Angina, Tietze's Syndrome, Thoracic Outlet Syndrome");
            muscleDifferential.add("Atypical Angina, Tietze's Syndrome, Thoracic Outlet Syndrome");
            muscleDifferential.add("Atypical Angina, Tietze's Syndrome, Thoracic Outlet Syndrome");
            muscleDifferential.add("Atypical Angina, Tietze's Syndrome, Thoracic Outlet Syndrome");
            muscleDifferential.add("Cervical Radiculopathy, Thoracic Outlet Syndrome");
            muscleDifferential.add("Coccyx Dysfunction, Sacral Trauma, Sacral Fracture");
            muscleDifferential.add("Other Lower Extremity Trigger points, Arthritis of the Ankle Joint");
            muscleDifferential.add("Other Lower Extremity Trigger points, Arthritis of the Ankle Joint");
            muscleDifferential.add("Other Lower Extremity Trigger points, Arthritis of the Ankle Joint");
            muscleDifferential.add("Disc Herniation, Sacroiliac Joint Dysfunction");
            muscleDifferential.add("Disc Herniation, Sacroiliac Joint Dysfunction");
            muscleDifferential.add("Calcaneal Spur, Achilles Tendonitis, Calcaneal Stress Fracture, Nerve Entrapment, Fat Pad Syndrome");
            muscleDifferential.add("Jaw Dysfunction");
            muscleDifferential.add("Baker's Cyst, Knee Instability, Knee Joint Disease, Meniscus Injury");
            muscleDifferential.add("Rotator Cuff Tendonopathy");
            muscleDifferential.add("Cervical Syndrome, Carpal Tunnel Syndrome");
            muscleDifferential.add("");                                 ///Q
            muscleDifferential.add("Trochanteric Bursitis, Sciatica or Disc Syndrome, Visceral Pathology, Somatic Spinal Pathology");
            muscleDifferential.add("Trochanteric Bursitis, Sciatica or Disc Syndrome, Visceral Pathology, Somatic Spinal Pathology");
            muscleDifferential.add("Trochanteric Bursitis, Sciatica or Disc Syndrome, Visceral Pathology, Somatic Spinal Pathology");
            muscleDifferential.add("Trochanteric Bursitis, Sciatica or Disc Syndrome, Visceral Pathology, Somatic Spinal Pathology");
            muscleDifferential.add("Other Lower Extremity Trigger Points");
            muscleDifferential.add("");                                 ///R
            muscleDifferential.add("Appendicitis, Back Pain, Dysmenorrhea");
            muscleDifferential.add("Patellofemoral Syndrome");
            muscleDifferential.add("Thoracic Rib Dysfunction, Rotator Cuff Injury");
            muscleDifferential.add("");                                 ///S
            muscleDifferential.add("Knee Joint Dysfunction, Quadriceps Syndrome");
            muscleDifferential.add("Cervical Radiculopathy, Scapulocostal Syndrome, Thoracic Outlet Syndrome");
            muscleDifferential.add("Cervical Radiculopathy, Scapulocostal Syndrome, Thoracic Outlet Syndrome");
            muscleDifferential.add("Cervical Syndrome, Carpal Tunnel Syndrome");
            muscleDifferential.add("Sciatica, Disc Dysfunction, Knee Joint Disease");
            muscleDifferential.add("Tension Headache");
            muscleDifferential.add("Tension Headache");
            muscleDifferential.add("Postherpetic Neuralgia");
            muscleDifferential.add("Low Back Pain");
            muscleDifferential.add("Thoracic Outlet Syndrome, Cervical Syndrome");
            muscleDifferential.add("Calcaneal Spur, Achilles Tendonitis, Calcaneal Stress Fracture, Nerve Entrapment, Fat Pad Syndrome");
            muscleDifferential.add("Calcaneal Spur, Achilles Tendonitis, Calcaneal Stress Fracture, Nerve Entrapment, Fat Pad Syndrome");
            muscleDifferential.add("Lumbar Dysfunction, Sacroiliac Joint Dysfunction");
            muscleDifferential.add("TMJ Dysfunction, Dysfunction of Muscles of Mastication");
            muscleDifferential.add("Tension Headache, Cervicogenic Headache");
            muscleDifferential.add("Tension Headache, Cervicogenic Headache");
            muscleDifferential.add("Angina, Cardiovascular Issues");
            muscleDifferential.add("Atypical Facial Neuralgia, Atypical Migrane, Tension Headache");
            muscleDifferential.add("Atypical Migrane, Tension Headache");
            muscleDifferential.add("Cervical Syndrome, Carpal Tunnel Syndrome");
            muscleDifferential.add("Tension Headache");
            muscleDifferential.add("Adhesive Capsulitis, Thoracic Outlet Syndrome");
            muscleDifferential.add("Epicondylitis");
            muscleDifferential.add("Cervical Syndrome, Thoracic Outlet Syndrome");
            muscleDifferential.add("Adhesive Capsulitis, Thoracic Outlet Syndrome");
            muscleDifferential.add("");                                 ///T
            muscleDifferential.add("Atypical Facial Neuralgia, Atypical Migrane, Tension Headache, Dental Issues, TMJ Dysfunction");
            muscleDifferential.add("Atypical Facial Neuralgia, Atypical Migrane, Tension Headache, Dental Issues, TMJ Dysfunction");
            muscleDifferential.add("Atypical Facial Neuralgia, Atypical Migrane, Tension Headache, Dental Issues, TMJ Dysfunction");
            muscleDifferential.add("Atypical Facial Neuralgia, Atypical Migrane, Tension Headache, Dental Issues, TMJ Dysfunction");
            muscleDifferential.add("Lumbar Radiculopathy, Meralgia Paresthetica, Trochanteric Bursitis");
            muscleDifferential.add("Thoracic Outlet Syndrome, Cervical Syndrome");
            muscleDifferential.add("Cervical Syndrome");
            muscleDifferential.add("Other Lower Extremity Trigger points, Great Toe Osteoarthritis, Lumbar Radiculopathy");
            muscleDifferential.add("Lumbar Radiculopathy, Calcaneal Spur, Achilles Tendonitis, Calcaneal Stress Fracture, Nerve Entrapment, Fat Pad Syndrome");
            muscleDifferential.add("Atypical Facial Neuralgia, Tension Headache, Chronic Neck and Back Pain");
            muscleDifferential.add("Chronic Neck and Back Pain");
            muscleDifferential.add("Scapulocostal Syndrome, Chronic Neck and Back Pain, Shoulder Bursitis");
            muscleDifferential.add("Scapulocostal Syndrome, Chronic Neck and Back Pain, Shoulder Bursitis");
            muscleDifferential.add("Scapulocostal Syndrome, Chronic Neck and Back Pain, Shoulder Bursitis");
            muscleDifferential.add("Scapulocostal Syndrome, Chronic Neck and Back Pain, Shoulder Bursitis");
            muscleDifferential.add("Chronic Neck and Back Pain, Shoulder Bursitis");
            muscleDifferential.add("Epicondylitis");
            muscleDifferential.add("Epicondylitis");
            muscleDifferential.add("Epicondylitis");
            muscleDifferential.add("Epicondylitis");
            muscleDifferential.add("Epicondylitis");
            muscleDifferential.add("");                                 ///V
            muscleDifferential.add("Patellofemoral Syndrome, Knee and Hip Joint Disease, Knee Buckling, Jumper's Knee");
            muscleDifferential.add("Patellofemoral Syndrome, Knee and Hip Joint Disease, Knee Buckling, Jumper's Knee");
            muscleDifferential.add("Patellofemoral Syndrome, Knee and Hip Joint Disease, Knee Buckling, Jumper's Knee");
            muscleDifferential.add("Patellofemoral Syndrome, Knee and Hip Joint Disease, Knee Buckling, Jumper's Knee");
            muscleDifferential.add("Patellofemoral Syndrome, Knee and Hip Joint Disease, Knee Buckling, Jumper's Knee ");
            muscleDifferential.add("Patellofemoral Syndrome, Knee and Hip Joint Disease, Knee Buckling, Jumper's Knee");
            muscleDifferential.add("Patellofemoral Syndrome, Knee and Hip Joint Disease, Knee Buckling, Jumper's Knee");
            muscleDifferential.add("Patellofemoral Syndrome, Knee and Hip Joint Disease, Knee Buckling, Jumper's Knee");
            muscleDifferential.add("");                                 ///Z
            muscleDifferential.add("Sinus Issues");
        }

        if(muscleAssociated.size() < 1){

            muscleAssociated.add("");                                ///A
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Posterior Tibial, Medial Lateral Plantar");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Obtrurator, Genitofemoral");
            muscleAssociated.add("Obtrurator, Genitofemoral");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                ///B
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Sciatic (due to Fibrosis)");
            muscleAssociated.add("Sensory branch of Radial");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                ///C
            muscleAssociated.add("Musculocutaneous");
            muscleAssociated.add("");                               ////D
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                               ///E
            muscleAssociated.add("Radial");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Posterior Tibial, Medial Lateral Plantar");
            muscleAssociated.add("Deep Peroneal");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                 ///F
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Radial");
            muscleAssociated.add("Ulnar");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Ulnar");
            muscleAssociated.add("Medial");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                 ///G
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Middle Cluneal Nerve");
            muscleAssociated.add("Middle Cluneal Nerve");
            muscleAssociated.add("Middle Cluneal Nerve");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Obtrurator, Genitofemoral");
            muscleAssociated.add("");                                 ///I
            muscleAssociated.add("Iliohypogastric, Ilioinguninal, Lateral Femoral Cutaneuous, Femoral");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                 ///L
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                 ///M
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                 ///O
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Supraorbital");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                 ///P
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Obtrurator, Genitofemoral");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Brachial Plexus (4th and 5th digits)");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Common Peroneal");
            muscleAssociated.add("Common Peroneal");
            muscleAssociated.add("Common Peroneal");
            muscleAssociated.add("Superior Gluteal, Inferior Gluteal, Pudendal, Sciatic, Posterior Femoral Cutaneous");
            muscleAssociated.add("Superior Gluteal, Inferior Gluteal, Pudendal, Sciatic, Posterior Femoral Cutaneous");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Median");
            muscleAssociated.add("");                                 ///Q
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                 ///R
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                 ///S
            muscleAssociated.add("Lateral Femoral Cutaneous");
            muscleAssociated.add("Thoracic Outlet, Roots that contribute to Long Thoracic Nerve");
            muscleAssociated.add("Thoracic Outlet");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Sciatic (due to Fibrosis)");
            muscleAssociated.add("Greater Occipital Nerve");
            muscleAssociated.add("Greater Occipital Nerve");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Tibial Nerve");
            muscleAssociated.add("Tibial Nerve");
            muscleAssociated.add("Tibial Nerve");
            muscleAssociated.add("Tibial Nerve");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Spinal Accessory");
            muscleAssociated.add("Spinal Accessory");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Radial");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documentedn");
            muscleAssociated.add("");                                 ///T
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Axillary");
            muscleAssociated.add("Axillary");
            muscleAssociated.add("Anterior Compartment Syndrome");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("Radial");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                 ///V
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("None Documented");
            muscleAssociated.add("");                                 ///Z
            muscleAssociated.add("None Documented");
        }

        if(ligamentDifferential.size() < 1){

            ligamentDifferential.add("A");
            ligamentDifferential.add("Shoulder Strain");
            ligamentDifferential.add("Patellofemoral Syndrome, Patellar Dysfunction, Baker's Cyst");
            ligamentDifferential.add("C");
            ligamentDifferential.add("Cervical Radiculopathy");
            ligamentDifferential.add("Cervical Radiculopathy");
            ligamentDifferential.add("Cervical Radiculopathy");
            ligamentDifferential.add("Costovertebral Dysfunction, Rib Fracture, Costochondritis");
            ligamentDifferential.add("Costovertebral Dysfunction, Rib Fracture, Costochondritis");
            ligamentDifferential.add("D");
            ligamentDifferential.add("Lower Extremity Myofascial Trigger Points, Dosral Tarsal Injury");
            ligamentDifferential.add("Radiculopathy, Carpal Tunnel Syndrome, Thoracic Outlet Syndrome");
            ligamentDifferential.add("E");
            ligamentDifferential.add("Lower Extremity Myofascial Trigger Points, Dosral Tarsal Injury");
            ligamentDifferential.add("F");
            ligamentDifferential.add("Lumbar Radiculopathy");
            ligamentDifferential.add("G");
            ligamentDifferential.add("Shoulder Strain");
            ligamentDifferential.add("I");
            ligamentDifferential.add("Hip Strain, Lumbar Radiculopathy, Meralgia Paresthetica");
            ligamentDifferential.add("Hip Strain, Groin Strain");
            ligamentDifferential.add("Forearm Strain");
            ligamentDifferential.add("Tibal Stress Syndrome");
            ligamentDifferential.add("L");
            ligamentDifferential.add("Tibal Stress Syndrome");
            ligamentDifferential.add("Hip Strain, Hip Bursitis");
            ligamentDifferential.add("M");
            ligamentDifferential.add("Tibal Stress Syndrome");
            ligamentDifferential.add("P");
            ligamentDifferential.add("Carpal Tunnel Syndrome");
            ligamentDifferential.add("Groin Strain");
            ligamentDifferential.add("R");
            ligamentDifferential.add("Carpal Tunnel Syndrome");
            ligamentDifferential.add("S");
            ligamentDifferential.add("Lumbar Radiculopathy");
            ligamentDifferential.add("Lumbar Radiculopathy");
            ligamentDifferential.add("Lumbar Radiculopathy");
            ligamentDifferential.add("Upper Extremity Myofascial Trigger Points, Costosternal Injury");
            ligamentDifferential.add("Shoulder Strain, Cervical Radiculopathy");
            ligamentDifferential.add("Cervical Radiculopathy");
            ligamentDifferential.add("Cervical Radiculopathy, Carpal Tunnel");
            ligamentDifferential.add("Lumbar Radiculopathy");
            ligamentDifferential.add("Lumbar Radiculopathy");
            ligamentDifferential.add("Lumbar Radiculopathy");
            ligamentDifferential.add("Lumbar Radiculopathy");
            ligamentDifferential.add("Cervical Radiculopathy");
            ligamentDifferential.add("T");
            ligamentDifferential.add("Lower Extremity Myofascial Trigger Points, Dosral Tarsal Injury");
            ligamentDifferential.add("Carpal Tunnel Syndrome, Cervical Radiculopathy, Dorsal Hand Injury");
            ligamentDifferential.add("Lumbago, Disc Lesion, Lumbopelvic Strain");
            ligamentDifferential.add("U");
            ligamentDifferential.add("Cervical Radiculopathy");
        }

        if(jointDifferential.size() < 1){

            jointDifferential.add("D");
            jointDifferential.add("Radiculopathy, Myofascial Pain Syndrome, Fibromyalgia, Systemic Disease");
            jointDifferential.add("Radiculopathy, Myofascial Pain Syndrome, Fibromyalgia, Systemic Disease");
            jointDifferential.add("Radiculopathy, Myofascial Pain Syndrome, Fibromyalgia, Systemic Disease");
            jointDifferential.add("F");
            jointDifferential.add("Cervical Strain, Tension Based Headache");
            jointDifferential.add("Cervical Strain, Tension Based Headache");
            jointDifferential.add("Cervical Strain, Tension Based Headache");
            jointDifferential.add("Cervical Strain, Tension Based Headache");
            jointDifferential.add("Cervical Radiculopathy, Shoulder Strain");
            jointDifferential.add("Cervical Radiculopathy, Shoulder Strain");
            jointDifferential.add("Cervical Radiculopathy, Shoulder Strain");
            jointDifferential.add("Cervical Radiculopathy, Shoulder Strain");
            jointDifferential.add("Cervical Radiculopathy, Shoulder Strain");
            jointDifferential.add("Lumbar Strain, Hip Strain, Hip Bursitis");
            jointDifferential.add("Scapulocostal Syndrome");
            jointDifferential.add("Scapulocostal Syndrome");
            jointDifferential.add("Scapulocostal Syndrome");
            jointDifferential.add("Lumbar Strain");
            jointDifferential.add("Scapulocostal Syndrome");
            jointDifferential.add("Scapulocostal Syndrome");
            jointDifferential.add("S");
            jointDifferential.add("Lumbar Radiculopathy, Hip Strain");
            jointDifferential.add("Upper cervical flexors");
            jointDifferential.add("Upper cervical extensors");
            jointDifferential.add("cervical lateral flexors");
            jointDifferential.add("shoulder girdle elevators");
            jointDifferential.add("Shoulder flexors, abductors, external rotation, elbow flexors");
            jointDifferential.add("Shoulder extensor, adductors, internal rotators, eblow flexors, elbow extensors, forearm supinators");
            jointDifferential.add("Shoulder extensor, adductors, internal rotators,  elbow extensors, forearm pronators, wrist flexion, wrist extension, finger flexion, finger extension");
            jointDifferential.add("Shoulder extensor, adductors, internal rotators,  elbow extensors, forearm pronators, wrist flexion, wrist extension, finger flexion, finger extension");
            jointDifferential.add("Hip Adductors, Hip Internal Rotators");
            jointDifferential.add("Hip flexors, Hip Adductors, Hip Internal Rotators");
            jointDifferential.add("Hip flexors, hip Adductors, Hip Internal Rotators, Knee Extensors");
            jointDifferential.add("Hip extensors, Knee Extensors, Ankle Dorsiflexors");
            jointDifferential.add("Hip Extensors, Hip Abductors, Hip External Rotators, Knee Flexion, Ankle Dorsiflexors");
            jointDifferential.add("foot eversion, Hip Abductors, External Rotators, Knee Flexors, Ankle Plantar Flexors");
            jointDifferential.add("Intrinsic Hand Muscles");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("Segmental Spinal Musculature");
            jointDifferential.add("T");
            jointDifferential.add("Complex Regional Pain Syndrome");
            jointDifferential.add("Complex Regional Pain Syndrome");
            jointDifferential.add("Complex Regional Pain Syndrome");
            jointDifferential.add("Complex Regional Pain Syndrome");
            jointDifferential.add("Complex Regional Pain Syndrome");
        }

        if (jointAssociated.size() < 0){

            jointAssociated.add("D");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("F");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("S");
            jointAssociated.add("");  /*0-22*/
    /*Associated Muscles*/           /*Associated Muscles*/
            jointAssociated.add("Upper cervical flexors");
            jointAssociated.add("Upper cervical extensors");
            jointAssociated.add("cervical lateral flexors");
            jointAssociated.add("shoulder girdle elevators");
            jointAssociated.add("Shoulder flexors, abductors, external rotation, elbow flexors");
            jointAssociated.add("Shoulder extensor, adductors, internal rotators, eblow flexors, elbow extensors, forearm supinators");
            jointAssociated.add("Shoulder extensor, adductors, internal rotators,  elbow extensors, forearm pronators, wrist flexion, wrist extension, finger flexion, finger extension");
            jointAssociated.add("Shoulder extensor, adductors, internal rotators,  elbow extensors, forearm pronators, wrist flexion, wrist extension, finger flexion, finger extension");
            jointAssociated.add("Hip Adductors, Hip Internal Rotators");
            jointAssociated.add("Hip flexors, Hip Adductors, Hip Internal Rotators");
            jointAssociated.add("Hip flexors, hip Adductors, Hip Internal Rotators, Knee Extensors");
            jointAssociated.add("Hip extensors, Knee Extensors, Ankle Dorsiflexors");
            jointAssociated.add("Hip Extensors, Hip Abductors, Hip External Rotators, Knee Flexion, Ankle Dorsiflexors");
            jointAssociated.add("foot eversion, Hip Abductors, External Rotators, Knee Flexors, Ankle Plantar Flexors");
            jointAssociated.add("Intrinsic Hand Muscles");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("Segmental Spinal Musculature");
            jointAssociated.add("T");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
            jointAssociated.add("");
        }

        if(periostealDifferential.size() < 1){

            periostealDifferential.add("A");
            periostealDifferential.add("Costovertebral Dysfunction, Rib Fracture");
            periostealDifferential.add("C");
            periostealDifferential.add("Tension Based Headaches");
            periostealDifferential.add("Myofascial Trigger Points of the Lower Extremity");
            periostealDifferential.add("Clavical Dysfunction, Other Upper Extremity Trigger Points");
            periostealDifferential.add("Coccyx Dysfunction, Sacral Trauma, Sacral Fracture");
            periostealDifferential.add("D");
            periostealDifferential.add("Upper Extremity Myofascial Trigger Points");
            periostealDifferential.add("E");
            periostealDifferential.add("Upper Extremity Myofascial Trigger Points");
            periostealDifferential.add("F");
            periostealDifferential.add("Other Lower Extremity Trigger Points");
            periostealDifferential.add("I");
            periostealDifferential.add("Scaitica, Lumbar Dysfunction, Thoracolumbar Syndrome");
            periostealDifferential.add("Other Lower Extreimty and Posterior Hip Trigger Points");
            periostealDifferential.add("K");
            periostealDifferential.add("Myofascial Trigger Points of the Lower Extremity and Hip Muscles");
            periostealDifferential.add("L");
            periostealDifferential.add("Epicondylitis");
            periostealDifferential.add("Ostitis Pubis");
            periostealDifferential.add("M");
            periostealDifferential.add("TMJ Dysfunction, Trigeminal Neuralgia");
            periostealDifferential.add("Epicondylitis");
            periostealDifferential.add("Metatarsal Injury, Morton's Neuroma");
            periostealDifferential.add("N");
            periostealDifferential.add("Tension Headache");
            periostealDifferential.add("P");
            periostealDifferential.add("Knee Injury");
            periostealDifferential.add("Tension Headache");
            periostealDifferential.add("R");
            periostealDifferential.add("De Quervain's Syndrome");
            periostealDifferential.add("Costocondritis");
            periostealDifferential.add("Costovertebral Dysfunction, Rib Fracture");
            periostealDifferential.add("S");
            periostealDifferential.add("Segmental Disc Lesion");
            periostealDifferential.add("Other Upper Extremity Myofascial Trigger Points");
            periostealDifferential.add("Upper Extremity Myofascial Trigger Points");
            periostealDifferential.add("Patellofemoral Syndrome, Patellar Dysfunction");
            periostealDifferential.add("Ostitis Pubis");
            periostealDifferential.add("T");
            periostealDifferential.add("Segmental Dysfunction");
            periostealDifferential.add("X");
            periostealDifferential.add("Gastic Dysfunction");
        }

        if (periostealCauseTenision.size() < 1){

            periostealCauseTenision.add("A");
            periostealCauseTenision.add("Rib Joint Dysfunction, Trigger Points in Serratus Anterior");
            periostealCauseTenision.add("C");
            periostealCauseTenision.add("Upper Cervical Joint Dysfunction, Trigger Points in Leavator Scapula");
            periostealCauseTenision.add("Trigger Points in the Quadratus Plantae");
            periostealCauseTenision.add("Trigger Points in SCM");
            periostealCauseTenision.add("Trigger Points in Gluteus Maximus");
            periostealCauseTenision.add("D");
            periostealCauseTenision.add("Glenohumeral Joint Dysfunction");
            periostealCauseTenision.add("E");
            periostealCauseTenision.add("Trigger Points in Scalenes, Cervical Joint Dysfunction");
            periostealCauseTenision.add("F");
            periostealCauseTenision.add("Proximal Fibular Head Dysfunction");
            periostealCauseTenision.add("I");
            periostealCauseTenision.add("Trigger Points in Quadratus Lumborum and Superior Portion of the Gluteals");
            periostealCauseTenision.add("Trigger Points in Medial Hamstrings");
            periostealCauseTenision.add("K");
            periostealCauseTenision.add("Knee Joint Dysfunction, Possible Meniscus Injury");
            periostealCauseTenision.add("L");
            periostealCauseTenision.add("Trigger Points in Biceps Brachii, Brachioradialis, Extensor Digitorum");
            periostealCauseTenision.add("Hip Joint Dysfunction, Trigger Points in Proximal Hip Adductors");
            periostealCauseTenision.add("M");
            periostealCauseTenision.add("Trigger Points in Muscles of Mastication");
            periostealCauseTenision.add("Trigger Points in Wrist and Finger Flexors");
            periostealCauseTenision.add("Joint Dysfunction of the Tarsal-Metatarsal Joints");
            periostealCauseTenision.add("N");
            periostealCauseTenision.add("Trigger Points in the Suboccipital Muscles");
            periostealCauseTenision.add("P");
            periostealCauseTenision.add("Hip Joint Dysfunction, Trigger Points in the Pes Anserine Muscles");
            periostealCauseTenision.add("Upper Cervical Joint Dysfunction");
            periostealCauseTenision.add("R");
            periostealCauseTenision.add("Proximal and Distal Radial-Unlar Joint Dysfunction");
            periostealCauseTenision.add("Trigger Points in the Pectoral Muscle Group");
            periostealCauseTenision.add("Trigger Points in the Serratus Anterior");
            periostealCauseTenision.add("S");
            periostealCauseTenision.add("Segmental Joint Dysfunction, Trigger Points in the Segmenal Multifidi");
            periostealCauseTenision.add("Joint Dysfunction of the first costosternal joint");
            periostealCauseTenision.add("Trigger Points of the SCM and Anterior Chest Wall Group");
            periostealCauseTenision.add("Trigger Points of the Proximal Quadriceps Group");
            periostealCauseTenision.add("Trigger Points in the Lower Abdominal Muscle Group");
            periostealCauseTenision.add("T");
            periostealCauseTenision.add("Trigger Points in the SCM Group");
            periostealCauseTenision.add("X");
            periostealCauseTenision.add("Trigger Points in the Upper Abdominal Muscle Group");
        }



        txv_trigger_content = (TextView)findViewById(R.id.txv_trigger_content);
        ui_txvDysfunction = (TextView)findViewById(R.id.txv_dysfunction);
        ui_txvAssessment = (TextView)findViewById(R.id.txv_assessment);
        ui_txvDifferential = (TextView)findViewById(R.id.txv_differential);
        ui_txvAssociated = (TextView)findViewById(R.id.txv_associated);

        /*TITLE_*/
        txv_title_trigger_point = (TextView)findViewById(R.id.txv_title_trigger_point);
        txv_title_dysfunction = (TextView)findViewById(R.id.txv_title_dysfunction);
        txv_title_assessment = (TextView)findViewById(R.id.txv_title_assessment);
        txv_title_differential = (TextView)findViewById(R.id.txv_title_differential);
        txv_title_associated = (TextView)findViewById(R.id.txv_title_associated);
        /************************************************************************/
        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_txvTitle = (TextView)findViewById(R.id.txv_title);

        ui_imagedetail = (ImageView)findViewById(R.id.imv_imagedetail);

        /*LIST_SETTING*/

        /*Muscle */
        if(Constants.select_Status == 1) {

            ui_txvAssociated.setVisibility(View.VISIBLE);
            txv_title_associated.setVisibility(View.VISIBLE);

            txv_title_trigger_point.setText("Muscle Trigger Point");
            txv_trigger_content.setText(muscleList.get(position));

            ui_imagedetail.setImageResource(array_image1.get(position));
            ui_txvTitle.setText(muscleList.get(position));

            ui_txvDysfunction.setText("Trigger points produce pain, weakness, taut bands and limited stretch range of motion in the asocaited muscles.  Referred pain in the above distribution, may feel like a dull ache, pressure, burning or a numbness associated with fatigue.  Referred symptoms can include pain, sweating, changes to skin color, gland secretion,  pilomotor reflex and dizziness.");

            ui_txvAssessment.setText("Palpation of the trigger point should reveal a taut band, moderate to severe spot tenderness of a nodule, reproduction of the patient's chief complaint and the assessment of end range of motion should be painful.");

            ui_txvDifferential.setText(muscleDifferential.get(position));

            ui_txvAssociated.setText(muscleAssociated.get(position));

            /* Periosteal */
        } else if(Constants.select_Status == 4) {

            ui_txvAssociated.setVisibility(View.VISIBLE);
            txv_title_associated.setVisibility(View.VISIBLE);

            txv_title_trigger_point.setText("Periosteal Point");
            txv_trigger_content.setText(periostealList.get(position));

            txv_title_dysfunction.setText("Cause of Periosteal Tension");
            ui_txvDysfunction.setText(periostealCauseTenision.get(position));

            txv_title_assessment.setText("Dysfunction");
            ui_txvAssessment.setText("Periosteal point pain may feel either sharp or dull and is usually local to a specific palpable point.  Periosteal irritation is commonly a result of altered tissue tension at the periosteum insertion.");

            txv_title_differential.setText("Assesment");
            ui_txvDifferential.setText("Pin point tenderness of the irritated periosteum will be appreciated during isolated palpation of the specific periosteal point.");

            txv_title_associated.setText("Differential or Misdiagnosis");
            ui_txvAssociated.setText(periostealDifferential.get(position));

            ui_imagedetail.setImageResource(array_imagePeriosteal1.get(position));
            ui_txvTitle.setText(periostealList.get(position));


            /*Ligament*/
        }else if(Constants.select_Status == 2){

            txv_title_associated.setVisibility(View.GONE);
            ui_txvAssociated.setVisibility(View.GONE);

            txv_title_trigger_point.setText("Ligament Trigger Point");
            txv_trigger_content.setText(array_ligamentList.get(position));

            ui_imagedetail.setImageResource(array_imageLigament.get(position));
            ui_txvTitle.setText(array_ligamentList.get(position));

            ui_txvDysfunction.setText("Ligament referred pain, similar to trigger point pain, may feel like a dull ache, pressure, burning or numbness associated with a feeling of instability.  Ligamentous referred pain is usually a result of ligamentous laxity or ligament injury.");

            if(position == 4 || position == 5 || position == 6 || position == 16 || position == 17 || position == 26 || position == 49 || position == 50){
                ui_txvAssessment.setText("Limited passive range of motion and hard segmental end feel, with capsular pattern of restricted lateral flexion, rotation and extension.");
            }else{
                ui_txvAssessment.setText("Active and passive movement of the joint in the direction of ligament stress will produce pain; active resisted movements in the direction of stress will not produce pain.");
            }

            txv_title_differential.setText("Differential or Misdiagnosis");
            ui_txvDifferential.setText(ligamentDifferential.get(position));


            /*Joint*/
        }else if(Constants.select_Status == 3){

            if (position < 23){
                txv_title_trigger_point.setText("Joint");
                txv_title_dysfunction.setText("Dysfunction");
                txv_title_assessment.setText("Assessment");

                txv_title_differential.setText("Differential or Misdiagnosis");
                //ui_txvDifferential.setText(jointDifferential.get(position));

                ui_txvAssociated.setVisibility(View.GONE);
                txv_title_associated.setVisibility(View.GONE);

            } else if (position >= 23 && position < 49){

                txv_title_trigger_point.setText("Sclerotome");
                txv_title_dysfunction.setText("Dysfunction");
                txv_title_assessment.setText("Assessment");

                txv_title_differential.setText("Associated Muscles");
                //ui_txvDifferential.setText(jointAssociated.get(position));

                ui_txvAssociated.setVisibility(View.GONE);
                txv_title_associated.setVisibility(View.GONE);

            } else {

                txv_title_associated.setVisibility(View.VISIBLE);
                ui_txvAssociated.setVisibility(View.VISIBLE);

                txv_title_trigger_point.setText("Thermatome");
                txv_title_dysfunction.setText("Joint Dysfunction");
                txv_title_assessment.setText("Dysfunction");
                txv_title_differential.setText("Assesment");
                txv_title_associated.setText("Differential or Misdiagnosis");

            }

            txv_trigger_content.setText(jointList.get(position));

            ui_imagedetail.setImageResource(joint_image.get(position));
            ui_txvTitle.setText(jointList.get(position));

            /*Dysfunction*/
            if(position < 4){
                ui_txvDysfunction.setText("Dural referred pain is usually a result of dural tension or dural adhesion due to spinal canal lesion or stenosis.  Referred pain may feel like a dull ache, pressure, burning or a numbness in the dural distribution of pain.");
            }else if(position >= 4 && position < 23){
                ui_txvDysfunction.setText("Joint referred pain may feel like a dull ache, pressure, burning or a numbness in the above distribution.  Joint referral may be associated with acute injury, or chronic degenerative changes in the joint.");
            }else if(position >= 23 && position < 49){
                ui_txvDysfunction.setText("Sclerotome referral may feel like a dull ache, pressure, burning or a numbness in the above distribution. Sclerotome pain is usually a result of segmental irritation of a spinal level.  Afferent signal from injured tissue assocaited with a specific segmental innervation may produce pain in any or all structures associated with the segmental level.");
            }else{

                if (position == 50){
                    ui_txvDysfunction.setText("T1-T2 Vertebral Segment");

                } else if (position == 51){

                    ui_txvDysfunction.setText("T1-T4 Vertebral Segments");

                } else if (position == 52){

                    ui_txvDysfunction.setText("T11-L2 Vertebral Segments");

                } else if (position == 53){

                    ui_txvDysfunction.setText("T2-T7 Vertebral Segments");

                } else if (position == 54){

                    ui_txvDysfunction.setText("T4-L2 Vertebral Segments");
                }

            }

            /*Assessment*/
            if(position < 4){
                ui_txvAssessment.setText("Increased symptoms during cervical and spinal flexion, with positive seated slump test and increased spinal pain or tension during cervical or spinal traction.");
            }else if(position == 22){
                ui_txvAssessment.setText("Limited passive range of motion, loss of springy end feel.");

            }else if(position >= 23 && position < 49){

                ui_txvAssessment.setText("Limited passive range of motion and hard segmental end feel, with capsular pattern of restricted lateral flexion, rotation and extension.");

                /*ui_txvDifferential.setText("Limited passive range of motion and hard segmental end feel, with capsular pattern of restricted lateral flexion, rotation and extension.");
                txv_title_associated.setText("Complex Regional Pain Syndrome");*/

            }else {
                ui_txvAssessment.setText("Thermatome symtpoms include color, temperature and sweat changes in the above distribution. Thermatome symptoms are a result of joint dysfunction at specific segments.");
            }

            /*Differential*/

            if (position < 23){

                ui_txvDifferential.setText(jointDifferential.get(position));
            } else if (position >= 23 && position < 49){

                ui_txvDifferential.setText(jointDifferential.get(position));

            } else {
                ui_txvDifferential.setText("Limited passive range of motion and hard segmental end feel, with capsular pattern of restricted lateral flexion, rotation and extension.");
            }

            /*Associated*/
            if(position < 49) {
                txv_title_associated.setVisibility(View.GONE);

            }else{
                ui_txvAssociated.setText("Complex Regional Pain Syndrome");
            }

        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:

                finish();
                break;
        }

    }

}

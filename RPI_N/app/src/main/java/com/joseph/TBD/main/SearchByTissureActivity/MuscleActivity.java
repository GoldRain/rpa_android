package com.joseph.TBD.main.SearchByTissureActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.joseph.TBD.R;
import com.joseph.TBD.adapter.CommonlistAdapter;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.DetailsActivity.MuscleDetailsActivity;
import com.joseph.TBD.main.MainActivity;

import java.util.Locale;

public class MuscleActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack ,ui_imvCacel ;
    EditText ui_edtSearch;
    ListView ui_lstContainer ;
    CommonlistAdapter _adapter_mus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muscle);

        loadLayout();
    }


    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);
        ui_edtSearch = (EditText)findViewById(R.id.edt_search);
        ui_imvCacel = (ImageView)findViewById(R.id.imv_cancel);
        ui_imvCacel.setOnClickListener(this);

        _adapter_mus = new CommonlistAdapter(this, Constants.muscleList);
        ui_lstContainer = (ListView)findViewById(R.id.lst_contaner_mus1);

        ui_edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (ui_edtSearch.getText().length()>0){
                    ui_imvCacel.setVisibility(View.VISIBLE);
                }else {
                    ui_imvCacel.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                String name = ui_edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                _adapter_mus.filter(name);

            }
        });

        _adapter_mus.addItem(Constants.muscleList);
        _adapter_mus.initProducts();
        _adapter_mus.notifyDataSetChanged();

        ui_lstContainer.setAdapter(_adapter_mus);

        ui_lstContainer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String name = _adapter_mus._contents.get(position);

                if(name.length() != 1) {

                    for (int i = 0; i < Constants.muscleList.size(); i++) {

                        if (Constants.muscleList.get(i).equals(name))
                        {
                            position = i;
                            Log.d("PositionNew=======>", String.valueOf(position));
                        }
                    }

                    Constants.select_Status = 1;
                    Constants.position = position;
                    Intent intent = new Intent(MuscleActivity.this, MuscleDetailsActivity.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;

            case R.id.imv_cancel:
                ui_edtSearch.setText("");
                break;
        }

    }

}

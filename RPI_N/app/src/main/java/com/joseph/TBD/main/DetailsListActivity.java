package com.joseph.TBD.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.joseph.TBD.R;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;

public class DetailsListActivity extends CommonActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_list);

        loadLayout();
    }

    private void loadLayout() {

    }

    @Override
    public void onClick(View v) {

    }
}

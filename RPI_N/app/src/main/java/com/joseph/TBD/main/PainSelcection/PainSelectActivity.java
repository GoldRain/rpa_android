package com.joseph.TBD.main.PainSelcection;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.joseph.TBD.R;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.MainActivity;

import static com.joseph.TBD.commons.Constants.anconiusstatus;
import static com.joseph.TBD.commons.Constants.backbody1;
import static com.joseph.TBD.commons.Constants.backbody2;
import static com.joseph.TBD.commons.Constants.backbody3;
import static com.joseph.TBD.commons.Constants.backbody4;
import static com.joseph.TBD.commons.Constants.backfoot1;
import static com.joseph.TBD.commons.Constants.backfoot10;
import static com.joseph.TBD.commons.Constants.backfoot11;
import static com.joseph.TBD.commons.Constants.backfoot12;
import static com.joseph.TBD.commons.Constants.backfoot13;
import static com.joseph.TBD.commons.Constants.backfoot14;
import static com.joseph.TBD.commons.Constants.backfoot15;
import static com.joseph.TBD.commons.Constants.backfoot16;
import static com.joseph.TBD.commons.Constants.backfoot17;
import static com.joseph.TBD.commons.Constants.backfoot18;
import static com.joseph.TBD.commons.Constants.backfoot19;
import static com.joseph.TBD.commons.Constants.backfoot2;
import static com.joseph.TBD.commons.Constants.backfoot20;
import static com.joseph.TBD.commons.Constants.backfoot21;
import static com.joseph.TBD.commons.Constants.backfoot22;
import static com.joseph.TBD.commons.Constants.backfoot3;
import static com.joseph.TBD.commons.Constants.backfoot4;
import static com.joseph.TBD.commons.Constants.backfoot5;
import static com.joseph.TBD.commons.Constants.backfoot6;
import static com.joseph.TBD.commons.Constants.backfoot7;
import static com.joseph.TBD.commons.Constants.backfoot8;
import static com.joseph.TBD.commons.Constants.backfoot9;
import static com.joseph.TBD.commons.Constants.backhead1;
import static com.joseph.TBD.commons.Constants.backhead2;
import static com.joseph.TBD.commons.Constants.backhead3;
import static com.joseph.TBD.commons.Constants.backhead4;
import static com.joseph.TBD.commons.Constants.backhead5;
import static com.joseph.TBD.commons.Constants.backhead6;
import static com.joseph.TBD.commons.Constants.backleg1;
import static com.joseph.TBD.commons.Constants.backleg10;
import static com.joseph.TBD.commons.Constants.backleg11;
import static com.joseph.TBD.commons.Constants.backleg12;
import static com.joseph.TBD.commons.Constants.backleg13;
import static com.joseph.TBD.commons.Constants.backleg2;
import static com.joseph.TBD.commons.Constants.backleg3;
import static com.joseph.TBD.commons.Constants.backleg4;
import static com.joseph.TBD.commons.Constants.backleg5;
import static com.joseph.TBD.commons.Constants.backleg6;
import static com.joseph.TBD.commons.Constants.backleg7;
import static com.joseph.TBD.commons.Constants.backleg8;
import static com.joseph.TBD.commons.Constants.backleg9;
import static com.joseph.TBD.commons.Constants.body_part;
import static com.joseph.TBD.commons.Constants.frontbody1;
import static com.joseph.TBD.commons.Constants.frontbody2;
import static com.joseph.TBD.commons.Constants.frontbody3;
import static com.joseph.TBD.commons.Constants.frontfoot1;
import static com.joseph.TBD.commons.Constants.frontfoot10;
import static com.joseph.TBD.commons.Constants.frontfoot11;
import static com.joseph.TBD.commons.Constants.frontfoot2;
import static com.joseph.TBD.commons.Constants.frontfoot3;
import static com.joseph.TBD.commons.Constants.frontfoot4;
import static com.joseph.TBD.commons.Constants.frontfoot5;
import static com.joseph.TBD.commons.Constants.frontfoot6;
import static com.joseph.TBD.commons.Constants.frontfoot7;
import static com.joseph.TBD.commons.Constants.frontfoot8;
import static com.joseph.TBD.commons.Constants.frontfoot9;
import static com.joseph.TBD.commons.Constants.fronthead1;
import static com.joseph.TBD.commons.Constants.fronthead2;
import static com.joseph.TBD.commons.Constants.fronthead3;
import static com.joseph.TBD.commons.Constants.fronthead4;
import static com.joseph.TBD.commons.Constants.fronthead5;
import static com.joseph.TBD.commons.Constants.fronthead6;
import static com.joseph.TBD.commons.Constants.fronthead7;
import static com.joseph.TBD.commons.Constants.fronthead8;
import static com.joseph.TBD.commons.Constants.fronthead9;
import static com.joseph.TBD.commons.Constants.frontleg1;
import static com.joseph.TBD.commons.Constants.frontleg10;
import static com.joseph.TBD.commons.Constants.frontleg11;
import static com.joseph.TBD.commons.Constants.frontleg2;
import static com.joseph.TBD.commons.Constants.frontleg3;
import static com.joseph.TBD.commons.Constants.frontleg4;
import static com.joseph.TBD.commons.Constants.frontleg5;
import static com.joseph.TBD.commons.Constants.frontleg6;
import static com.joseph.TBD.commons.Constants.frontleg7;
import static com.joseph.TBD.commons.Constants.frontleg8;
import static com.joseph.TBD.commons.Constants.frontleg9;
import static com.joseph.TBD.commons.Constants.post;

public class PainSelectActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    ImageView ui_imvDetails;

    FrameLayout ui_frm_backfoot, ui_frm_fronthead, ui_frm_frontleg, ui_frm_backleg, ui_frm_frontfoot;

    ImageView ui_imvBody_18_1, ui_imvBody_18_2, ui_imvBody_18_3;

    ImageView ui_imvBackHead24_1, ui_imvBackHead24_2, ui_imvBackHead24_3, ui_imvBackHead24_4, ui_imvBackHead24_5, ui_imvBackHead24_5_1, ui_imvBackHead24_6, ui_imvBackHead24_6_1;

    ImageView ui_imvBackBody28_1, ui_imvBackBody28_1_1,  ui_imvBackBody28_2, ui_imvBackBody28_2_1, ui_imvBackBody28_3, ui_imvBackBody28_4;

    ImageView ui_imvBackLeg25_1, ui_imvBackLeg25_2, ui_imvBackLeg25_3, ui_imvBackLeg25_4, ui_imvBackLeg25_5, ui_imvBackLeg25_6, ui_imvBackLeg25_7, ui_imvBackLeg25_8, ui_imvBackLeg25_9, ui_imvBackLeg25_10, ui_imvBackLeg25_11, ui_imvBackLeg25_12, ui_imvBackLeg25_13, ui_imvBackLeg25_5_1, ui_imvBackLeg25_3_1;

    ImageView ui_imvFrontHead13_1, ui_imvFrontHead13_2, ui_imvFrontHead13_3, ui_imvFrontHead13_4, ui_imvFrontHead13_5, ui_imvFrontHead13_6, ui_imvFrontHead13_7, ui_imvFrontHead13_8, ui_imvFrontHead13_8_1, ui_imvFrontHead13_9;

    ImageView ui_imvFrontLeg16_1, ui_imvFrontLeg16_2, ui_imvFrontLeg16_3, ui_imvFrontLeg16_4, ui_imvFrontLeg16_5, ui_imvFrontLeg16_6, ui_imvFrontLeg16_7, ui_imvFrontLeg16_8, ui_imvFrontLeg16_9, ui_imvFrontLeg16_10, ui_imvFrontLeg16_11;

    ImageView ui_imvBackFoot29_1, ui_imvBackFoot29_2, ui_imvBackFoot29_3, ui_imvBackFoot29_4, ui_imvBackFoot29_5, ui_imvBackFoot29_6, ui_imvBackFoot29_7, ui_imvBackFoot29_8, ui_imvBackFoot29_9, ui_imvBackFoot29_10, ui_imvBackFoot29_11, ui_imvBackFoot29_12, ui_imvBackFoot29_13, ui_imvBackFoot29_14;
    ImageView ui_imvBackFoot29_15, ui_imvBackFoot29_16, ui_imvBackFoot29_17, ui_imvBackFoot29_18, ui_imvBackFoot29_19, ui_imvBackFoot29_20, ui_imvBackFoot29_21, ui_imvBackFoot29_22;

    ImageView ui_imvFrontFoot19_1, ui_imvFrontFoot19_2, ui_imvFrontFoot19_3, ui_imvFrontFoot19_4, ui_imvFrontFoot19_5, ui_imvFrontFoot19_6, ui_imvFrontFoot19_7, ui_imvFrontFoot19_8, ui_imvFrontFoot19_9, ui_imvFrontFoot19_10, ui_imvFrontFoot19_11, ui_imvFrontFoot19_15, ui_imvFrontFoot19_16, ui_imvFrontFoot19_17, ui_imvFrontFoot19_18, ui_imvFrontFoot19_19, ui_imvFrontFoot19_20, ui_imvFrontFoot19_21, ui_imvFrontFoot19_22;

    FrameLayout ui_frm_backhead ,ui_frm_frontbody , ui_frm_backbody ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pain_sect);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_imvDetails = (ImageView) findViewById(R.id.imv_button_mus);
        ui_imvDetails.setOnClickListener(this);

        //front body
        ui_frm_frontbody = (FrameLayout)findViewById(R.id.frm_frontbody);

        ui_imvBody_18_1 = (ImageView)findViewById(R.id.imv_body_18_1);
        ui_imvBody_18_1.setOnClickListener(this);

        ui_imvBody_18_2 = (ImageView)findViewById(R.id.imv_body_18_2);
        ui_imvBody_18_2.setOnClickListener(this);

        ui_imvBody_18_3 = (ImageView)findViewById(R.id.imv_body_18_3);
        ui_imvBody_18_3.setOnClickListener(this);

        //back head
        ui_frm_backhead = (FrameLayout)findViewById(R.id.frm_backhead);

        ui_imvBackHead24_1 = (ImageView)findViewById(R.id.imv_backhead24_1);
        ui_imvBackHead24_1.setOnClickListener(this);

        ui_imvBackHead24_2 = (ImageView)findViewById(R.id.imv_backhead24_2);
        ui_imvBackHead24_2.setOnClickListener(this);

        ui_imvBackHead24_3 = (ImageView)findViewById(R.id.imv_backhead24_3);
        ui_imvBackHead24_3.setOnClickListener(this);

        ui_imvBackHead24_4 = (ImageView)findViewById(R.id.imv_backhead24_4);
        ui_imvBackHead24_4.setOnClickListener(this);

        ui_imvBackHead24_5 = (ImageView)findViewById(R.id.imv_backhead24_5);
        ui_imvBackHead24_5.setOnClickListener(this);

     /*   ui_imvBackHead24_5_1 = (ImageView)findViewById(R.id.imv_backhead24_5_1);
        ui_imvBackHead24_5_1.setOnClickListener(this);

        ui_imvBackHead24_6_1 = (ImageView)findViewById(R.id.imv_backhead24_6_1);
        ui_imvBackHead24_6_1.setOnClickListener(this);*/

        ui_imvBackHead24_6 = (ImageView)findViewById(R.id.imv_backhead24_6);
        ui_imvBackHead24_6.setOnClickListener(this);

        //Back body

        ui_frm_backbody = (FrameLayout)findViewById(R.id.frm_backbody);

        ui_imvBackBody28_1 = (ImageView)findViewById(R.id.imv_backbody_28_1);
        ui_imvBackBody28_1.setOnClickListener(this);

     /*   ui_imvBackBody28_1_1 = (ImageView)findViewById(R.id.imv_backbody_28_1_1);
        ui_imvBackBody28_1_1.setOnClickListener(this);*/

        ui_imvBackBody28_2 = (ImageView)findViewById(R.id.imv_backbody_28_2);
        ui_imvBackBody28_2.setOnClickListener(this);

      /*  ui_imvBackBody28_2_1 = (ImageView)findViewById(R.id.imv_backbody_28_2_1);
        ui_imvBackBody28_2_1.setOnClickListener(this);*/

        ui_imvBackBody28_3 = (ImageView)findViewById(R.id.imv_backbody_28_3);
        ui_imvBackBody28_3.setOnClickListener(this);

        ui_imvBackBody28_4 = (ImageView)findViewById(R.id.imv_backbody_28_4);
        ui_imvBackBody28_4.setOnClickListener(this);

        //Back Leg

        ui_frm_backleg = (FrameLayout)findViewById(R.id.frm_backleg);

        ui_imvBackLeg25_1 = (ImageView)findViewById(R.id.imv_backleg25_1);
        ui_imvBackLeg25_1.setOnClickListener(this);

        ui_imvBackLeg25_2 = (ImageView)findViewById(R.id.imv_backleg25_2);
        ui_imvBackLeg25_2.setOnClickListener(this);

        ui_imvBackLeg25_3 = (ImageView)findViewById(R.id.imv_backleg25_3);
        ui_imvBackLeg25_3.setOnClickListener(this);

        ui_imvBackLeg25_4 = (ImageView)findViewById(R.id.imv_backleg25_4);
        ui_imvBackLeg25_4.setOnClickListener(this);

        ui_imvBackLeg25_5 = (ImageView)findViewById(R.id.imv_backleg25_5);
        ui_imvBackLeg25_5.setOnClickListener(this);

        ui_imvBackLeg25_6 = (ImageView)findViewById(R.id.imv_backleg25_6);
        ui_imvBackLeg25_6.setOnClickListener(this);

        ui_imvBackLeg25_7 = (ImageView)findViewById(R.id.imv_backleg25_7);
        ui_imvBackLeg25_7.setOnClickListener(this);

        ui_imvBackLeg25_8 = (ImageView)findViewById(R.id.imv_backleg25_8);
        ui_imvBackLeg25_8.setOnClickListener(this);

        ui_imvBackLeg25_9 = (ImageView)findViewById(R.id.imv_backleg25_9);
        ui_imvBackLeg25_9.setOnClickListener(this);

        ui_imvBackLeg25_10 = (ImageView)findViewById(R.id.imv_backleg25_10);
        ui_imvBackLeg25_10.setOnClickListener(this);

        ui_imvBackLeg25_11 = (ImageView)findViewById(R.id.imv_backleg25_11);
        ui_imvBackLeg25_11.setOnClickListener(this);

        ui_imvBackLeg25_12 = (ImageView)findViewById(R.id.imv_backleg25_12);
        ui_imvBackLeg25_12.setOnClickListener(this);

        ui_imvBackLeg25_13 = (ImageView)findViewById(R.id.imv_backleg25_13);
        ui_imvBackLeg25_13.setOnClickListener(this);

        ui_imvBackLeg25_5_1 = (ImageView)findViewById(R.id.imv_backleg25_5_1);
        ui_imvBackLeg25_5_1.setOnClickListener(this);

        ui_imvBackLeg25_3_1 = (ImageView)findViewById(R.id.imv_backleg25_3_1);
        ui_imvBackLeg25_3_1.setOnClickListener(this);

        //Front Head

        ui_frm_fronthead = (FrameLayout)findViewById(R.id.frm_fronthead);

        ui_imvFrontHead13_1 = (ImageView)findViewById(R.id.imv_fronthead13_1);
        ui_imvFrontHead13_1.setOnClickListener(this);

        ui_imvFrontHead13_2 = (ImageView)findViewById(R.id.imv_fronthead13_2);
        ui_imvFrontHead13_2.setOnClickListener(this);

        ui_imvFrontHead13_3 = (ImageView)findViewById(R.id.imv_fronthead13_3);
        ui_imvFrontHead13_3.setOnClickListener(this);

        ui_imvFrontHead13_4 = (ImageView)findViewById(R.id.imv_fronthead13_4);
        ui_imvFrontHead13_4.setOnClickListener(this);

        ui_imvFrontHead13_5 = (ImageView)findViewById(R.id.imv_fronthead13_5);
        ui_imvFrontHead13_5.setOnClickListener(this);

        ui_imvFrontHead13_6 = (ImageView)findViewById(R.id.imv_fronthead13_6);
        ui_imvFrontHead13_6.setOnClickListener(this);

        ui_imvFrontHead13_7 = (ImageView)findViewById(R.id.imv_fronthead13_7);
        ui_imvFrontHead13_7.setOnClickListener(this);

        ui_imvFrontHead13_8 = (ImageView)findViewById(R.id.imv_fronthead13_8);
        ui_imvFrontHead13_8.setOnClickListener(this);

        ui_imvFrontHead13_8_1 = (ImageView)findViewById(R.id.imv_fronthead13_8_1);
        ui_imvFrontHead13_8_1.setOnClickListener(this);

        ui_imvFrontHead13_9 = (ImageView)findViewById(R.id.imv_fronthead13_9);
        ui_imvFrontHead13_9.setOnClickListener(this);

        //front Leg

        ui_frm_frontleg = (FrameLayout)findViewById(R.id.frm_frontleg);


        ui_imvFrontLeg16_1 = (ImageView)findViewById(R.id.imv_frontleg16_1);
        ui_imvFrontLeg16_1.setOnClickListener(this);

        ui_imvFrontLeg16_2 = (ImageView)findViewById(R.id.imv_frontleg16_2);
        ui_imvFrontLeg16_2.setOnClickListener(this);

        ui_imvFrontLeg16_3 = (ImageView)findViewById(R.id.imv_frontleg16_3);
        ui_imvFrontLeg16_3.setOnClickListener(this);

        ui_imvFrontLeg16_4 = (ImageView)findViewById(R.id.imv_frontleg16_4);
        ui_imvFrontLeg16_4.setOnClickListener(this);

        ui_imvFrontLeg16_5 = (ImageView)findViewById(R.id.imv_frontleg16_5);
        ui_imvFrontLeg16_5.setOnClickListener(this);

        ui_imvFrontLeg16_6 = (ImageView)findViewById(R.id.imv_frontleg16_6);
        ui_imvFrontLeg16_6.setOnClickListener(this);

        ui_imvFrontLeg16_7 = (ImageView)findViewById(R.id.imv_frontleg16_7);
        ui_imvFrontLeg16_7.setOnClickListener(this);

        ui_imvFrontLeg16_8 = (ImageView)findViewById(R.id.imv_frontleg16_8);
        ui_imvFrontLeg16_8.setOnClickListener(this);

        ui_imvFrontLeg16_9 = (ImageView)findViewById(R.id.imv_frontleg16_9);
        ui_imvFrontLeg16_9.setOnClickListener(this);

        ui_imvFrontLeg16_10 = (ImageView)findViewById(R.id.imv_frontleg16_10);
        ui_imvFrontLeg16_10.setOnClickListener(this);

        ui_imvFrontLeg16_11 = (ImageView)findViewById(R.id.imv_frontleg16_11);
        ui_imvFrontLeg16_11.setOnClickListener(this);


        //back foot

        ui_frm_backfoot = (FrameLayout)findViewById(R.id.frm_backfoot);

        ui_imvBackFoot29_1 = (ImageView)findViewById(R.id.imv_backfoot_29_1);
        ui_imvBackFoot29_1.setOnClickListener(this);

        ui_imvBackFoot29_2 = (ImageView)findViewById(R.id.imv_backfoot_29_2);
        ui_imvBackFoot29_2.setOnClickListener(this);

        ui_imvBackFoot29_3 = (ImageView)findViewById(R.id.imv_backfoot_29_3);
        ui_imvBackFoot29_3.setOnClickListener(this);

        ui_imvBackFoot29_4 = (ImageView)findViewById(R.id.imv_backfoot_29_4);
        ui_imvBackFoot29_4.setOnClickListener(this);

        ui_imvBackFoot29_5 = (ImageView)findViewById(R.id.imv_backfoot_29_5);
        ui_imvBackFoot29_5.setOnClickListener(this);

        ui_imvBackFoot29_6 = (ImageView)findViewById(R.id.imv_backfoot_29_6);
        ui_imvBackFoot29_6.setOnClickListener(this);

        ui_imvBackFoot29_7 = (ImageView)findViewById(R.id.imv_backfoot_29_7);
        ui_imvBackFoot29_7.setOnClickListener(this);

        ui_imvBackFoot29_8 = (ImageView)findViewById(R.id.imv_backfoot_29_8);
        ui_imvBackFoot29_8.setOnClickListener(this);

        ui_imvBackFoot29_9 = (ImageView)findViewById(R.id.imv_backfoot_29_9);
        ui_imvBackFoot29_9.setOnClickListener(this);

        ui_imvBackFoot29_10 = (ImageView)findViewById(R.id.imv_backfoot_29_10);
        ui_imvBackFoot29_10.setOnClickListener(this);

        ui_imvBackFoot29_11 = (ImageView)findViewById(R.id.imv_backfoot_29_11);
        ui_imvBackFoot29_11.setOnClickListener(this);

        ui_imvBackFoot29_12 = (ImageView)findViewById(R.id.imv_backfoot_29_12);
        ui_imvBackFoot29_12.setOnClickListener(this);

        ui_imvBackFoot29_13 = (ImageView)findViewById(R.id.imv_backfoot_29_13);
        ui_imvBackFoot29_13.setOnClickListener(this);

        ui_imvBackFoot29_14 = (ImageView)findViewById(R.id.imv_backfoot_29_14);
        ui_imvBackFoot29_14.setOnClickListener(this);

        ui_imvBackFoot29_15 = (ImageView)findViewById(R.id.imv_backfoot_29_15);
        ui_imvBackFoot29_15.setOnClickListener(this);

        ui_imvBackFoot29_16 = (ImageView)findViewById(R.id.imv_backfoot_29_16);
        ui_imvBackFoot29_16.setOnClickListener(this);

        ui_imvBackFoot29_17 = (ImageView)findViewById(R.id.imv_backfoot_29_17);
        ui_imvBackFoot29_17.setOnClickListener(this);

        ui_imvBackFoot29_18 = (ImageView)findViewById(R.id.imv_backfoot_29_18);
        ui_imvBackFoot29_18.setOnClickListener(this);

        ui_imvBackFoot29_19 = (ImageView)findViewById(R.id.imv_backfoot_29_19);
        ui_imvBackFoot29_19.setOnClickListener(this);

        ui_imvBackFoot29_20 = (ImageView)findViewById(R.id.imv_backfoot_29_20);
        ui_imvBackFoot29_20.setOnClickListener(this);

        ui_imvBackFoot29_21 = (ImageView)findViewById(R.id.imv_backfoot_29_21);
        ui_imvBackFoot29_21.setOnClickListener(this);

        ui_imvBackFoot29_22 = (ImageView)findViewById(R.id.imv_backfoot_29_22);
        ui_imvBackFoot29_22.setOnClickListener(this);

        //front foot

        ui_frm_frontfoot = (FrameLayout)findViewById(R.id.frm_frontfoot);

        ui_imvFrontFoot19_1 = (ImageView)findViewById(R.id.imv_frontfoot_19_1);
        ui_imvFrontFoot19_1.setOnClickListener(this);

        ui_imvFrontFoot19_2 = (ImageView)findViewById(R.id.imv_frontfoot_19_2);
        ui_imvFrontFoot19_2.setOnClickListener(this);

        ui_imvFrontFoot19_3 = (ImageView)findViewById(R.id.imv_frontfoot_19_3);
        ui_imvFrontFoot19_3.setOnClickListener(this);

        ui_imvFrontFoot19_4 = (ImageView)findViewById(R.id.imv_frontfoot_19_4);
        ui_imvFrontFoot19_4.setOnClickListener(this);

        ui_imvFrontFoot19_5 = (ImageView)findViewById(R.id.imv_frontfoot_19_5);
        ui_imvFrontFoot19_5.setOnClickListener(this);

        ui_imvFrontFoot19_6 = (ImageView)findViewById(R.id.imv_frontfoot_19_6);
        ui_imvFrontFoot19_6.setOnClickListener(this);

        ui_imvFrontFoot19_7 = (ImageView)findViewById(R.id.imv_frontfoot_19_7);
        ui_imvFrontFoot19_7.setOnClickListener(this);

        ui_imvFrontFoot19_8 = (ImageView)findViewById(R.id.imv_frontfoot_19_8);
        ui_imvFrontFoot19_8.setOnClickListener(this);

        ui_imvFrontFoot19_9 = (ImageView)findViewById(R.id.imv_frontfoot_19_9);
        ui_imvFrontFoot19_9.setOnClickListener(this);

        ui_imvFrontFoot19_10 = (ImageView)findViewById(R.id.imv_frontfoot_19_10);
        ui_imvFrontFoot19_10.setOnClickListener(this);

        ui_imvFrontFoot19_11 = (ImageView)findViewById(R.id.imv_frontfoot_19_11);
        ui_imvFrontFoot19_11.setOnClickListener(this);

        ui_imvFrontFoot19_15 = (ImageView)findViewById(R.id.imv_frontfoot_19_15);
        ui_imvFrontFoot19_15.setOnClickListener(this);

        ui_imvFrontFoot19_16 = (ImageView)findViewById(R.id.imv_frontfoot_19_16);
        ui_imvFrontFoot19_16.setOnClickListener(this);

        ui_imvFrontFoot19_17 = (ImageView)findViewById(R.id.imv_frontfoot_19_17);
        ui_imvFrontFoot19_17.setOnClickListener(this);

        ui_imvFrontFoot19_18 = (ImageView)findViewById(R.id.imv_frontfoot_19_18);
        ui_imvFrontFoot19_18.setOnClickListener(this);

        ui_imvFrontFoot19_19 = (ImageView)findViewById(R.id.imv_frontfoot_19_19);
        ui_imvFrontFoot19_19.setOnClickListener(this);

        ui_imvFrontFoot19_20 = (ImageView)findViewById(R.id.imv_frontfoot_19_20);
        ui_imvFrontFoot19_20.setOnClickListener(this);

        ui_imvFrontFoot19_21 = (ImageView)findViewById(R.id.imv_frontfoot_19_21);
        ui_imvFrontFoot19_21.setOnClickListener(this);

        ui_imvFrontFoot19_22 = (ImageView)findViewById(R.id.imv_frontfoot_19_22);
        ui_imvFrontFoot19_22.setOnClickListener(this);



        Log.d("post:::Bodypart===++++>", post + "##" + body_part);

        if(post == 1 && body_part == 8){

            ui_frm_frontbody.setVisibility(View.VISIBLE);

            ui_frm_backhead.setVisibility(View.GONE);
            ui_frm_backbody.setVisibility(View.GONE);
            ui_frm_backleg.setVisibility(View.GONE);
            ui_frm_fronthead.setVisibility(View.GONE);
            ui_frm_frontleg.setVisibility(View.GONE);
            ui_frm_backfoot.setVisibility(View.GONE);
            ui_frm_frontfoot.setVisibility(View.GONE);
        } else if(post == 2 && body_part == 4){

            ui_frm_backhead.setVisibility(View.VISIBLE);

            ui_frm_frontbody.setVisibility(View.GONE);
            ui_frm_backbody.setVisibility(View.GONE);
            ui_frm_backleg.setVisibility(View.GONE);
            ui_frm_fronthead.setVisibility(View.GONE);
            ui_frm_frontleg.setVisibility(View.GONE);
            ui_frm_backfoot.setVisibility(View.GONE);
            ui_frm_frontfoot.setVisibility(View.GONE);

        } else if(post == 2 && body_part == 8){

            ui_frm_backbody.setVisibility(View.VISIBLE);

            ui_frm_backfoot.setVisibility(View.GONE);
            ui_frm_backhead.setVisibility(View.GONE);
            ui_frm_frontbody.setVisibility(View.GONE);
            ui_frm_backleg.setVisibility(View.GONE);
            ui_frm_fronthead.setVisibility(View.GONE);
            ui_frm_frontleg.setVisibility(View.GONE);
            ui_frm_frontfoot.setVisibility(View.GONE);
        } else if(post == 2 && (body_part == 5 || body_part == 6 || body_part == 7)){
            ui_frm_backleg.setVisibility(View.VISIBLE);

            ui_frm_backfoot.setVisibility(View.GONE);
            ui_frm_frontbody.setVisibility(View.GONE);
            ui_frm_backhead.setVisibility(View.GONE);
            ui_frm_backbody.setVisibility(View.GONE);
            ui_frm_fronthead.setVisibility(View.GONE);
            ui_frm_frontleg.setVisibility(View.GONE);
            ui_frm_frontfoot.setVisibility(View.GONE);
        } else if(post == 1 && body_part == 3){
            ui_frm_fronthead.setVisibility(View.VISIBLE);

            ui_frm_backfoot.setVisibility(View.GONE);
            ui_frm_frontbody.setVisibility(View.GONE);
            ui_frm_frontleg.setVisibility(View.GONE);
            ui_frm_backhead.setVisibility(View.GONE);
            ui_frm_backbody.setVisibility(View.GONE);
            ui_frm_backleg.setVisibility(View.GONE);
            ui_frm_frontfoot.setVisibility(View.GONE);
        } else if(post == 1 && (body_part == 6 || body_part == 5 || body_part == 7)){

            ui_frm_frontleg.setVisibility(View.VISIBLE);

            ui_frm_backfoot.setVisibility(View.GONE);
            ui_frm_frontbody.setVisibility(View.GONE);
            ui_frm_fronthead.setVisibility(View.GONE);
            ui_frm_backhead.setVisibility(View.GONE);
            ui_frm_backbody.setVisibility(View.GONE);
            ui_frm_backleg.setVisibility(View.GONE);
            ui_frm_frontfoot.setVisibility(View.GONE);

        }else if(post == 2 && body_part == 9){

            ui_frm_backfoot.setVisibility(View.VISIBLE);

            ui_frm_fronthead.setVisibility(View.GONE);
            ui_frm_frontbody.setVisibility(View.GONE);
            ui_frm_frontleg.setVisibility(View.GONE);
            ui_frm_backhead.setVisibility(View.GONE);
            ui_frm_backbody.setVisibility(View.GONE);
            ui_frm_backleg.setVisibility(View.GONE);
            ui_frm_frontfoot.setVisibility(View.GONE);
        }else if(post == 1 && body_part == 9){

            ui_frm_frontfoot.setVisibility(View.VISIBLE);

            ui_frm_fronthead.setVisibility(View.GONE);
            ui_frm_frontbody.setVisibility(View.GONE);
            ui_frm_frontleg.setVisibility(View.GONE);
            ui_frm_backhead.setVisibility(View.GONE);
            ui_frm_backbody.setVisibility(View.GONE);
            ui_frm_backleg.setVisibility(View.GONE);
            ui_frm_backfoot.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.imv_back:

                fronthead1 = 0; fronthead2 =0; fronthead3 = 0; fronthead4 = 0; fronthead5 = 0; fronthead6 = 0; fronthead7 = 0; fronthead8 = 0; fronthead9 = 0;
                frontbody1 = 0; frontbody2 = 0; frontbody3 = 0;
                frontleg1 = 0; frontleg2 = 0; frontleg3 = 0; frontleg4 = 0; frontleg5 = 0; frontleg6 = 0; frontleg7 = 0; frontleg8 = 0; frontleg9 = 0; frontleg10 = 0; frontleg11 = 0;
                frontfoot1 = 0; frontfoot2 = 0; frontfoot3 = 0; frontfoot4 = 0; frontfoot5 = 0; frontfoot6 = 0; frontfoot7 = 0; frontfoot8 = 0; frontfoot9 = 0; frontfoot10 = 0; frontfoot11 = 0;
                backhead1 = 0; backhead2 = 0; backhead3 = 0; backhead4 = 0; backhead5 = 0; backhead6 = 0;
                backbody1 = 0; backbody2 = 0; backbody3 = 0; backbody4 = 0;
                backleg1 = 0; backleg2 = 0; backleg3 = 0; backleg4 = 0; backleg5 = 0; backleg6 = 0; backleg7 = 0; backleg8 = 0; backleg9 = 0; backleg10 = 0; backleg11 = 0; backleg12 = 0; backleg13 = 0;
                backfoot1 = 0; backfoot2 = 0; backfoot3 = 0; backfoot4 = 0; backfoot5 = 0; backfoot6 = 0; backfoot7 = 0; backfoot8 = 0; backfoot9 = 0; backfoot10 = 0; backfoot11 = 0; backfoot12 = 0; backfoot13 = 0; backfoot14 = 0; backfoot15 = 0; backfoot16 = 0; backfoot17 = 0; backfoot18 = 0; backfoot19 = 0; backfoot20 = 0; backfoot21 = 0; backfoot22 = 0;

//                Intent intent = new Intent(this, MainActivity.class);
//                startActivity(intent);
                finish();
                break;

            case R.id.imv_button_mus:

                if( fronthead1+fronthead2+fronthead3+fronthead4+fronthead5+fronthead6+fronthead7+fronthead8+fronthead9+frontbody1+frontbody2+frontbody3+frontleg1+frontleg2+frontleg3+frontleg4+frontleg5+frontleg6+frontleg7+frontleg8+frontleg9+frontleg10+frontleg11+frontfoot1+frontfoot2+frontfoot3+frontfoot4+frontfoot5+frontfoot6+frontfoot7+frontfoot8+frontfoot9+frontfoot10+frontfoot11+backhead1+backhead2+backhead3+backhead4+backhead5+backhead6+
                    backbody1+backbody2+backbody3+backbody4+backleg1+backleg2+backleg3+backleg4+backleg5+backleg6+backleg7+backleg8+backleg9+backleg10+backleg11+backleg12+backleg13+backfoot1+backfoot2+backfoot3+backfoot4+backfoot5+backfoot6+backfoot7+backfoot8+backfoot9+backfoot10+backfoot11+backfoot12+backfoot13+backfoot14+backfoot15+backfoot16+backfoot17+backfoot18+backfoot19+backfoot20+backfoot21+backfoot22 == 0){

                    showAlertDialog("Please select a body region");
                    break;

                }else {
                    Intent intent1 = new Intent(this, PainSelectDetaileActivity.class);
                    startActivity(intent1);
                    break;
                }

            case R.id.imv_body_18_1:
                if(frontbody1 == 1){
                    frontbody1 = 0;
                    ui_imvBody_18_1.setBackgroundResource(R.drawable.frontbody18_1_none);
                }else{
                    frontbody1 = 1;
                    ui_imvBody_18_1.setBackgroundResource(R.drawable.frontbody18_1);
                }
                break;
            case R.id.imv_body_18_2:
                if(frontbody2 == 1){
                    frontbody2 = 0;
                    ui_imvBody_18_2.setBackgroundResource(R.drawable.frontbody18_2_none);
                }else{
                    frontbody2 = 1;
                    ui_imvBody_18_2.setBackgroundResource(R.drawable.frontbody18_2);
                }
                break;
            case R.id.imv_body_18_3:
                if(frontbody3 == 1){
                    frontbody3 = 0;
                    ui_imvBody_18_3.setBackgroundResource(R.drawable.frontbody18_3_none);
                }else{
                    frontbody3 = 1;
                    ui_imvBody_18_3.setBackgroundResource(R.drawable.frontbody18_3);
                }
                break;

            case R.id.imv_backhead24_1:
                if(backhead1 == 1){
                    backhead1 = 0;
                    ui_imvBackHead24_1.setBackgroundResource(R.drawable.backhead24_1_none);
                }else{
                    backhead1 = 1;
                    ui_imvBackHead24_1.setBackgroundResource(R.drawable.backhead24_1);
                }
                break;

            case R.id.imv_backhead24_2:
                if(backhead2 == 1){
                    backhead2 = 0;
                    ui_imvBackHead24_2.setBackgroundResource(R.drawable.backhead24_2_none);
                }else{
                    backhead2 = 1;
                    ui_imvBackHead24_2.setBackgroundResource(R.drawable.backhead24_2);
                }
                break;

            case R.id.imv_backhead24_3:
                if(backhead3 == 1){
                    backhead3 = 0;
                    ui_imvBackHead24_3.setBackgroundResource(R.drawable.backhead24_3_none);
                }else{
                    backhead3 = 1;
                    ui_imvBackHead24_3.setBackgroundResource(R.drawable.backhead24_3);
                }
                break;

            case R.id.imv_backhead24_4:
                if(backhead4 == 1){
                    backhead4 = 0;
                    ui_imvBackHead24_4.setBackgroundResource(R.drawable.backhead24_4_none);
                }else{
                    backhead4 = 1;
                    ui_imvBackHead24_4.setBackgroundResource(R.drawable.backhead24_4);
                }
                break;

            case R.id.imv_backhead24_5:
                if(backhead5 == 1){
                    backhead5 = 0;

                    ui_imvBackHead24_5.setBackgroundResource(R.drawable.backhead24_5_none);
                    //ui_imvBackHead24_5_1.setBackgroundResource(R.drawable.back_head24_5_r_none);
                }else{
                    backhead5 = 1;
                    ui_imvBackHead24_5.setBackgroundResource(R.drawable.backhead24_5);
                    //ui_imvBackHead24_5_1.setBackgroundResource(R.drawable.back_head24_5_r);
                }
                break;

 /*           case R.id.imv_backhead24_5_1:

                if(backhead5 == 1){
                    backhead5 = 0;
                    ui_imvBackHead24_5.setBackgroundResource(R.drawable.back_head24_5_l_none);
                    ui_imvBackHead24_5_1.setBackgroundResource(R.drawable.back_head24_5_r_none);
                }else{
                    backhead5 = 1;
                    ui_imvBackHead24_5.setBackgroundResource(R.drawable.back_head24_5_l);
                    ui_imvBackHead24_5_1.setBackgroundResource(R.drawable.back_head24_5_r);
                }
                break;*/

            case R.id.imv_backhead24_6:
                if(backhead6 == 1){
                    backhead6 = 0;
                    ui_imvBackHead24_6.setBackgroundResource(R.drawable.backhead24_6_none);
                    //ui_imvBackHead24_6_1.setBackgroundResource(R.drawable.ear_r);
                }else{
                    backhead6 = 1;
                    ui_imvBackHead24_6.setBackgroundResource(R.drawable.backhead24_6);
                    //ui_imvBackHead24_6_1.setBackgroundResource(R.drawable.ear_r_none);
                }
                break;

            /*case R.id.imv_backhead24_6_1:


                if(backhead6 == 1){
                    backhead6 = 0;
                    ui_imvBackHead24_6.setBackgroundResource(R.drawable.ear_l);
                    ui_imvBackHead24_6_1.setBackgroundResource(R.drawable.ear_r);
                }else{
                    backhead6 = 1;
                    ui_imvBackHead24_6.setBackgroundResource(R.drawable.ear_l_none);
                    ui_imvBackHead24_6_1.setBackgroundResource(R.drawable.ear_r_none);
                }
                break;*/

            case R.id.imv_backbody_28_1:
                if(backbody1 == 1){
                    backbody1 = 0;
                    ui_imvBackBody28_1.setBackgroundResource(R.drawable.backbody28_1_none);
                    //ui_imvBackBody28_1_1.setBackgroundResource(R.drawable.backbody28_1_none);

                }else{
                    backbody1 = 1;
                    ui_imvBackBody28_1.setBackgroundResource(R.drawable.backbody28_1);
                    //ui_imvBackBody28_1_1.setBackgroundResource(R.drawable.bbody5);
                }
                break;

           /* case R.id.imv_backbody_28_1_1:

                if(backbody1 == 1){
                    backbody1 = 0;
                    ui_imvBackBody28_1.setBackgroundResource(R.drawable.backbody28_1_none);
                    ui_imvBackBody28_1_1.setBackgroundResource(R.drawable.backbody28_1_none);

                }else{
                    backbody1 = 1;
                    ui_imvBackBody28_1.setBackgroundResource(R.drawable.bbody3);
                    ui_imvBackBody28_1_1.setBackgroundResource(R.drawable.bbody5);
                }
                break;*/

            case R.id.imv_backbody_28_2:
                if(backbody2 == 1){
                    backbody2 = 0;
                    ui_imvBackBody28_2.setBackgroundResource(R.drawable.backbody28_2_none);
                }else{
                    backbody2 = 1;
                    ui_imvBackBody28_2.setBackgroundResource(R.drawable.backbody28_2);
                }
                break;

           /* case R.id.imv_backbody_28_2_1:

                if(backbody2 == 1){
                    backbody2 = 0;
                    ui_imvBackBody28_2.setBackgroundResource(R.drawable.backbody28_2_none);
                    ui_imvBackBody28_2_1.setBackgroundResource(R.drawable.backbody28_2_none);
                }else{
                    backbody2 = 1;
                    ui_imvBackBody28_2.setBackgroundResource(R.drawable.bbody2);
                    ui_imvBackBody28_2_1.setBackgroundResource(R.drawable.bbody4);
                }
                break;
*/


            case R.id.imv_backbody_28_3:
                if(backbody3 == 1){
                    backbody3 = 0;
                    ui_imvBackBody28_3.setBackgroundResource(R.drawable.backbody28_3_none);
                }else{
                    backbody3 = 1;
                    ui_imvBackBody28_3.setBackgroundResource(R.drawable.backbody28_3);
                }
                break;

            case R.id.imv_backbody_28_4:
                if(backbody4 == 1){
                    backbody4 = 0;
                    ui_imvBackBody28_4.setBackgroundResource(R.drawable.backbody28_4_none);
                }else{
                    backbody4 = 1;
                    ui_imvBackBody28_4.setBackgroundResource(R.drawable.backbody28_4);
                }
                break;

            case R.id.imv_backleg25_1:
                if(backleg1 == 1){
                    backleg1 = 0;
                    ui_imvBackLeg25_1.setBackgroundResource(R.drawable.backleg25_1_none);
                }else{
                    backleg1 = 1;
                    ui_imvBackLeg25_1.setBackgroundResource(R.drawable.backleg25_1);
                }
                break;

            case R.id.imv_backleg25_2:
                if(backleg2 == 1){
                    backleg2 = 0;
                    ui_imvBackLeg25_2.setBackgroundResource(R.drawable.backleg25_2_none);
                }else{
                    backleg2 = 1;
                    ui_imvBackLeg25_2.setBackgroundResource(R.drawable.backleg25_2);
                }
                break;

            case R.id.imv_backleg25_3:
                if(backleg3 == 1){
                    backleg3 = 0;
                    ui_imvBackLeg25_3.setBackgroundResource(R.drawable.backleg25_3_none);
                }else{
                    backleg3 = 1;
                    ui_imvBackLeg25_3.setBackgroundResource(R.drawable.backleg25_3);
                }
                break;

            case R.id.imv_backleg25_4:
                if(backleg4 == 1){
                    backleg4 = 0;
                    ui_imvBackLeg25_4.setBackgroundResource(R.drawable.backleg25_4_none);
                }else{
                    backleg4 = 1;
                    ui_imvBackLeg25_4.setBackgroundResource(R.drawable.backleg25_4);
                }
                break;

            case R.id.imv_backleg25_5:
                if(backleg5 == 1){
                    backleg5 = 0;
                    ui_imvBackLeg25_5.setBackgroundResource(R.drawable.backleg25_5_none);
                }else{
                    backleg5 = 1;
                    ui_imvBackLeg25_5.setBackgroundResource(R.drawable.backleg25_5);
                }
                break;

            case R.id.imv_backleg25_6:
                if(backleg6 == 1){
                    backleg6 = 0;
                    ui_imvBackLeg25_6.setBackgroundResource(R.drawable.backleg25_6_none);
                }else{
                    backleg6 = 1;
                    ui_imvBackLeg25_6.setBackgroundResource(R.drawable.backleg25_6);
                }
                break;

            case R.id.imv_backleg25_7:
                if(backleg7 == 1){
                    backleg7 = 0;
                    ui_imvBackLeg25_7.setBackgroundResource(R.drawable.backleg25_7_none);
                }else{
                    backleg7 = 1;
                    ui_imvBackLeg25_7.setBackgroundResource(R.drawable.backleg25_7);
                }
                break;

            case R.id.imv_backleg25_8:
                if(backleg8 == 1){
                    backleg8 = 0;
                    ui_imvBackLeg25_8.setBackgroundResource(R.drawable.backleg25_8_none);
                }else{
                    backleg8 = 1;
                    ui_imvBackLeg25_8.setBackgroundResource(R.drawable.backleg25_8);
                }
                break;

            case R.id.imv_backleg25_9:
                if(backleg9 == 1){
                    backleg9 = 0;
                    ui_imvBackLeg25_9.setBackgroundResource(R.drawable.backleg25_9_none);
                }else{
                    backleg9 = 1;
                    ui_imvBackLeg25_9.setBackgroundResource(R.drawable.backleg25_9);
                }
                break;

            case R.id.imv_backleg25_10:
                if(backleg10 == 1){
                    backleg10 = 0;
                    ui_imvBackLeg25_10.setBackgroundResource(R.drawable.backleg25_10_none);
                }else{
                    backleg10 = 1;
                    ui_imvBackLeg25_10.setBackgroundResource(R.drawable.backleg25_10);
                }
                break;

            case R.id.imv_backleg25_11:
                if(backleg11 == 1){
                    backleg11 = 0;
                    ui_imvBackLeg25_11.setBackgroundResource(R.drawable.backleg25_11_none);
                }else{
                    backleg11 = 1;
                    ui_imvBackLeg25_11.setBackgroundResource(R.drawable.backleg25_11);
                }
                break;

            case R.id.imv_backleg25_12:
                if(backleg12 == 1){
                    backleg12 = 0;
                    ui_imvBackLeg25_12.setBackgroundResource(R.drawable.backleg25_12_none);
                }else{
                    backleg12 = 1;
                    ui_imvBackLeg25_12.setBackgroundResource(R.drawable.backleg25_12);
                }
                break;

            case R.id.imv_backleg25_13:
                if(backleg13 == 1){
                    backleg13 = 0;
                    ui_imvBackLeg25_13.setBackgroundResource(R.drawable.backleg25_13_none);
                }else{
                    backleg13 = 1;
                    ui_imvBackLeg25_13.setBackgroundResource(R.drawable.backleg25_13);
                }
                break;

            case R.id.imv_backleg25_3_1:
                if(backleg3 == 1){
                    backleg3 = 0;
                    ui_imvBackLeg25_3.setBackgroundResource(R.drawable.backleg25_3_none);
                }else{
                    backleg3 = 1;
                    ui_imvBackLeg25_3.setBackgroundResource(R.drawable.backleg25_3);
                }
                break;

            case R.id.imv_backleg25_5_1:
                if(backleg5 == 1){
                    backleg5 = 0;
                    ui_imvBackLeg25_5.setBackgroundResource(R.drawable.backleg25_5_none);
                }else{
                    backleg5 = 1;
                    ui_imvBackLeg25_5.setBackgroundResource(R.drawable.backleg25_5);
                }
                break;

            case R.id.imv_fronthead13_1:
                if(fronthead1 == 1){
                    fronthead1 = 0;
                    ui_imvFrontHead13_1.setBackgroundResource(R.drawable.fronthead13_1_none);
                }else{
                    fronthead1 = 1;
                    ui_imvFrontHead13_1.setBackgroundResource(R.drawable.fronthead13_1);
                }
                break;

            case R.id.imv_fronthead13_2:
                if(fronthead2 == 1){
                    fronthead2 = 0;
                    ui_imvFrontHead13_2.setBackgroundResource(R.drawable.fronthead13_2_none);
                }else{
                    fronthead2 = 1;
                    ui_imvFrontHead13_2.setBackgroundResource(R.drawable.fronthead13_2);
                }
                break;

            case R.id.imv_fronthead13_3:
                if(fronthead3 == 1){
                    fronthead3 = 0;
                    ui_imvFrontHead13_3.setBackgroundResource(R.drawable.fronthead13_3_none);
                }else{
                    fronthead3 = 1;
                    ui_imvFrontHead13_3.setBackgroundResource(R.drawable.fronthead13_3);
                }
                break;

            case R.id.imv_fronthead13_4:
                if(fronthead4 == 1){
                    fronthead4 = 0;
                    ui_imvFrontHead13_4.setBackgroundResource(R.drawable.fronthead13_4_none);
                }else{
                    fronthead4 = 1;
                    ui_imvFrontHead13_4.setBackgroundResource(R.drawable.fronthead13_4);
                }
                break;

            case R.id.imv_fronthead13_5:
                if(fronthead5 == 1){
                    fronthead5 = 0;
                    ui_imvFrontHead13_5.setBackgroundResource(R.drawable.fronthead13_5_none);
                }else{
                    fronthead5 = 1;
                    ui_imvFrontHead13_5.setBackgroundResource(R.drawable.fronthead13_5);
                }
                break;

            case R.id.imv_fronthead13_6:
                if(fronthead6 == 1){
                    fronthead6 = 0;
                    ui_imvFrontHead13_6.setBackgroundResource(R.drawable.fronthead13_6_none);
                }else{
                    fronthead6 = 1;
                    ui_imvFrontHead13_6.setBackgroundResource(R.drawable.fronthead13_6);
                }
                break;

            case R.id.imv_fronthead13_7:
                if(fronthead7 == 1){
                    fronthead7 = 0;
                    ui_imvFrontHead13_7.setBackgroundResource(R.drawable.fronthead13_7_none);
                }else{
                    fronthead7 = 1;
                    ui_imvFrontHead13_7.setBackgroundResource(R.drawable.fronthead13_7);
                }
                break;

            case R.id.imv_fronthead13_8:
                if(fronthead8 == 1){
                    fronthead8 = 0;
                    ui_imvFrontHead13_8.setBackgroundResource(R.drawable.fronthead13_8_none);
                }else{
                    fronthead8 = 1;
                    ui_imvFrontHead13_8.setBackgroundResource(R.drawable.fronthead13_8);
                }
                break;

      /*      case R.id.imv_fronthead13_8_1:
                if(fronthead6 == 1){
                    fronthead6 = 0;
                    ui_imvFrontHead13_6.setBackgroundResource(R.drawable.fronthead13_6_none);
                }else{
                    fronthead6 = 1;
                    ui_imvFrontHead13_6.setBackgroundResource(R.drawable.fronthead13_6);
                }
                break;*/

            case R.id.imv_fronthead13_9:
                if(fronthead9 == 1){
                    fronthead9 = 0;
                    ui_imvFrontHead13_9.setBackgroundResource(R.drawable.fronthead13_9_none);
                }else{
                    fronthead9 = 1;
                    ui_imvFrontHead13_9.setBackgroundResource(R.drawable.fronthead13_9);
                }
                break;

            case R.id.imv_frontleg16_1:
                if(frontleg1 == 1){
                    frontleg1 = 0;
                    ui_imvFrontLeg16_1.setBackgroundResource(R.drawable.frontleg16_1_none);
                }else{
                    frontleg1 = 1;
                    ui_imvFrontLeg16_1.setBackgroundResource(R.drawable.frontleg16_1);
                }
                break;

            case R.id.imv_frontleg16_2:
                if(frontleg2 == 1){
                    frontleg2 = 0;
                    ui_imvFrontLeg16_2.setBackgroundResource(R.drawable.frontleg16_2_none);
                }else{
                    frontleg2 = 1;
                    ui_imvFrontLeg16_2.setBackgroundResource(R.drawable.frontleg16_2);
                }
                break;

            case R.id.imv_frontleg16_3:
                if(frontleg3 == 1){
                    frontleg3 = 0;
                    ui_imvFrontLeg16_3.setBackgroundResource(R.drawable.frontleg16_3_none);
                }else{
                    frontleg3 = 1;
                    ui_imvFrontLeg16_3.setBackgroundResource(R.drawable.frontleg16_3);
                }
                break;

            case R.id.imv_frontleg16_4:
                if(frontleg4 == 1){
                    frontleg4 = 0;
                    ui_imvFrontLeg16_4.setBackgroundResource(R.drawable.frontleg16_4_none);
                }else{
                    frontleg4 = 1;
                    ui_imvFrontLeg16_4.setBackgroundResource(R.drawable.frontleg16_4);
                }
                break;

            case R.id.imv_frontleg16_5:
                if(frontleg5 == 1){
                    frontleg5 = 0;
                    ui_imvFrontLeg16_5.setBackgroundResource(R.drawable.frontleg16_5_none);
                }else{
                    frontleg5 = 1;
                    ui_imvFrontLeg16_5.setBackgroundResource(R.drawable.frontleg16_5);
                }
                break;

            case R.id.imv_frontleg16_6:
                if(frontleg6 == 1){
                    frontleg6 = 0;
                    ui_imvFrontLeg16_6.setBackgroundResource(R.drawable.frontleg16_6_none);
                }else{
                    frontleg6 = 1;
                    ui_imvFrontLeg16_6.setBackgroundResource(R.drawable.frontleg16_6);
                }
                break;

            case R.id.imv_frontleg16_7:
                if(frontleg7 == 1){
                    frontleg7 = 0;
                    ui_imvFrontLeg16_7.setBackgroundResource(R.drawable.frontleg16_7_none);
                }else{
                    frontleg7 = 1;
                    ui_imvFrontLeg16_7.setBackgroundResource(R.drawable.frontleg16_7);
                }
                break;

            case R.id.imv_frontleg16_8:
                if(frontleg8 == 1){
                    frontleg8 = 0;
                    ui_imvFrontLeg16_8.setBackgroundResource(R.drawable.frontleg16_8_none);
                }else{
                    frontleg8 = 1;
                    ui_imvFrontLeg16_8.setBackgroundResource(R.drawable.frontleg16_8);
                }
                break;

            case R.id.imv_frontleg16_9:
                if(frontleg9 == 1){
                    frontleg9 = 0;
                    ui_imvFrontLeg16_9.setBackgroundResource(R.drawable.frontleg16_9_none);
                }else{
                    frontleg9 = 1;
                    ui_imvFrontLeg16_9.setBackgroundResource(R.drawable.frontleg16_9);
                }
                break;

            case R.id.imv_frontleg16_10:
                if(frontleg10 == 1){
                    frontleg10 = 0;
                    ui_imvFrontLeg16_10.setBackgroundResource(R.drawable.frontleg16_10_none);
                }else{
                    frontleg10 = 1;
                    ui_imvFrontLeg16_10.setBackgroundResource(R.drawable.frontleg16_10);
                }
                break;

            case R.id.imv_frontleg16_11:
                if(frontleg11 == 1){
                    frontleg11 = 0;
                    ui_imvFrontLeg16_11.setBackgroundResource(R.drawable.frontleg16_11_none);
                }else{
                    frontleg11 = 1;
                    ui_imvFrontLeg16_11.setBackgroundResource(R.drawable.frontleg16_11);
                }
                break;

            case R.id.imv_backfoot_29_1:
                if(backfoot1 == 1){
                    backfoot1 = 0;
                    ui_imvBackFoot29_1.setBackgroundResource(R.drawable.backfoot29_1_none);
                }else{
                    backfoot1 = 1;
                    ui_imvBackFoot29_1.setBackgroundResource(R.drawable.backfoot29_1);
                }
                break;

            case R.id.imv_backfoot_29_2:
                if(backfoot2 == 1){
                    backfoot2 = 0;
                    ui_imvBackFoot29_2.setBackgroundResource(R.drawable.backfoot29_2_none);
                }else{
                    backfoot2 = 1;
                    ui_imvBackFoot29_2.setBackgroundResource(R.drawable.backfoot29_2);
                }
                break;

            case R.id.imv_backfoot_29_3:
                if(backfoot3 == 1){
                    backfoot3 = 0;
                    ui_imvBackFoot29_3.setBackgroundResource(R.drawable.backfoot29_3_none);
                }else{
                    backfoot3 = 1;
                    ui_imvBackFoot29_3.setBackgroundResource(R.drawable.backfoot29_3);
                }
                break;

            case R.id.imv_backfoot_29_4:
                if(backfoot4 == 1){
                    backfoot4 = 0;
                    ui_imvBackFoot29_4.setBackgroundResource(R.drawable.backfoot29_4_none);
                }else{
                    backfoot4 = 1;
                    ui_imvBackFoot29_4.setBackgroundResource(R.drawable.backfoot29_4);
                }
                break;

            case R.id.imv_backfoot_29_5:
                if(backfoot5 == 1){
                    backfoot5 = 0;
                    ui_imvBackFoot29_5.setBackgroundResource(R.drawable.backfoot29_5_none);
                }else{
                    backfoot5 = 1;
                    ui_imvBackFoot29_5.setBackgroundResource(R.drawable.backfoot29_5);
                }
                break;

            case R.id.imv_backfoot_29_6:
                if(backfoot6 == 1){
                    backfoot6 = 0;
                    ui_imvBackFoot29_6.setBackgroundResource(R.drawable.backfoot29_6_none);
                }else{
                    backfoot6 = 1;
                    ui_imvBackFoot29_6.setBackgroundResource(R.drawable.backfoot29_6);
                }
                break;

            case R.id.imv_backfoot_29_7:
                if(backfoot7 == 1){
                    backfoot7 = 0;
                    ui_imvBackFoot29_7.setBackgroundResource(R.drawable.backfoot29_7_none);
                }else{
                    backfoot7 = 1;
                    ui_imvBackFoot29_7.setBackgroundResource(R.drawable.backfoot29_7);
                }
                break;

            case R.id.imv_backfoot_29_8:
                if(backfoot8 == 1){
                    backfoot8 = 0;
                    ui_imvBackFoot29_8.setBackgroundResource(R.drawable.backfoot29_8_none);
                }else{
                    backfoot8 = 1;
                    ui_imvBackFoot29_8.setBackgroundResource(R.drawable.backfoot29_8);
                }
                break;

            case R.id.imv_backfoot_29_9:
                if(backfoot9 == 1){
                    backfoot9 = 0;
                    ui_imvBackFoot29_9.setBackgroundResource(R.drawable.backfoot29_9_none);
                }else{
                    backfoot9 = 1;
                    ui_imvBackFoot29_9.setBackgroundResource(R.drawable.backfoot29_9);
                }
                break;

            case R.id.imv_backfoot_29_10:
                if(backfoot10 == 1){
                    backfoot10 = 0;
                    ui_imvBackFoot29_10.setBackgroundResource(R.drawable.backfoot29_10_none);
                }else{
                    backfoot10 = 1;
                    ui_imvBackFoot29_10.setBackgroundResource(R.drawable.backfoot29_10);
                }
                break;

            case R.id.imv_backfoot_29_11:
                if(backfoot11 == 1){
                    backfoot11 = 0;
                    ui_imvBackFoot29_11.setBackgroundResource(R.drawable.backfoot29_11_none);
                }else{
                    backfoot11 = 1;
                    ui_imvBackFoot29_11.setBackgroundResource(R.drawable.backfoot29_11);
                }
                break;

            case R.id.imv_backfoot_29_12:
                if(backfoot12 == 1){
                    backfoot12 = 0;
                    ui_imvBackFoot29_12.setBackgroundResource(R.drawable.backfoot29_12_none);
                }else{
                    backfoot12 = 1;
                    ui_imvBackFoot29_12.setBackgroundResource(R.drawable.backfoot29_12);
                }
                break;

            case R.id.imv_backfoot_29_13:
                if(backfoot13 == 1){
                    backfoot13 = 0;
                    ui_imvBackFoot29_13.setBackgroundResource(R.drawable.backfoot29_13_none);
                }else{
                    backfoot13 = 1;
                    ui_imvBackFoot29_13.setBackgroundResource(R.drawable.backfoot29_13);
                }
                break;

            case R.id.imv_backfoot_29_14:
                if(backfoot14 == 1){
                    backfoot14 = 0;
                    ui_imvBackFoot29_14.setBackgroundResource(R.drawable.backfoot29_14_none);
                }else{
                    backfoot14 = 1;
                    ui_imvBackFoot29_14.setBackgroundResource(R.drawable.backfoot29_14);
                }
                break;

            case R.id.imv_backfoot_29_15:
                if(backfoot15 == 1){
                    backfoot15 = 0;
                    ui_imvBackFoot29_15.setBackgroundResource(R.drawable.backfoot29_15_none);
                }else{
                    backfoot15 = 1;
                    ui_imvBackFoot29_15.setBackgroundResource(R.drawable.backfoot29_15);
                }
                break;


            case R.id.imv_backfoot_29_16:
                if(backfoot16 == 1){
                    backfoot16 = 0;
                    ui_imvBackFoot29_16.setBackgroundResource(R.drawable.backfoot29_16_none);
                }else{
                    backfoot16 = 1;
                    ui_imvBackFoot29_16.setBackgroundResource(R.drawable.backfoot29_16);
                }
                break;


            case R.id.imv_backfoot_29_17:
                if(backfoot17 == 1){
                    backfoot17 = 0;
                    ui_imvBackFoot29_17.setBackgroundResource(R.drawable.backfoot29_17_none);
                }else{
                    backfoot17 = 1;
                    ui_imvBackFoot29_17.setBackgroundResource(R.drawable.backfoot29_17);
                }
                break;


            case R.id.imv_backfoot_29_18:
                if(backfoot18 == 1){
                    backfoot18 = 0;
                    ui_imvBackFoot29_18.setBackgroundResource(R.drawable.backfoot29_18_none);
                }else{
                    backfoot18 = 1;
                    ui_imvBackFoot29_18.setBackgroundResource(R.drawable.backfoot29_18);
                }
                break;


            case R.id.imv_backfoot_29_19:
                if(backfoot19 == 1){
                    backfoot19 = 0;
                    ui_imvBackFoot29_19.setBackgroundResource(R.drawable.backfoot29_19_none);
                }else{
                    backfoot19 = 1;
                    ui_imvBackFoot29_19.setBackgroundResource(R.drawable.backfoot29_19);
                }
                break;


            case R.id.imv_backfoot_29_20:
                if(backfoot20 == 1){
                    backfoot20 = 0;
                    ui_imvBackFoot29_20.setBackgroundResource(R.drawable.backfoot29_20_none);
                }else{
                    backfoot20 = 1;
                    ui_imvBackFoot29_20.setBackgroundResource(R.drawable.backfoot29_20);
                }
                break;


            case R.id.imv_backfoot_29_21:
                if(backfoot21 == 1){
                    backfoot21 = 0;
                    ui_imvBackFoot29_21.setBackgroundResource(R.drawable.backfoot29_21_none);
                }else{
                    backfoot21 = 1;
                    ui_imvBackFoot29_21.setBackgroundResource(R.drawable.backfoot29_21);
                }
                break;


            case R.id.imv_backfoot_29_22:
                if(backfoot22 == 1){
                    backfoot22 = 0;
                    ui_imvBackFoot29_22.setBackgroundResource(R.drawable.backfoot29_22_none);
                }else{
                    backfoot22 = 1;
                    ui_imvBackFoot29_22.setBackgroundResource(R.drawable.backfoot29_22);
                }
                break;

            case R.id.imv_frontfoot_19_1:
                if(frontfoot1 == 1){
                    frontfoot1 = 0;
                    ui_imvFrontFoot19_1.setBackgroundResource(R.drawable.frontfoot19_1_none);
                }else{
                    frontfoot1 = 1;
                    ui_imvFrontFoot19_1.setBackgroundResource(R.drawable.frontfoot19_1);
                }
                break;

            case R.id.imv_frontfoot_19_2:
                if(frontfoot2 == 1){
                    frontfoot2 = 0;
                    ui_imvFrontFoot19_2.setBackgroundResource(R.drawable.frontfoot19_2_none);
                }else{
                    frontfoot2 = 1;
                    ui_imvFrontFoot19_2.setBackgroundResource(R.drawable.frontfoot19_2);
                }
                break;

            case R.id.imv_frontfoot_19_3:
                if(frontfoot3 == 1){
                    frontfoot3 = 0;
                    ui_imvFrontFoot19_3.setBackgroundResource(R.drawable.frontfoot19_3_none);
                }else{
                    frontfoot3 = 1;
                    ui_imvFrontFoot19_3.setBackgroundResource(R.drawable.frontfoot19_3);
                }
                break;

            case R.id.imv_frontfoot_19_4:
                if(frontfoot4 == 1){
                    frontfoot4 = 0;
                    ui_imvFrontFoot19_4.setBackgroundResource(R.drawable.frontfoot19_4_none);
                }else{
                    frontfoot4 = 1;
                    ui_imvFrontFoot19_4.setBackgroundResource(R.drawable.frontfoot19_4);
                }
                break;

            case R.id.imv_frontfoot_19_5:
                if(frontfoot5 == 1){
                    frontfoot5 = 0;
                    ui_imvFrontFoot19_5.setBackgroundResource(R.drawable.frontfoot19_5_none);
                }else{
                    frontfoot5 = 1;
                    ui_imvFrontFoot19_5.setBackgroundResource(R.drawable.frontfoot19_5);
                }
                break;

            case R.id.imv_frontfoot_19_6:
                if(frontfoot6 == 1){
                    frontfoot6 = 0;
                    ui_imvFrontFoot19_6.setBackgroundResource(R.drawable.frontfoot19_6_none);
                }else{
                    frontfoot6 = 1;
                    ui_imvFrontFoot19_6.setBackgroundResource(R.drawable.frontfoot19_6);
                }
                break;

            case R.id.imv_frontfoot_19_7:
                if(frontfoot7 == 1){
                    frontfoot7 = 0;
                    ui_imvFrontFoot19_7.setBackgroundResource(R.drawable.frontfoot19_7_none);
                }else{
                    frontfoot7 = 1;
                    ui_imvFrontFoot19_7.setBackgroundResource(R.drawable.frontfoot19_7);
                }
                break;

            case R.id.imv_frontfoot_19_8:
                if(frontfoot8 == 1){
                    frontfoot8 = 0;
                    ui_imvFrontFoot19_8.setBackgroundResource(R.drawable.frontfoot19_8_none);
                }else{
                    frontfoot8 = 1;
                    ui_imvFrontFoot19_8.setBackgroundResource(R.drawable.frontfoot19_8);
                }
                break;

            case R.id.imv_frontfoot_19_9:
                if(frontfoot9 == 1){
                    frontfoot9 = 0;
                    ui_imvFrontFoot19_9.setBackgroundResource(R.drawable.frontfoot19_9_none);
                }else{
                    frontfoot9 = 1;
                    ui_imvFrontFoot19_9.setBackgroundResource(R.drawable.frontfoot19_9);
                }
                break;

            case R.id.imv_frontfoot_19_10:
                if(frontfoot10 == 1){
                    frontfoot10 = 0;
                    ui_imvFrontFoot19_10.setBackgroundResource(R.drawable.frontfoot19_10_none);
                }else{
                    frontfoot10 = 1;
                    ui_imvFrontFoot19_10.setBackgroundResource(R.drawable.frontfoot19_10);
                }
                break;

            case R.id.imv_frontfoot_19_11:
                if(frontfoot11 == 1){
                    frontfoot11 = 0;
                    ui_imvFrontFoot19_11.setBackgroundResource(R.drawable.frontfoot19_11_none);
                }else{
                    frontfoot11 = 1;
                    ui_imvFrontFoot19_11.setBackgroundResource(R.drawable.frontfoot19_11);
                }
                break;

            case R.id.imv_frontfoot_19_15:
                if(backfoot15 == 1){
                    backfoot15 = 0;
                    ui_imvFrontFoot19_15.setBackgroundResource(R.drawable.backfoot29_15_none);
                }else{
                    backfoot15 = 1;
                    ui_imvFrontFoot19_15.setBackgroundResource(R.drawable.backfoot29_15);
                }
                break;


            case R.id.imv_frontfoot_19_16:
                if(backfoot16 == 1){
                    backfoot16 = 0;
                    ui_imvFrontFoot19_16.setBackgroundResource(R.drawable.backfoot29_16_none);
                }else{
                    backfoot16 = 1;
                    ui_imvFrontFoot19_16.setBackgroundResource(R.drawable.backfoot29_16);
                }
                break;


            case R.id.imv_frontfoot_19_17:
                if(backfoot17 == 1){
                    backfoot17 = 0;
                    ui_imvFrontFoot19_17.setBackgroundResource(R.drawable.backfoot29_17_none);
                }else{
                    backfoot17 = 1;
                    ui_imvFrontFoot19_17.setBackgroundResource(R.drawable.backfoot29_17);
                }
                break;


            case R.id.imv_frontfoot_19_18:
                if(backfoot18 == 1){
                    backfoot18 = 0;
                    ui_imvFrontFoot19_18.setBackgroundResource(R.drawable.backfoot29_18_none);
                }else{
                    backfoot18 = 1;
                    ui_imvFrontFoot19_18.setBackgroundResource(R.drawable.backfoot29_18);
                }
                break;


            case R.id.imv_frontfoot_19_19:
                if(backfoot19 == 1){
                    backfoot19 = 0;
                    ui_imvFrontFoot19_19.setBackgroundResource(R.drawable.backfoot29_19_none);
                }else{
                    backfoot19 = 1;
                    ui_imvFrontFoot19_19.setBackgroundResource(R.drawable.backfoot29_19);
                }
                break;


            case R.id.imv_frontfoot_19_20:
                if(backfoot20 == 1){
                    backfoot20 = 0;
                    ui_imvFrontFoot19_20.setBackgroundResource(R.drawable.backfoot29_20_none);
                }else{
                    backfoot20 = 1;
                    ui_imvFrontFoot19_20.setBackgroundResource(R.drawable.backfoot29_20);
                }
                break;


            case R.id.imv_frontfoot_19_21:
                if(backfoot21 == 1){
                    backfoot21 = 0;
                    ui_imvFrontFoot19_21.setBackgroundResource(R.drawable.backfoot29_21_none);
                }else{
                    backfoot21 = 1;
                    ui_imvFrontFoot19_21.setBackgroundResource(R.drawable.backfoot29_21);
                }
                break;


            case R.id.imv_frontfoot_19_22:
                if(backfoot22 == 1){
                    backfoot22 = 0;
                    ui_imvFrontFoot19_22.setBackgroundResource(R.drawable.backfoot29_22_none);
                }else{
                    backfoot22 = 1;
                    ui_imvFrontFoot19_22.setBackgroundResource(R.drawable.backfoot29_22);
                }
                break;

        }

    }

}

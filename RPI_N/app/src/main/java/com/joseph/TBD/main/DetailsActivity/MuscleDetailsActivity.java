package com.joseph.TBD.main.DetailsActivity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.PainSelcection.PainSelectActivity;

import static com.joseph.TBD.commons.Constants.anconiusstatus;
import static com.joseph.TBD.commons.Constants.array_image1;
import static com.joseph.TBD.commons.Constants.array_imageLigament;
import static com.joseph.TBD.commons.Constants.array_imagePeriosteal1;
import static com.joseph.TBD.commons.Constants.array_ligamentList;
import static com.joseph.TBD.commons.Constants.jointList;
import static com.joseph.TBD.commons.Constants.joint_image;
import static com.joseph.TBD.commons.Constants.muscleList;
import static com.joseph.TBD.commons.Constants.periostealList;
import static com.joseph.TBD.commons.Constants.position;

public class MuscleDetailsActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack , ui_imvContent, ui_imvDetails;
    TextView ui_txvTitle ;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muscle_details);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_imvContent = (ImageView)findViewById(R.id.imv_detailsImg);
       // ui_imvContent.setImageResource(R.drawable.anconius_details);
        ui_imvDetails = (ImageView)findViewById(R.id.imv_button);

        ui_txvTitle = (TextView)findViewById(R.id.txv_title);

        if(Constants.select_Status == 1) {

            ui_imvContent.setImageResource(array_image1.get(position));
            ui_txvTitle.setText(muscleList.get(position));

        } else if(Constants.select_Status == 4){

            ui_imvContent.setImageResource(array_imagePeriosteal1.get(position));
            ui_txvTitle.setText(periostealList.get(position));

        } else if(Constants.select_Status == 2){

            ui_imvContent.setImageResource(array_imageLigament.get(position));
            ui_txvTitle.setText(array_ligamentList.get(position));

        } else if(Constants.select_Status == 3){

            ui_imvContent.setImageResource(joint_image.get(position));
            ui_txvTitle.setText(jointList.get(position));
        }

        ui_imvDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anconiusstatus = 0;
                Intent intent = new Intent(MuscleDetailsActivity.this, AnconiusDetailsActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:

                finish();
                break;
        }

    }
}

package com.joseph.TBD.main.SearchByTissureActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.joseph.TBD.R;
import com.joseph.TBD.adapter.CommonlistAdapter;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.DetailsActivity.MuscleDetailsActivity;
import com.joseph.TBD.main.MainActivity;

import java.util.Locale;

public class PeriostealActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack ,ui_imvCacel ;
    EditText ui_edtSearch;
    ListView ui_lstContainer ;
    CommonlistAdapter _adapter_per;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_periosteal);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);
        ui_edtSearch = (EditText)findViewById(R.id.edt_search);
        ui_imvCacel = (ImageView)findViewById(R.id.imv_cancel);
        ui_imvCacel.setOnClickListener(this);

        _adapter_per = new CommonlistAdapter(this, Constants.periostealList);
        ui_lstContainer = (ListView)findViewById(R.id.lst_contaner_perio);

        ui_lstContainer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String name = _adapter_per._contents.get(position);

                if(name.length() != 1) {
                    for (int i = 0; i < Constants.periostealList.size(); i++) {

                        if (Constants.periostealList.get(i).equals(name))
                        {
                            position = i;
                            Log.d("PositionNew=======>", String.valueOf(position));
                        }
                    }
                    Constants.select_Status = 4;
                    Constants.position = position;
                    Intent intent = new Intent(PeriostealActivity.this, MuscleDetailsActivity.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }

            }
        });

        ui_edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (ui_edtSearch.getText().length()>0){
                    ui_imvCacel.setVisibility(View.VISIBLE);
                }else {
                    ui_imvCacel.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

                String name = ui_edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                _adapter_per.filter(name);

            }
        });

        _adapter_per.addItem(Constants.periostealList);
        _adapter_per.initProducts();
        _adapter_per.notifyDataSetChanged();
        ui_lstContainer.setAdapter(_adapter_per);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:

                finish();
                break;


            case R.id.imv_cancel:

                ui_edtSearch.setText("");
                break;

        }

    }
}

package com.joseph.TBD.main;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.joseph.TBD.R;
import com.joseph.TBD.commons.Constants;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        gotoMainActivity();
    }

    private void gotoMainActivity() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, SecondSplashActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        },Constants.SPLASH_TIME);
    }
}

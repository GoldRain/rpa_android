package com.joseph.TBD.adapter;

import android.content.Context;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.main.PainSelcection.PainSelectDetaileActivity;
import com.joseph.TBD.model.ItemModel;

import java.util.ArrayList;

import static com.joseph.TBD.commons.Constants.array_image1;
import static com.joseph.TBD.commons.Constants.array_imageLigament;
import static com.joseph.TBD.commons.Constants.array_imagePeriosteal1;
import static com.joseph.TBD.commons.Constants.array_ligamentList;
import static com.joseph.TBD.commons.Constants.jointList;
import static com.joseph.TBD.commons.Constants.joint_image;
import static com.joseph.TBD.commons.Constants.muscleList;
import static com.joseph.TBD.commons.Constants.periostealList;
import static com.joseph.TBD.commons.Constants.screenheight;
import static com.joseph.TBD.commons.Constants.screenwidth;


public class StoreAdapter extends BaseAdapter{

    private ArrayList<ItemModel> _userDatas = new ArrayList<>();
    private ArrayList<ItemModel> _allUserDatas = new ArrayList<>();

    private LayoutInflater inflater;

    public StoreAdapter(Context context)
    {
        inflater = LayoutInflater.from(context);

    }

    public void setUserDatas(ArrayList<ItemModel> users) {

        _allUserDatas = users;
        _userDatas.clear();
        _userDatas.addAll(_allUserDatas);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return _userDatas.size();
    }

    @Override
    public Object getItem(int i) {
        return _userDatas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = view;
        ImageView picture;
        TextView name;

        if(v == null){

            v = inflater.inflate(R.layout.store_item, viewGroup, false);
            v.setTag(R.id.picture, v.findViewById(R.id.picture));
            v.setTag(R.id.txv_name, v.findViewById(R.id.txv_name));
        }

        picture = (ImageView) v.getTag(R.id.picture);
        name = (TextView)v.getTag(R.id.txv_name);

        picture.getLayoutParams().height = screenheight / 18*8 ;

        picture.getLayoutParams().width = screenwidth / 2;

        name.getLayoutParams().height = screenheight / 18;
        name.getLayoutParams().width = screenwidth / 2;

        ItemModel item = (ItemModel) getItem(i);

        int position1 = item.getPosition();
        int select_status = item.getSelect_status();

        if(select_status == 1) {
            picture.setImageResource(array_image1.get(position1));
            name.setText(muscleList.get(position1));
        } else if(select_status == 4){
            picture.setImageResource(array_imagePeriosteal1.get(position1));
            name.setText(periostealList.get(position1));
        } else if(select_status == 2){

            picture.setImageResource(array_imageLigament.get(position1));
            name.setText(array_ligamentList.get(position1));

        } else if(select_status == 3){
            picture.setImageResource(joint_image.get(position1));
            name.setText(jointList.get(position1));
        }

        return v;
    }

}

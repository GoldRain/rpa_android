package com.joseph.TBD.main;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.fragment.HomeFragment;
import com.joseph.TBD.fragment.SearchFragment;
import com.joseph.TBD.fragment.TreatmentFragment;

import static com.joseph.TBD.commons.Constants.array_image1;
import static com.joseph.TBD.commons.Constants.array_imageLigament;
import static com.joseph.TBD.commons.Constants.array_imagePeriosteal1;
import static com.joseph.TBD.commons.Constants.array_ligamentList;
import static com.joseph.TBD.commons.Constants.jointList;
import static com.joseph.TBD.commons.Constants.joint_image;
import static com.joseph.TBD.commons.Constants.periostealList;

public class MainActivity extends CommonActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    ImageView ui_imvDrawer;
    ImageView ui_imvTreatment,ui_imvSearch, ui_imvHelp, ui_imvLogo ;
    TextView ui_txvTreatment, ui_txvSearch;

    NavigationView ui_drawerMenu;
    DrawerLayout ui_drawerLayout;
    ActionBarDrawerToggle drawerToggle ;

    LinearLayout ui_homeFrag, ui_searchFrag, ui_empty, ui_treatFrag;

    View headerView ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadLayout();
        setupNavigationBar();
    }

    private void loadLayout() {

        ui_drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);

        ui_imvDrawer = (ImageView)findViewById(R.id.imv_drawer);
        ui_imvDrawer.setOnClickListener(this);

        ui_imvLogo = (ImageView)findViewById(R.id.imv_logo);

        ui_imvSearch = (ImageView)findViewById(R.id.imv_search);
        ui_txvSearch = (TextView)findViewById(R.id.txv_title_search);

        ui_imvTreatment = (ImageView)findViewById(R.id.imv_treatment);
        ui_txvTreatment = (TextView)findViewById(R.id.txv_title_treatment);

        ui_imvHelp = (ImageView)findViewById(R.id.imv_help);
        ui_imvHelp.setOnClickListener(this);

        ui_drawerMenu = (NavigationView)findViewById(R.id.drawer_menu);
        ui_drawerMenu.setNavigationItemSelectedListener(this);
        ui_drawerLayout.addDrawerListener(drawerToggle);

        if(array_ligamentList.size() < 1){

            array_ligamentList.add("A");
            array_ligamentList.add("Acromioclavicular");
            array_ligamentList.add("Anterior and Posterior Cruciate");
            array_ligamentList.add("C");
            array_ligamentList.add("Cervical Facet Capsule (Lower)");
            array_ligamentList.add("Cervical Facet Capsule (Mid)");
            array_ligamentList.add("Cervical Facet Capsule (Upper)");
            array_ligamentList.add("Costosternal");
            array_ligamentList.add("Costotransverse");
            array_ligamentList.add("D");
            array_ligamentList.add("Deltoid");
            array_ligamentList.add("Dorsal Carpal");
            array_ligamentList.add("E");
            array_ligamentList.add("Extensor Retinaculum (Ankle)");
            array_ligamentList.add("F");
            array_ligamentList.add("Femoralacetabular Joint Capsule");
            array_ligamentList.add("G");
            array_ligamentList.add("Glenohumeral Joint Capsule");
            array_ligamentList.add("I");
            array_ligamentList.add("Iliolumbar");
            array_ligamentList.add("Inguinal");
            array_ligamentList.add("Interosseous Membrane (Forearm)");
            array_ligamentList.add("Interosseous Membrane (Shin)");
            array_ligamentList.add("L");
            array_ligamentList.add("Lateral Collateral (Knee)");
            array_ligamentList.add("Lumbar Facet Capsule");
            array_ligamentList.add("M");
            array_ligamentList.add("Medial Collateral (Knee)");
            array_ligamentList.add("P");
            array_ligamentList.add("Palmer Carpal");
            array_ligamentList.add("Pubic Symphysis");
            array_ligamentList.add("R");
            array_ligamentList.add("Radial Collateral");
            array_ligamentList.add("S");
            array_ligamentList.add("Sacroiliac");
            array_ligamentList.add("Sacrospinous");
            array_ligamentList.add("Sacrotuberous");
            array_ligamentList.add("Sternoclavicular");
            array_ligamentList.add("supraspinous (C5)");
            array_ligamentList.add("supraspinous (C6)");
            array_ligamentList.add("supraspinous (C7)");
            array_ligamentList.add("supraspinous (L3)");
            array_ligamentList.add("supraspinous (L4)");
            array_ligamentList.add("supraspinous (L5)");
            array_ligamentList.add("supraspinous (S1)");
            array_ligamentList.add("supraspinous (T1)");
            array_ligamentList.add("T");
            array_ligamentList.add("Talofibular");
            array_ligamentList.add("Thoracic Facet Capsule (Upper)");
            array_ligamentList.add("Thoracolumbar Facet Capsule");
            array_ligamentList.add("U");
            array_ligamentList.add("Ulnar Collateral");
        }

        if(periostealList.size() < 1){

            periostealList.add("A");
            periostealList.add("Angle of the Ribs");
            periostealList.add("C");
            periostealList.add("C2 Spinous Process");
            periostealList.add("Calcaneal Spur");
            periostealList.add("Clavicle (Sternal End)");
            periostealList.add("Coccyx");
            periostealList.add("D");
            periostealList.add("Deltoid Insertion");
            periostealList.add("E");
            periostealList.add("Erb's Point");
            periostealList.add("F");
            periostealList.add("Fibular Head");
            periostealList.add("I");
            periostealList.add("Iliac Crest");
            periostealList.add("Ischial Tuberosity");
            periostealList.add("K");
            periostealList.add("Knee Joint Line");
            periostealList.add("L");
            periostealList.add("Lateral Epicondyle");
            periostealList.add("Lateral Pubic Symphysis");
            periostealList.add("M");
            periostealList.add("Mandibular Condyle");
            periostealList.add("Medial Epicondyle");
            periostealList.add("Metatarsal Head");
            periostealList.add("N");
            periostealList.add("Nuchal Line");
            periostealList.add("P");
            periostealList.add("Pes Anserine");
            periostealList.add("Posterior Foramen Magnum");
            periostealList.add("R");
            periostealList.add("Radial Styloid");
            periostealList.add("Ribs (Anterior)");
            periostealList.add("Ribs (Axilla)");
            periostealList.add("S");
            periostealList.add("Spinous Process");
            periostealList.add("Sternalcostal (1st Rib)");
            periostealList.add("Sternoclavicular");
            periostealList.add("Superior Patella");
            periostealList.add("Superior Pubic Symphysis");
            periostealList.add("T");
            periostealList.add("Transverse Process of Atlas");
            periostealList.add("X");
            periostealList.add("Xiphoid");
        }

        if(Constants.muscleList.size() < 1) {
            Constants.muscleList.add("A");
            Constants.muscleList.add("Abductor Digiti Minimi (foot)");
            Constants.muscleList.add("Abductor Digiti Minimi (hand)");
            Constants.muscleList.add("Abductor Hallucis");
            Constants.muscleList.add("Adductor Hallicis");
            Constants.muscleList.add("Adductor Longus and Brevis");
            Constants.muscleList.add("Adductor Magnus");
            Constants.muscleList.add("Adductor Pollicis");
            Constants.muscleList.add("Anconeus");
            Constants.muscleList.add("Anterior Deltoid");
            Constants.muscleList.add("B");
            Constants.muscleList.add("Biceps Brachii");
            Constants.muscleList.add("Biceps Femoris");
            Constants.muscleList.add("Brachialis");
            Constants.muscleList.add("Brachioradialis");
            Constants.muscleList.add("Buccinator");
            Constants.muscleList.add("C");
            Constants.muscleList.add("Coracobrachialis");
            Constants.muscleList.add("D");
            Constants.muscleList.add("Diagastric 1");
            Constants.muscleList.add("Diagastric 2");
            Constants.muscleList.add("Diaphragm");
            Constants.muscleList.add("E");
            Constants.muscleList.add("Extensor Carpi Radialis Brevis");
            Constants.muscleList.add("Extensor Carpi Radialis Longus");
            Constants.muscleList.add("Extensor Carpi Ulnaris");
            Constants.muscleList.add("Extensor Digitorum 1");
            Constants.muscleList.add("Extensor Digitorum 2");
            Constants.muscleList.add("Extensor Digitorum Brevis");
            Constants.muscleList.add("Extensor Digitorum Longus");
            Constants.muscleList.add("Extensor Hallucis Brevis");
            Constants.muscleList.add("Extensor Hallucis Longus");
            Constants.muscleList.add("Extensor Indicis");
            Constants.muscleList.add("External Oblique");
            Constants.muscleList.add("F");
            Constants.muscleList.add("First Dorsal Interosseous of the Foot");
            Constants.muscleList.add("First Dorsal Interosseous of the Hand");
            Constants.muscleList.add("Flexor Carpi Radialis");
            Constants.muscleList.add("Flexor Carpi Ulnaris");
            Constants.muscleList.add("Flexor Digitorum Brevis");
            Constants.muscleList.add("Flexor Digitorum Longus");
            Constants.muscleList.add("Flexor Digitorum Profundus");
            Constants.muscleList.add("Flexor Digitorum Superficialis");
            Constants.muscleList.add("Flexor Hallucis Brevis");
            Constants.muscleList.add("Flexor Hallucis Longus");
            Constants.muscleList.add("Flexor Pollicis Longus");
            Constants.muscleList.add("G");
            Constants.muscleList.add("Gastrocnemius 1");
            Constants.muscleList.add("Gastrocnemius 2");
            Constants.muscleList.add("Gastrocnemius 3");
            Constants.muscleList.add("Gastrocnemius 4");
            Constants.muscleList.add("Gluteus Maximus 1");
            Constants.muscleList.add("Gluteus Maximus 2");
            Constants.muscleList.add("Gluteus Maximus 3");
            Constants.muscleList.add("Gluteus Medius 1");
            Constants.muscleList.add("Gluteus Medius 2");
            Constants.muscleList.add("Gluteus Medius 3");
            Constants.muscleList.add("Gluteus Minimus 1");
            Constants.muscleList.add("Gluteus Minimus 2");
            Constants.muscleList.add("Gracilis");
            Constants.muscleList.add("I");
            Constants.muscleList.add("Iliopsoas");
            Constants.muscleList.add("Infraspinatus");
            Constants.muscleList.add("Intercostal");
            Constants.muscleList.add("L");
            Constants.muscleList.add("Lateral Oblique");
            Constants.muscleList.add("Lateral Pterygoid");
            Constants.muscleList.add("Latissimus Dorsi 1");
            Constants.muscleList.add("Latissimus Dorsi 2");
            Constants.muscleList.add("Levator Scapulae");
            Constants.muscleList.add("M");
            Constants.muscleList.add("Masseter 1");
            Constants.muscleList.add("Masseter 2");
            Constants.muscleList.add("Masseter 3");
            Constants.muscleList.add("Masseter 4");
            Constants.muscleList.add("Medial Pterygoid");
            Constants.muscleList.add("Middle Deltoid");
            Constants.muscleList.add("Multifidus (Cervical)");
            Constants.muscleList.add("Multifidus (Thoracic and Lumbar)");
            Constants.muscleList.add("O");
            Constants.muscleList.add("Obturator Internus");
            Constants.muscleList.add("Occipitalis");
            Constants.muscleList.add("Occipitofrontalis");
            Constants.muscleList.add("Opponens Pollicis");
            Constants.muscleList.add("Orbicularis Oculi");
            Constants.muscleList.add("P");
            Constants.muscleList.add("Palmaris Longus");
            Constants.muscleList.add("Paraspinal (Mid Thoracic)");
            Constants.muscleList.add("Paraspinal (Thoracolumbar)");
            Constants.muscleList.add("Pectineus");
            Constants.muscleList.add("Pectoralis Major 1");
            Constants.muscleList.add("Pectoralis Major 2");
            Constants.muscleList.add("Pectoralis Major 3");
            Constants.muscleList.add("Pectoralis Major 4");
            Constants.muscleList.add("Pectoralis Minor");
            Constants.muscleList.add("Pelvic Floor");
            Constants.muscleList.add("Peroneus Brevis");
            Constants.muscleList.add("Peroneus Longus");
            Constants.muscleList.add("Peroneus Tertius");
            Constants.muscleList.add("Piriformis 1");
            Constants.muscleList.add("Piriformis 2");
            Constants.muscleList.add("Plantaris");
            Constants.muscleList.add("Platysma");
            Constants.muscleList.add("Popliteus");
            Constants.muscleList.add("Posterior Deltoid");
            Constants.muscleList.add("Pronator Teres");
            Constants.muscleList.add("Q");
            Constants.muscleList.add("Quadratus Lumborum 1");
            Constants.muscleList.add("Quadratus Lumborum 2");
            Constants.muscleList.add("Quadratus Lumborum 3");
            Constants.muscleList.add("Quadratus Lumborum 4");
            Constants.muscleList.add("Quadratus Plantae");
            Constants.muscleList.add("R");
            Constants.muscleList.add("Rectus Abdominis");
            Constants.muscleList.add("Rectus Femoris");
            Constants.muscleList.add("Rhomboid");
            Constants.muscleList.add("S");
            Constants.muscleList.add("Sartorius");
            Constants.muscleList.add("Scalene");
            Constants.muscleList.add("Scalenus Minimus");
            Constants.muscleList.add("Second Dorsal Interosseous of the Hand");
            Constants.muscleList.add("Semimembranosus and Semitendinosus");
            Constants.muscleList.add("Semispinalis Capitis 1");
            Constants.muscleList.add("Semispinalis Capitis 2");
            Constants.muscleList.add("Serratus Anterior");
            Constants.muscleList.add("Serratus Posterior Inferior");
            Constants.muscleList.add("Serratus Posterior Superior");
            Constants.muscleList.add("Soleus 1");
            Constants.muscleList.add("Soleus 2");
            Constants.muscleList.add("Soleus 3");
            Constants.muscleList.add("Soleus 4");
            Constants.muscleList.add("Splenius Cervicis 1");
            Constants.muscleList.add("Splenius Cervicis 2");
            Constants.muscleList.add("Sternalis");
            Constants.muscleList.add("Sternocleidomastoid 1");
            Constants.muscleList.add("Sternocleidomastoid 2");
            Constants.muscleList.add("Subclavius");
            Constants.muscleList.add("Suboccipital");
            Constants.muscleList.add("Subscapularis");
            Constants.muscleList.add("Supinator");
            Constants.muscleList.add("supraspinatus 1");
            Constants.muscleList.add("supraspinatus 2");
            Constants.muscleList.add("T");
            Constants.muscleList.add("Temporalis 1");
            Constants.muscleList.add("Temporalis 2");
            Constants.muscleList.add("Temporalis 3");
            Constants.muscleList.add("Temporalis 4");
            Constants.muscleList.add("Tensor Fasciae Latae");
            Constants.muscleList.add("Teres Major");
            Constants.muscleList.add("Teres Minor");
            Constants.muscleList.add("Tibialis Anterior");
            Constants.muscleList.add("Tibialis Posterior");
            Constants.muscleList.add("Trapezius 1");
            Constants.muscleList.add("Trapezius 2");
            Constants.muscleList.add("Trapezius 3");
            Constants.muscleList.add("Trapezius 4");
            Constants.muscleList.add("Trapezius 5");
            Constants.muscleList.add("Trapezius 6");
            Constants.muscleList.add("Trapezius 7");
            Constants.muscleList.add("Triceps Brachii 1");
            Constants.muscleList.add("Triceps Brachii 2");
            Constants.muscleList.add("Triceps Brachii 3");
            Constants.muscleList.add("Triceps Brachii 4");
            Constants.muscleList.add("Triceps Brachii 5");
            Constants.muscleList.add("V");
            Constants.muscleList.add("Vastus Intermedius");
            Constants.muscleList.add("Vastus Lateralis 1");
            Constants.muscleList.add("Vastus Lateralis 2");
            Constants.muscleList.add("Vastus Lateralis 3");
            Constants.muscleList.add("Vastus Lateralis 4");
            Constants.muscleList.add("Vastus Lateralis 5");
            Constants.muscleList.add("Vastus Medialis 1");
            Constants.muscleList.add("Vastus Medialis 2");
            Constants.muscleList.add("Z");
            Constants.muscleList.add("Zygomaticus Major");
        }

        if(jointList.size() < 1){

            jointList.add("D");
            jointList.add("Dura (Cervicothoracic)");
            jointList.add("Dura (Thoracic)");
            jointList.add("Dura (Thoracolumbar)");
            jointList.add("F");
            jointList.add("Facet C0-C1-C2");
            jointList.add("Facet C2-C3");
            jointList.add("Facet C3-C4");
            jointList.add("Facet C4-C5");
            jointList.add("Facet C5-C6");
            jointList.add("Facet C6-C7");
            jointList.add("Facet C7-T1");
            jointList.add("Facet Lumbar");
            jointList.add("Facet T1-T2");
            jointList.add("Facet T11-T12");
            jointList.add("Facet T3-T4");
            jointList.add("Facet T5-T6");
            jointList.add("Facet T6-T7");
            jointList.add("Facet T7-T11");
            jointList.add("FacetT2-T3");
            jointList.add("FacetT4-T5");
            jointList.add("S");
            jointList.add("Sacroiliac Joint");
            jointList.add("Sclerotome C1");
            jointList.add("Sclerotome C2");
            jointList.add("Sclerotome C3");
            jointList.add("Sclerotome C4");
            jointList.add("Sclerotome C5");
            jointList.add("Sclerotome C6");
            jointList.add("Sclerotome C7");
            jointList.add("Sclerotome C8");
            jointList.add("Sclerotome L1");
            jointList.add("Sclerotome L2");
            jointList.add("Sclerotome L3");
            jointList.add("Sclerotome L4");
            jointList.add("Sclerotome L5");
            jointList.add("Sclerotome S1");
            jointList.add("Sclerotome T1");
            jointList.add("Sclerotome T10");
            jointList.add("Sclerotome T11");
            jointList.add("Sclerotome T12");
            jointList.add("Sclerotome T2");
            jointList.add("Sclerotome T3");
            jointList.add("Sclerotome T4");
            jointList.add("Sclerotome T5");
            jointList.add("Sclerotome T6");
            jointList.add("Sclerotome T7");
            jointList.add("Sclerotome T8");
            jointList.add("Sclerotome T9");
            jointList.add("T");
            jointList.add("Thermatome (T1-T2)");
            jointList.add("Thermatome (T1-T4)");
            jointList.add("Thermatome (T11-L2)");
            jointList.add("Thermatome (T2-T7)");
            jointList.add("Thermatome (T4-L2)");
        }

        if(array_image1.size() < 1) {
            array_image1.add(0);
            array_image1.add(R.mipmap.abductordigitiminimi_foot_s);
            array_image1.add(R.mipmap.abductordigitiminimi_hand_s);
            array_image1.add(R.mipmap.abductorhallucis_s);
            array_image1.add(R.mipmap.adductorhallicis_s);
            array_image1.add(R.mipmap.adductorlongusandbrevis_s);
            array_image1.add(R.mipmap.adductormagnus_s);
            array_image1.add(R.mipmap.adductorpollicis_s);
            array_image1.add(R.mipmap.anconeus_s);
            array_image1.add(R.mipmap.anteriordeltoid_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.bicepsbrachii_s);
            array_image1.add(R.mipmap.bicepsfemoris_s);
            array_image1.add(R.mipmap.brachialis_s);
            array_image1.add(R.mipmap.brachioradialis_s);
            array_image1.add(R.mipmap.buccinator_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.coracobrachialis_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.diagastric1_s);
            array_image1.add(R.mipmap.diagastric2_s);
            array_image1.add(R.mipmap.diaphragm_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.extensorcarpiradialisbrevis_s);
            array_image1.add(R.mipmap.extensorcarpiradialislongus_s);
            array_image1.add(R.mipmap.extensorcarpiulnaris_s);
            array_image1.add(R.mipmap.extensordigitorum1_s);
            array_image1.add(R.mipmap.extensordigitorum2_s);
            array_image1.add(R.mipmap.extensordigitorumbrevis_s);
            array_image1.add(R.mipmap.extensordigitorumlongus_s);
            array_image1.add(R.mipmap.extensorhallucisbrevis_s);
            array_image1.add(R.mipmap.extensorhallucislongus_s);
            array_image1.add(R.mipmap.extensorindicis_s);
            array_image1.add(R.mipmap.externaloblique_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.firstdorsalinterosseousofthefoot_s);
            array_image1.add(R.mipmap.firstdorsalinterosseousofthehand_s);
            array_image1.add(R.mipmap.flexorcarpiradialis_s);
            array_image1.add(R.mipmap.flexorcarpiulnaris_s);
            array_image1.add(R.mipmap.flexordigitorumbrevis_s);
            array_image1.add(R.mipmap.flexordigitorumlongus_s);
            array_image1.add(R.mipmap.flexordigitorumprofundus_s);
            array_image1.add(R.mipmap.flexordigitorumsuperficialis_s);
            array_image1.add(R.mipmap.flexorhallucisbrevis_s);
            array_image1.add(R.mipmap.flexorhallucislongus_s);
            array_image1.add(R.mipmap.flexorpollicislongus_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.gastrocnemius1_s);
            array_image1.add(R.mipmap.gastrocnemius2_s);
            array_image1.add(R.mipmap.gastrocnemius3_s);
            array_image1.add(R.mipmap.gastrocnemius4_s);
            array_image1.add(R.mipmap.gluteusmaximus1_s);
            array_image1.add(R.mipmap.gluteusmaximus2_s);
            array_image1.add(R.mipmap.gluteusmaximus3_s);
            array_image1.add(R.mipmap.gluteusmedius1_s);
            array_image1.add(R.mipmap.gluteusmedius2_s);
            array_image1.add(R.mipmap.gluteusmedius3_s);
            array_image1.add(R.mipmap.gluteusminimus1_s);
            array_image1.add(R.mipmap.gluteusminimus2_s);
            array_image1.add(R.mipmap.gracilis_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.iliopsoas_s);
            array_image1.add(R.mipmap.infraspinatus_s);
            array_image1.add(R.mipmap.intercostal_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.lateraloblique_s);
            array_image1.add(R.mipmap.lateralpterygoid_s);
            array_image1.add(R.mipmap.latissimusdorsi1_s);
            array_image1.add(R.mipmap.latissimusdorsi2_s);
            array_image1.add(R.mipmap.levatorscapulae_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.masseter1_s);
            array_image1.add(R.mipmap.masseter2_s);
            array_image1.add(R.mipmap.masseter3_s);
            array_image1.add(R.mipmap.masseter4_s);
            array_image1.add(R.mipmap.medialpterygoid_s);
            array_image1.add(R.mipmap.middledeltoid_s);
            array_image1.add(R.mipmap.multifidus_cervical_s);
            array_image1.add(R.mipmap.multifidus_thoracicandlumbar_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.obturatorinternus_s);
            array_image1.add(R.mipmap.occipitalis_s);
            array_image1.add(R.mipmap.occipitofrontalis_s);
            array_image1.add(R.mipmap.opponenspollicis_s);
            array_image1.add(R.mipmap.orbicularisoculi_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.palmarislongus_s);
            array_image1.add(R.mipmap.paraspinal_midthoracic_s);
            array_image1.add(R.mipmap.paraspinal_thoracolumbar_s);
            array_image1.add(R.mipmap.pectineus_s);
            array_image1.add(R.mipmap.pectoralismajor1_s);
            array_image1.add(R.mipmap.pectoralismajor2_s);
            array_image1.add(R.mipmap.pectoralismajor3_s);
            array_image1.add(R.mipmap.pectoralismajor4_s);
            array_image1.add(R.mipmap.pectoralisminor_s);
            array_image1.add(R.mipmap.pelvicfloor_s);
            array_image1.add(R.mipmap.peroneusbrevis_s);
            array_image1.add(R.mipmap.peroneuslongus_s);
            array_image1.add(R.mipmap.peroneustertius_s);
            array_image1.add(R.mipmap.piriformis1_s);
            array_image1.add(R.mipmap.piriformis2_s);
            array_image1.add(R.mipmap.plantaris_s);
            array_image1.add(R.mipmap.platysma_s);
            array_image1.add(R.mipmap.popliteus_s);
            array_image1.add(R.mipmap.posteriordeltoid_s);
            array_image1.add(R.mipmap.pronatorteres_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.quadratuslumborum1_s);
            array_image1.add(R.mipmap.quadratuslumborum2_s);
            array_image1.add(R.mipmap.quadratuslumborum3_s);
            array_image1.add(R.mipmap.quadratuslumborum4_s);
            array_image1.add(R.mipmap.quadratusplantae_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.rectusabdominis_s);
            array_image1.add(R.mipmap.rectusfemoris_s);
            array_image1.add(R.mipmap.rhomboid_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.sartorius_s);
            array_image1.add(R.mipmap.scalene_s);
            array_image1.add(R.mipmap.scalenusminimus_s);
            array_image1.add(R.mipmap.seconddorsalinterosseousofthehand_s);
            array_image1.add(R.mipmap.semimembranosusandsemitendinosus_s);
            array_image1.add(R.mipmap.semispinaliscapitis1_s);
            array_image1.add(R.mipmap.semispinaliscapitis2_s);
            array_image1.add(R.mipmap.serratusanterior_s);
            array_image1.add(R.mipmap.serratusposteriorinferior_s);
            array_image1.add(R.mipmap.serratusposteriorsuperior_s);
            array_image1.add(R.mipmap.soleus1_s);
            array_image1.add(R.mipmap.soleus2_s);
            array_image1.add(R.mipmap.soleus3_s);
            array_image1.add(R.mipmap.soleus4_s);
            array_image1.add(R.mipmap.spleniuscervicis1_s);
            array_image1.add(R.mipmap.spleniuscervicis2_s);
            array_image1.add(R.mipmap.sternalis_s);
            array_image1.add(R.mipmap.sternocleidomastoid1_s);
            array_image1.add(R.mipmap.sternocleidomastoid2_s);
            array_image1.add(R.mipmap.subclavius_s);
            array_image1.add(R.mipmap.suboccipital_s);
            array_image1.add(R.mipmap.subscapularis_s);
            array_image1.add(R.mipmap.supinator_s);
            array_image1.add(R.mipmap.supraspinatus1_s);
            array_image1.add(R.mipmap.supraspinatus2_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.temporalis1_s);
            array_image1.add(R.mipmap.temporalis2_s);
            array_image1.add(R.mipmap.temporalis3_s);
            array_image1.add(R.mipmap.temporalis4_s);
            array_image1.add(R.mipmap.tensorfasciaelatae_s);
            array_image1.add(R.mipmap.teresmajor_s);
            array_image1.add(R.mipmap.teresminor_s);
            array_image1.add(R.mipmap.tibialisanterior_s);
            array_image1.add(R.mipmap.tibialisposterior_s);
            array_image1.add(R.mipmap.trapezius1_s);
            array_image1.add(R.mipmap.trapezius2_s);
            array_image1.add(R.mipmap.trapezius3_s);
            array_image1.add(R.mipmap.trapezius4_s);
            array_image1.add(R.mipmap.trapezius5_s);
            array_image1.add(R.mipmap.trapezius6_s);
            array_image1.add(R.mipmap.trapezius7_s);
            array_image1.add(R.mipmap.tricepsbrachii1_s);
            array_image1.add(R.mipmap.tricepsbrachii2_s);
            array_image1.add(R.mipmap.tricepsbrachii3_s);
            array_image1.add(R.mipmap.tricepsbrachii4_s);
            array_image1.add(R.mipmap.tricepsbrachii5_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.vastusintermedius_s);
            array_image1.add(R.mipmap.vastuslateralis1_s);
            array_image1.add(R.mipmap.vastuslateralis2_s);
            array_image1.add(R.mipmap.vastuslateralis3_s);
            array_image1.add(R.mipmap.vastuslateralis4_s);
            array_image1.add(R.mipmap.vastuslateralis5_s);
            array_image1.add(R.mipmap.vastusmedialis1_s);
            array_image1.add(R.mipmap.vastusmedialis2_s);
            array_image1.add(0);
            array_image1.add(R.mipmap.zygomaticusmajor_s);

        }

        if(array_imagePeriosteal1.size() < 1){

            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.angleoftheribs_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.c2spinousprocess_s);
            array_imagePeriosteal1.add(R.mipmap.calcanealspur_s);
            array_imagePeriosteal1.add(R.mipmap.clavicle_sternalend_s);
            array_imagePeriosteal1.add(R.mipmap.coccyx_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.deltoidinsertion_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.erbspoint_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.fibularhead_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.iliaccrest_s);
            array_imagePeriosteal1.add(R.mipmap.ischialtuberosity_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.kneejointline_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.lateralepicondyle_s);
            array_imagePeriosteal1.add(R.mipmap.lateralpubicsymphysis_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.mandibularcondyle_s);
            array_imagePeriosteal1.add(R.mipmap.medialepicondyle_s);
            array_imagePeriosteal1.add(R.mipmap.metatarsalhead_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.nuchalline_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.pesanserine_s);
            array_imagePeriosteal1.add(R.mipmap.posteriorforamenmagnum_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.radialstyloid_s);
            array_imagePeriosteal1.add(R.mipmap.ribs_anterior_s);
            array_imagePeriosteal1.add(R.mipmap.ribs_axilla_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.spinousprocess_s);
            array_imagePeriosteal1.add(R.mipmap.sternalcostal_1strib_s);
            array_imagePeriosteal1.add(R.mipmap.sternoclavicular_s);
            array_imagePeriosteal1.add(R.mipmap.superiorpatella_s);
            array_imagePeriosteal1.add(R.mipmap.superiorpubicsymphysis_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.transverseprocessofatlas_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.xiphoid_s);
        }

        if(joint_image.size() < 1){

            joint_image.add(0);
            joint_image.add(R.mipmap.dura_cervicothoracic);
            joint_image.add(R.mipmap.dura_thoracic);
            joint_image.add(R.mipmap.dura_thoracolumbar);
            joint_image.add(0);
            joint_image.add(R.mipmap.facetc0_c1_c2);
            joint_image.add(R.mipmap.facetc2_c3);
            joint_image.add(R.mipmap.facetc3_c4);
            joint_image.add(R.mipmap.facetc4_c5);
            joint_image.add(R.mipmap.facetc5_c6);
            joint_image.add(R.mipmap.facetc6_c7);
            joint_image.add(R.mipmap.facetc7_t1);
            joint_image.add(R.mipmap.facetlumbar);
            joint_image.add(R.mipmap.facett1_t2);
            joint_image.add(R.mipmap.facett11_t12);
            joint_image.add(R.mipmap.facett3_t4);
            joint_image.add(R.mipmap.facett5_t6);
            joint_image.add(R.mipmap.facett6_t7);
            joint_image.add(R.mipmap.facett7_t11);
            joint_image.add(R.mipmap.facett2_t3);
            joint_image.add(R.mipmap.facett4_t5);
            joint_image.add(0);
            joint_image.add(R.mipmap.sacroiliacjoint);
            joint_image.add(R.mipmap.sclerotomec1);
            joint_image.add(R.mipmap.sclerotomec2);
            joint_image.add(R.mipmap.sclerotomec3);
            joint_image.add(R.mipmap.sclerotomec4);
            joint_image.add(R.mipmap.sclerotomec5);
            joint_image.add(R.mipmap.sclerotomec6);
            joint_image.add(R.mipmap.sclerotomec7);
            joint_image.add(R.mipmap.sclerotomec8);
            joint_image.add(R.mipmap.sclerotomel1);
            joint_image.add(R.mipmap.sclerotomel2);
            joint_image.add(R.mipmap.sclerotomel3);
            joint_image.add(R.mipmap.sclerotomel4);
            joint_image.add(R.mipmap.sclerotomel5);
            joint_image.add(R.mipmap.sclerotomes1);
            joint_image.add(R.mipmap.sclerotomet1);
            joint_image.add(R.mipmap.sclerotomet10);
            joint_image.add(R.mipmap.sclerotomet11);
            joint_image.add(R.mipmap.sclerotomet12);
            joint_image.add(R.mipmap.sclerotomet2);
            joint_image.add(R.mipmap.sclerotomet3);
            joint_image.add(R.mipmap.sclerotomet4);
            joint_image.add(R.mipmap.sclerotomet5);
            joint_image.add(R.mipmap.sclerotomet6);
            joint_image.add(R.mipmap.sclerotomet7);
            joint_image.add(R.mipmap.sclerotomet8);
            joint_image.add(R.mipmap.sclerotomet9);
            joint_image.add(0);
            joint_image.add(R.mipmap.thermatome_t1_t2);
            joint_image.add(R.mipmap.thermatome_t1_t4);
            joint_image.add(R.mipmap.thermatome_t11_l2);
            joint_image.add(R.mipmap.thermatome_t2_t7);
            joint_image.add(R.mipmap.thermatome_t4_l2);
        }

        if(array_imageLigament.size() < 1){
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.acromioclavicular);
            array_imageLigament.add(R.drawable.anteriorandposteriorcruciate);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.cervicalfacetcapsule_lower);
            array_imageLigament.add(R.drawable.cervicalfacet_capsule_mid);
            array_imageLigament.add(R.drawable.cervicalfacetcapsule_upper);
            array_imageLigament.add(R.drawable.costosternal);
            array_imageLigament.add(R.drawable.costotransverse);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.deltoid);
            array_imageLigament.add(R.drawable.dorsalcarpal);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.extensorretinaculum_ankle);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.femoralacetabularjointcapsule);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.glenohumeraljointcapsule);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.iliolumbar);
            array_imageLigament.add(R.drawable.inguinal);
            array_imageLigament.add(R.drawable.interosseousmembrane_forearm);
            array_imageLigament.add(R.drawable.interosseousmembrane_shin);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.lateralcollateral_knee);
            array_imageLigament.add(R.drawable.lumbarfacetcapsule);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.medialcollateral_knee);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.palmercarpal);
            array_imageLigament.add(R.drawable.pubicsymphysis);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.radialcollateral);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.sacroiliac);
            array_imageLigament.add(R.drawable.sacrospinous);
            array_imageLigament.add(R.drawable.sacrotuberous);
            array_imageLigament.add(R.drawable.sternoclavicular);
            array_imageLigament.add(R.drawable.supraspinatus_c5);
            array_imageLigament.add(R.drawable.supraspinatus_c6);
            array_imageLigament.add(R.drawable.supraspinatus_c7);
            array_imageLigament.add(R.drawable.supraspinatus_l3);
            array_imageLigament.add(R.drawable.supraspinatus_l4);
            array_imageLigament.add(R.drawable.supraspinatus_l5);
            array_imageLigament.add(R.drawable.supraspinatus_s1);
            array_imageLigament.add(R.drawable.supraspinatus_t1);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.talofibular);
            array_imageLigament.add(R.drawable.thoracicfacetcapsule_upper);
            array_imageLigament.add(R.drawable.thoracolumbarfacetcapsule);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.ulnarcollateral);
        }


       /* select HomeTitle */
        titleChcange(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE);
        if(Constants.activitystatus==0){
            gotoHomeFragment();
        }else if(Constants.activitystatus==1){
            gotoSearchFragment();
        }



    }


    private void setupNavigationBar() {

        drawerToggle = new ActionBarDrawerToggle(this, ui_drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);}

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);}
        };

        ui_drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void showDrawer() {

        ui_drawerLayout.openDrawer(Gravity.LEFT);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_drawer:
                showDrawer();
                break;

            case R.id.imv_help:
                gotoTerms();
                titleChcange(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE);
                break;

        }

    }

    @Override
    public boolean onNavigationItemSelected( MenuItem item) {

        int id = item.getItemId();

        if (item.isChecked()) item.setChecked(false); else item.setChecked(true);
        ui_drawerLayout.closeDrawers();

        switch (id){

            case R.id.nav_home:
                gotoHomeFragment();
                break;

            case R.id.nav_glossary:
                gotoTreatmentFragment();
                break;

            case R.id.nav_tissues:
                gotoSearchFragment();
                break;

        }

        return true;
    }



    public void gotoSearchFragment() {

        SearchFragment fragment = new SearchFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();

    }

    public void gotoTreatmentFragment(){

        TreatmentFragment fragment = new TreatmentFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoHomeFragment(){

        HomeFragment fragment = new HomeFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoTerms(){

        startActivity(new Intent(this, OrientationActivity.class));

        overridePendingTransition(0,0);

    }

    public void titleChcange(int a, int b, int c,int d, int e, int f){

        ui_imvLogo.setVisibility(a);

        ui_imvSearch.setVisibility(b);
        ui_txvSearch.setVisibility(c);

        ui_imvTreatment.setVisibility(d);
        ui_txvTreatment.setVisibility(e);

        ui_imvHelp.setVisibility(f);

    }

    @Override
    public void onBackPressed() {

        if (ui_drawerLayout.isDrawerOpen(GravityCompat.START)){
            this.ui_drawerLayout.closeDrawer(GravityCompat.START);

        } else {

            onExit();
        }
    }


}

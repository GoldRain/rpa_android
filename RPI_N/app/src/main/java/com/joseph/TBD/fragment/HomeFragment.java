package com.joseph.TBD.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.base.BaseFragment;
import com.joseph.TBD.main.MainActivity;
import com.joseph.TBD.main.PainSelcection.PainSelectActivity;
import com.joseph.TBD.utils.ScaleContents;

import java.util.Set;

import static com.joseph.TBD.R.id.imv_head;
import static com.joseph.TBD.commons.Constants.body_part;
import static com.joseph.TBD.commons.Constants.post;


public class HomeFragment extends BaseFragment implements View.OnClickListener {

    MainActivity _activity;
    View view ;

    ImageView ui_fabDetails;
    LinearLayout ui_lytAnterior, ui_lytPosterior;
    TextView ui_txvAnterior, ui_txvPosterior , txv_title;

    ImageView ui_imvHead,ui_imvArms,ui_imvArmL , ui_imvArmL_1,ui_imvArmR, ui_imvArmR_1, ui_imvBody,ui_imvBody_1, ui_imvLegs,ui_imvLegL, ui_imvLegR, ui_imvImage;

    LinearLayout ui_lytAnterior_b, ui_lytPosterior_b ;

    public HomeFragment(MainActivity activity) {
        // Required empty public constructor

        this._activity = activity ;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_home, container, false);

        loadLayout();
        return view ;
    }

    private void loadLayout() {

        ui_txvAnterior = (TextView)view.findViewById(R.id.txv_posterior_hom);
        ui_txvPosterior = (TextView)view.findViewById(R.id.txv_anterior_hom);

        ui_lytAnterior_b = (LinearLayout)view.findViewById(R.id.lyt_anterior_hom);
        ui_lytAnterior_b.setOnClickListener(this);

        ui_lytPosterior_b = (LinearLayout)view.findViewById(R.id.lyt_posterior_hom);
        ui_lytPosterior_b.setOnClickListener(this);

        ui_imvHead = (ImageView)view.findViewById(R.id.imv_head);
        ui_imvHead.setOnClickListener(this);

        ui_imvImage = (ImageView)view.findViewById(R.id.imv_image);
        ui_imvImage.setImageResource(R.drawable.anterior);

        ui_imvArms = (ImageView)view.findViewById(R.id.imv_chest);
        ui_imvArms.setOnClickListener(this);
        ui_imvArmL =(ImageView)view.findViewById(R.id.imv_armsL);
        ui_imvArmL.setOnClickListener(this);

        ui_imvArmL_1 = (ImageView)view.findViewById(R.id.imv_armsL_1);
        ui_imvArmL_1.setOnClickListener(this);

        ui_imvArmR =(ImageView)view.findViewById(R.id.imv_armsR);
        ui_imvArmR.setOnClickListener(this);

        ui_imvArmR_1 = (ImageView)view.findViewById(R.id.imv_armsR_1);
        ui_imvArmR_1.setOnClickListener(this);

        ui_imvBody = (ImageView)view.findViewById(R.id.imv_body);
        ui_imvBody.setOnClickListener(this);

        ui_imvBody_1 = (ImageView)view.findViewById(R.id.imv_body_1);
        ui_imvBody_1.setOnClickListener(this);

        ui_imvLegs = (ImageView)view.findViewById(R.id.imv_legs);
        ui_imvLegs.setOnClickListener(this);

        ui_imvLegL = (ImageView)view.findViewById(R.id.imv_legsL);
        ui_imvLegL.setOnClickListener(this);

        ui_imvLegR = (ImageView)view.findViewById(R.id.imv_legsR);
        ui_imvLegR.setOnClickListener(this);

        ui_fabDetails = (ImageView)view.findViewById(R.id.fab_details);
        ui_fabDetails.setOnClickListener(this);

        post = 1;
        body_part = 1;
        //visibleMain(View.VISIBLE, View.GONE);
        colorSelect(R.color.colorAccent, R.color.white);

        if(post == 1 && body_part == 1){

            colorSelect(R.color.colorAccent, R.color.white);
            ui_imvImage.setImageResource(R.drawable.anterior);

        }else if(post == 2 && body_part == 2){

            colorSelect(R.color.white, R.color.colorAccent);
            ui_imvImage.setImageResource(R.drawable.posterior);
        }
    }


    public boolean checkValue(){

        if (body_part == 1 || body_part == 2 ){
            _activity.showAlertDialog("Please select a body region");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lyt_anterior_hom: //front
                post = 1;
                body_part = 1;
                colorSelect(R.color.colorAccent, R.color.white);
                ui_imvImage.setImageResource(R.drawable.anterior);

                break;

            case R.id.lyt_posterior_hom: // back
                post = 2;
                body_part = 2;
                colorSelect(R.color.white, R.color.colorAccent);
                ui_imvImage.setImageResource(R.drawable.posterior);

                break;

            case R.id.imv_head:
                if (post == 1) {
                    ui_imvImage.setImageResource(R.drawable.head);
                    body_part = 3 ;

                } else if (post == 2){
                    ui_imvImage.setImageResource(R.drawable.back_head);
                    body_part = 4 ;
                }
                break;

            case R.id.imv_chest:
                if (post == 1) {
                    ui_imvImage.setImageResource(R.drawable.chest);

                } else if (post == 2){
                    ui_imvImage.setImageResource(R.drawable.back_chest);
                }
                body_part = 5;
                break;


            case R.id.imv_armsL:
                if (post == 1) {
                    ui_imvImage.setImageResource(R.drawable.chest);


                } else if (post == 2){
                    ui_imvImage.setImageResource(R.drawable.back_chest);
                }body_part = 6;
                break;

            case R.id.imv_armsL_1:

                if (post == 1) {
                    ui_imvImage.setImageResource(R.drawable.chest);


                } else if (post == 2){
                    ui_imvImage.setImageResource(R.drawable.back_chest);
                }body_part = 6;
                break;

            case R.id.imv_armsR_1:

                if (post == 1) {
                    ui_imvImage.setImageResource(R.drawable.chest);


                } else if (post == 2){
                    ui_imvImage.setImageResource(R.drawable.back_chest);
                }body_part = 6;
                break;

            case R.id.imv_armsR:
                if (post == 1) {
                    ui_imvImage.setImageResource(R.drawable.chest);

                } else if (post == 2){
                    ui_imvImage.setImageResource(R.drawable.back_chest);
                } body_part = 7;
                break;

            case R.id.imv_body:
                if (post == 1) {
                    ui_imvImage.setImageResource(R.drawable.body);

                } else if (post == 2){
                    ui_imvImage.setImageResource(R.drawable.back_body);
                } body_part = 8;
                break;

            case R.id.imv_body_1:

                if (post == 1) {

                    ui_imvImage.setImageResource(R.drawable.body);

                    body_part = 8;

                } else if (post == 2){

                    ui_imvImage.setImageResource(R.drawable.back_legs);

                    body_part = 9;
                }

                break;

            case R.id.imv_legs:
                if (post == 1) {
                    ui_imvImage.setImageResource(R.drawable.legs);
                } else if (post == 2){
                    ui_imvImage.setImageResource(R.drawable.back_legs);
                }body_part = 9;
                break;

            case R.id.imv_legsL:
                if (post == 1) {
                    ui_imvImage.setImageResource(R.drawable.legs);
                } else if (post == 2){
                    ui_imvImage.setImageResource(R.drawable.back_legs);
                }body_part = 9;
                break;

            case R.id.imv_legsR:

                if (post == 1) {
                    ui_imvImage.setImageResource(R.drawable.legs);
                } else if (post == 2){
                    ui_imvImage.setImageResource(R.drawable.back_legs);
                }body_part = 9;
                break;

            case R.id.fab_details:
                if (checkValue()){

                    Log.d("post++++bodypart=====>", post + ":" + body_part);
                    Intent intent = new Intent(_activity, PainSelectActivity.class);
                    _activity.startActivity(intent);
                }
        }
    }

    public void colorSelect(int a, int b){

        ui_txvPosterior.setTextColor(getResources().getColor(a));
        ui_txvAnterior.setTextColor(getResources().getColor(b));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}
